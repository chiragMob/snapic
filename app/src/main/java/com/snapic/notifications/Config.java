package com.assistance.roadside.notifications;

/**
 * Created by Mahipal Singh  mahisingh1@Outlook.com on 18/12/18.
 */
public class Config {

    //global topic to receive app wide push notifications
    public static final String TOPIC_GLOBAL = "global";

    //broadcast receiver intent filter
    public static final String REGISTRATION_COMPLETE = "registrationComplete";
    public static final String PUSH_NOTIFICATION = "pushNotification";
    public static final String CHAT_PUSH_NOTIFICATION = "chat_notification";
    public static final String RECURUITER_ID = "jobpost_id";
    public static final String NOTIFICATION_COUNT = "countnoti";
    public static final String BADGE_COUNT = "badgeCount";
    public static final String NOTIFICATION_ID_INTENT = "notificationid_intent";


    //id to handle the notification in the notificaiton trau
    public static final int NOTIFICATION_ID = 100;
    public static final int NOTIFICATION_ID_BIG_IMAGE = 101;

    public static final String SHARED_PREF = "ah_firebase";


}
