package com.assistance.roadside.notifications;

import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.assistance.roadside.activity.UserCurrentBookingActivity;
import com.assistance.roadside.activity.UserHomeActivity;
import com.assistance.roadside.utilities.SharedPreferenceWriter;
import com.assistance.roadside.vendoractivity.ProviderReActivity;
import com.assistance.roadside.vendoractivity.VendorHomeActivity;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;


import org.json.JSONObject;

import java.util.Map;

import androidx.annotation.NonNull;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;


/**
 * Created by Mahipal Singh  mahisingh1@Outlook.com on 18/12/18.
 */

public class MyFirebaseMessagingService extends FirebaseMessagingService {

    private static final String TAG = MyFirebaseMessagingService.class.getSimpleName();

    private NotificationUtils notificationUtils;

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);
        if (remoteMessage == null)
            return;

        try {

            handleDataMessage(remoteMessage);

            //new BackgroundWork().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, jsonObject);
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }


    //for new version
    @Override
    public void onNewToken(@NonNull String s) {
        super.onNewToken(s);

        String refreshedToken = s;
        Log.v(TAG, "sendRegistrationToServer: " + refreshedToken);

    //    SharedPreferenceWriter.getInstance(this).writeStringValue(SPreferenceKey.DEVICETOKEN, refreshedToken);

    }


    private void handleDataMessage(RemoteMessage remoteMessage) throws Exception {
        //Toast.makeText(notificationUtils, ""+remoteMessage.getData().toString(), Toast.LENGTH_SHORT).show();
        Map<String, String> data = remoteMessage.getData();
        JSONObject jsonObject = new JSONObject(data);

        String title = jsonObject.getString("title");
        String body = jsonObject.getString("body");
        //String type = jsonObject.getString("message");


        try {
            if (NotificationUtils.isAppIsInBackground(getApplicationContext())) {
                if(title.matches("New booking request available"))
                {
                    Intent pushNotification = new Intent(Config.PUSH_NOTIFICATION);
                    LocalBroadcastManager.getInstance(this).sendBroadcast(pushNotification);
//                showNotificationMessage(getApplicationContext(), title, body, "", new Intent());
                    Intent intent = new Intent(getApplicationContext(), ProviderReActivity.class);
                    showNotificationMessage(getApplicationContext(), title, body, "", intent);

                    // play notification sound
                    NotificationUtils notificationUtils = new NotificationUtils(getApplicationContext());
                    notificationUtils.playNotificationSound();
                }
                else {
                    Intent pushNotification = new Intent(Config.PUSH_NOTIFICATION);
                    LocalBroadcastManager.getInstance(this).sendBroadcast(pushNotification);
//                showNotificationMessage(getApplicationContext(), title, body, "", new Intent());
                    Intent intent = new Intent(getApplicationContext(), VendorHomeActivity.class);
                    showNotificationMessage(getApplicationContext(), title, body, "", intent);

                    // play notification sound
                    NotificationUtils notificationUtils = new NotificationUtils(getApplicationContext());
                    notificationUtils.playNotificationSound();
                }
            }


            else {
                if(title.matches("New booking request available"))
                {
                    Intent pushNotification = new Intent(Config.PUSH_NOTIFICATION);
                    LocalBroadcastManager.getInstance(this).sendBroadcast(pushNotification);
                    Intent intent = new Intent(getApplicationContext(), ProviderReActivity.class);
                    showNotificationMessage(getApplicationContext(), title, body, "", intent);
                    // play notification sound
                    NotificationUtils notificationUtils = new NotificationUtils(getApplicationContext());
                    notificationUtils.playNotificationSound();
                }
                else
                {
                    Intent pushNotification = new Intent(Config.PUSH_NOTIFICATION);
                    LocalBroadcastManager.getInstance(this).sendBroadcast(pushNotification);
                    Intent intent = new Intent(getApplicationContext(), VendorHomeActivity.class);
                    showNotificationMessage(getApplicationContext(), title, body, "", intent);
                    // play notification sound
                    NotificationUtils notificationUtils = new NotificationUtils(getApplicationContext());
                    notificationUtils.playNotificationSound();
                }

            }


        } catch (Exception e) {
            Log.e(TAG, "Exception: " + e.getMessage());
        }
    }


    /**
     * Showing notification with text only
     */
    private void showNotificationMessage(Context context, String title, String message, String timeStamp, Intent intent) {
        notificationUtils = new NotificationUtils(context);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        notificationUtils.showNotificationMessage(title, message, timeStamp, intent);
        // play notification sound
        NotificationUtils notificationUtils = new NotificationUtils(getApplicationContext());
        notificationUtils.playNotificationSound();
    }

    /**
     * Showing notification with text and image
     */
    private void showNotificationMessageWithBigImage(Context context, String title, String message, String timeStamp, Intent intent, String imageUrl) {
        notificationUtils = new NotificationUtils(context);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        notificationUtils.showNotificationMessage(title, message, timeStamp, intent, imageUrl);
    }


}

