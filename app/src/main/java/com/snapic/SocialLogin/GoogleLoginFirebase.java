package com.assistance.roadside.SocialLogin;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;

import com.assistance.roadside.R;
import com.assistance.roadside.utilities.DialogPopup;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GoogleAuthProvider;


import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;


/**
 * Created by mahipal singh on 5/9/18.
 */
public class GoogleLoginFirebase extends AppCompatActivity implements GoogleApiClient.OnConnectionFailedListener, GoogleApiClient.ConnectionCallbacks {

    private static final String TAG = "LoginModel";
    private static final int RC_SIGN_IN = 9001;
    private static final int GOOGLE_DATA = 122;
    private GoogleApiClient mGoogleApiClient;
    private GoogleSignInClient mGoogleSignInClient;
    private String image = "";
    private String gender;
    private String socialid = "";
    private String email = "";
    private String name = "";
    private String f_name = "";
    private String l_name = "";
    private FirebaseAuth mAuth;
    private String phoneNumber = "";
    DialogPopup dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
// Initialize Firebase Auth
        mAuth = FirebaseAuth.getInstance();
        dialog = new DialogPopup(this);
        // Configure Google Sign In
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(this.getString(R.string.default_web_client_id))
                .requestEmail()
                .build();
        // Build a GoogleSignInClient with the options specified by gso.
        mGoogleSignInClient = GoogleSignIn.getClient(this, gso);

        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this, this)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();

        signIn();

    }

    private void signIn() {
        Intent signInIntent = mGoogleSignInClient.getSignInIntent();
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {

        Log.d(TAG, "onConnectionFailed:" + connectionResult);
    }

    private void signOut() {
        new Handler().post(new Runnable() {
            @Override
            public void run() {
                if (mGoogleApiClient.isConnected()) {
                    Auth.GoogleSignInApi.signOut(mGoogleApiClient).setResultCallback(new ResultCallback<Status>() {
                        @Override
                        public void onResult(Status status) {
                            if (status.getStatus().getStatusCode() == 0) {
                                Intent intent = new Intent();
                                intent.putExtra("socialid", socialid);
                                intent.putExtra("email", email);
                                intent.putExtra("name", name);
                                intent.putExtra("f_name", f_name);
                                intent.putExtra("l_name", l_name);
                                intent.putExtra("image", image);
                                intent.putExtra("phoneNumber", phoneNumber);
                                setResult(GOOGLE_DATA, intent);
                                dialog.dismissLoadingDialog();

                                finish();
                            }
                        }
                    });
                } else {
                    signOut();
                }
            }
        });

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // Result returned from launching the Intent from GoogleSignInApi.getSignInIntent(...);
        if (requestCode == RC_SIGN_IN) {
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            try {
                // Google Sign In was successful, authenticate with Firebase
                GoogleSignInAccount account = task.getResult(ApiException.class);
                firebaseAuthWithGoogle(account);
            } catch (ApiException e) {
                // Google Sign In failed, update UI appropriately
                //  Toast.makeText(this, ""+e, Toast.LENGTH_SHORT).show();
                Log.w(TAG, "Google sign in failed", e);
                // ...
                finish();
            }
        }
    }

    private void firebaseAuthWithGoogle(GoogleSignInAccount acct) {
        Log.d(TAG, "firebaseAuthWithGoogle:" + acct.getId());
        AuthCredential credential = GoogleAuthProvider.getCredential(acct.getIdToken(), null);
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            // Sign in success, update UI with the signed-in user's information
                            Log.d(TAG, "signInWithCredential:success");
                            FirebaseUser user = mAuth.getCurrentUser();
                            handleSignInResult(user, task);
                            task.getResult().getUser().getEmail();
                        } else {
                            // If sign in fails, display a message to the user.
                            Log.w(TAG, "signInWithCredential:failure", task.getException());
//                            updateUI(null);
                        }

                    }
                });
    }

    private void handleSignInResult(FirebaseUser result, Task<AuthResult> task) {
        Log.d(TAG, "handleSignInResult:" + result);
        Log.d(TAG, "google uid:" + result.getUid());
        dialog.showLoadingDialog(this, "");
        if (result != null) {

            Uri personPhoto = result.getPhotoUrl();
            if (personPhoto != null) {
                image = personPhoto.toString();
            } else {
                image = "";
            }
            gender = "Male";
            socialid = task.getResult().getAdditionalUserInfo().getProfile().get("sub").toString();
            email = result.getEmail();
            if (email == null)
                email = (String) task.getResult().getAdditionalUserInfo().getProfile().get("email");

            phoneNumber = result.getPhoneNumber();
            name = result.getDisplayName();
            if (name == null) {
                name = "";
            }
            String[] u_name = null;
            if (name.contains(" ")) {
                u_name = name.split(" ");
                if (u_name.length > 1) {
                    f_name = u_name[0];
                    l_name = u_name[1];
                } else {
                    f_name = u_name[0];
                    l_name = u_name[0];
                }


            }
            signOut();
            Log.i("Gmail LoginModel", "" + name + "" + email + "" + socialid + "" + image);
        }
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        dialog.dismissLoadingDialog();
        finish();
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {

    }

    @Override
    public void onConnectionSuspended(int i) {

    }
}
