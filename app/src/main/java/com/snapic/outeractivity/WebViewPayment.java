package com.snapic.outeractivity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.webkit.WebView;
import android.webkit.WebViewClient;


import com.snapic.R;
import com.snapic.utilities.DialogPopup;

import androidx.appcompat.app.AppCompatActivity;
import butterknife.BindView;
import butterknife.ButterKnife;

public class WebViewPayment extends AppCompatActivity {

    @BindView(R.id.webUrl)
    WebView webUrl;
    String mobilenumber, amoount, purpuse, email, donationid;
    String supid, sellerid;
    String data;
    String appointdate;
    String ticketid, quantity;
    String Appoinid, deliverdate, addressid;
    String total="";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_web_views);
        ButterKnife.bind(this);
        getIntentData();
        DialogPopup dialog = new DialogPopup(WebViewPayment.this);
        dialog.showLoadingDialog(WebViewPayment.this, "");
        webUrl.setWebViewClient(new WebViewClient() {
            @Override
            public void onPageFinished(WebView view, String url) {
                DialogPopup dialog = new DialogPopup(WebViewPayment.this);
                dialog.dismissLoadingDialog();
                super.onPageFinished(view, url);

                try {
                    if (url.contains("http://18.190.46.17:3000/redirect?")) {
                        String paymentId = url.split("=")[1];
                        Intent i = new Intent(WebViewPayment.this, PaymentStatusActivity.class);
                        i.putExtra("id", paymentId);
                        i.putExtra("orderid",purpuse);
                        i.putExtra("total",total);

                        WebViewPayment.this.startActivity(i);
                        finish();

                        //  paymentDetail(mobilenumber,amoount,purpuse,paymentId,email);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                super.onPageStarted(view, url, favicon);

                //    CommonUtilities.showLoadingDialog(WebViewAct.this);
            }

        });

    }

//    private void serviceDonationBooking(String id, String paymentid) {
//        {
//            String value;
//
//
//            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
//            String formattedDate = dateFormat.format(new Date()).toString();
//
//
//            if (new InternetCheck(WebViewPayment.this).isConnect()) {
//                String key = SharedPreferenceWriter.getInstance(WebViewPayment.this).getString(SPreferenceKey.jwtToken);
//
//                //aray1@gmail.com,arya8055
//                DialogPopup dialog = new DialogPopup(WebViewPayment.this);
//                dialog.showLoadingDialog(WebViewPayment.this, "");
//
//                ApiInterface api_service = RetrofitInit.getConnect().createConnection();
//                Call<DonationAmountResponse> call = api_service.
//                        DonationBookingAmount(key, "application/x-www-form-urlencoded", id, amoount, "Card", paymentid, "Success", formattedDate);
//                call.enqueue(new Callback<DonationAmountResponse>() {
//                    @Override
//                    public void onResponse(Call<DonationAmountResponse> call, Response<DonationAmountResponse> response) {
//                        if (response.isSuccessful()) {
//                            DonationAmountResponse server_response = response.body();
//                            if (server_response.getStatus() == 200) {
//                                dialog.dismissLoadingDialog();
//                                //  setPreferences(server_response);
//
//                                Toast.makeText(WebViewPayment.this, "Thank you for your contribution! Your donation is greatly appreciated.", Toast.LENGTH_SHORT).show();
//
//                                Intent i = new Intent(WebViewPayment.this, HomeActivity.class);
//                                i.putExtra("id", "");
//
//                                WebViewPayment.this.startActivity(i);
//
//
//                            } else if (server_response.getStatus() == 400) {
//                                dialog.dismissLoadingDialog();
//
//                            } else if (server_response.getStatus() == 500) {
//                                dialog.dismissLoadingDialog();
//
//                              //  startActivity(new Intent(WebViewPayment.this, LoginActivity.class));
//                            } else {
//                                dialog.dismissLoadingDialog();
//
//                                Toast.makeText(WebViewPayment.this, server_response.getMessage(), Toast.LENGTH_SHORT).show();
//                            }
//                        }
//                    }
//
//                    @Override
//                    public void onFailure(Call<DonationAmountResponse> call, Throwable t) {
//                        dialog.dismissLoadingDialog();
//
//                    }
//                });
//            } else {
//                Toast toast = Toast.makeText(WebViewPayment.this, "Internet connection lost", Toast.LENGTH_LONG);
//                toast.setGravity(Gravity.CENTER, 0, 0);
//                toast.show();
//            }
//        }
//
//
//    }

//    private void serviceSupBooking(String paymentid) {
//        {
//            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
//            String formattedDate = dateFormat.format(new Date()).toString();
//
//            if (new InternetCheck(WebViewPayment.this).isConnect()) {
//                String key = SharedPreferenceWriter.getInstance(WebViewPayment.this).getString(SPreferenceKey.jwtToken);
//
//                //aray1@gmail.com,arya8055
//                DialogPopup dialog = new DialogPopup(WebViewPayment.this);
//                dialog.showLoadingDialog(WebViewPayment.this, "");
//
//                ApiInterface api_service = RetrofitInit.getConnect().createConnection();
//                Call<SubcriptionPaymentResponse> call = api_service.
//                        SubscriptionBooking(key, "application/x-www-form-urlencoded", sellerid, supid, "Card", paymentid, "Success", formattedDate);
//                call.enqueue(new Callback<SubcriptionPaymentResponse>() {
//                    @Override
//                    public void onResponse(Call<SubcriptionPaymentResponse> call, Response<SubcriptionPaymentResponse> response) {
//                        if (response.isSuccessful()) {
//                            SubcriptionPaymentResponse server_response = response.body();
//                            if (server_response.getStatus() == 200) {
//                                dialog.dismissLoadingDialog();
//                                //  setPreferences(server_response);
//
//                                Toast.makeText(WebViewPayment.this, "Subscription plan purchased successfully.", Toast.LENGTH_SHORT).show();
//                                Intent i = new Intent(WebViewPayment.this, HomeActivity.class);
//                                i.putExtra("id", "");
//                                WebViewPayment.this.startActivity(i);
//
//
//                            } else if (server_response.getStatus() == 400) {
//                                dialog.dismissLoadingDialog();
//
//                            } else if (server_response.getStatus() == 500) {
//                                dialog.dismissLoadingDialog();
//
//                            } else {
//                                dialog.dismissLoadingDialog();
//
//                                Toast.makeText(WebViewPayment.this, server_response.getMessage(), Toast.LENGTH_SHORT).show();
//                            }
//                        }
//                    }
//
//                    @Override
//                    public void onFailure(Call<SubcriptionPaymentResponse> call, Throwable t) {
//                        dialog.dismissLoadingDialog();
//
//                    }
//                });
//            } else {
//                Toast toast = Toast.makeText(WebViewPayment.this, "Internet connection lost", Toast.LENGTH_LONG);
//                toast.setGravity(Gravity.CENTER, 0, 0);
//                toast.show();
//            }
//        }
//
//
//    }

    private void getIntentData() {
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            // set title of toolbar
            Intent i = getIntent();
            purpuse = i.getStringExtra("orderid");
            total=i.getStringExtra("total");
            String url = i.getStringExtra("id");
            webUrl.getSettings().setJavaScriptEnabled(true);
            webUrl.loadUrl(url);



        }
    }


//    private void serviceTicketBooking(String paymentid) {
//        {
//
//            if (new InternetCheck(WebViewPayment.this).isConnect()) {
//                String key = SharedPreferenceWriter.getInstance(WebViewPayment.this).getString(SPreferenceKey.jwtToken);
//
//                //aray1@gmail.com,arya8055
//                DialogPopup dialog = new DialogPopup(WebViewPayment.this);
//                dialog.showLoadingDialog(WebViewPayment.this, "");
//
//                ApiInterface api_service = RetrofitInit.getConnect().createConnection();
//                Call<TicketBookingResponse> call = api_service.
//                        ticketBooking(key, "application/x-www-form-urlencoded", ticketid, amoount, "Card", paymentid, "Success", "0", amoount, quantity);
//                call.enqueue(new Callback<TicketBookingResponse>() {
//                    @Override
//                    public void onResponse(Call<TicketBookingResponse> call, Response<TicketBookingResponse> response) {
//                        if (response.isSuccessful()) {
//                            TicketBookingResponse server_response = response.body();
//                            if (server_response.getStatus() == 200) {
//                                dialog.dismissLoadingDialog();
//                                //  setPreferences(server_response);
//
//                                Toast.makeText(WebViewPayment.this, "Ticket is successfully purchased. Your booking is confirmed.", Toast.LENGTH_SHORT).show();
//
//                                Intent i = new Intent(WebViewPayment.this, HomeActivity.class);
//                                i.putExtra("id", "");
//
//                                WebViewPayment.this.startActivity(i);
//
//
//                            } else if (server_response.getStatus() == 400) {
//                                dialog.dismissLoadingDialog();
//
//                            } else if (server_response.getStatus() == 500) {
//                                dialog.dismissLoadingDialog();
//
//                                startActivity(new Intent(WebViewPayment.this, LoginActivity.class));
//                            } else {
//                                dialog.dismissLoadingDialog();
//
//                                Toast.makeText(WebViewPayment.this, server_response.getMessage(), Toast.LENGTH_SHORT).show();
//                            }
//                        }
//                    }
//
//                    @Override
//                    public void onFailure(Call<TicketBookingResponse> call, Throwable t) {
//                        dialog.dismissLoadingDialog();
//
//                    }
//                });
//            } else {
//                Toast toast = Toast.makeText(WebViewPayment.this, "Internet connection lost", Toast.LENGTH_LONG);
//                toast.setGravity(Gravity.CENTER, 0, 0);
//                toast.show();
//            }
//        }
//    }




}









