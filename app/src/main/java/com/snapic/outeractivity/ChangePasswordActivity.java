package com.snapic.outeractivity;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.view.Gravity;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.snapic.R;
import com.snapic.response.ChangePasswordResponse;
import com.snapic.retrofit.ApiInterface;
import com.snapic.retrofit.RetrofitInit;
import com.snapic.utilities.DialogPopup;
import com.snapic.utilities.InternetCheck;
import com.snapic.utilities.KeyBoardUtils;
import com.snapic.utilities.SPreferenceKey;
import com.snapic.utilities.SharedPreferenceWriter;

import androidx.appcompat.app.AppCompatActivity;
import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class ChangePasswordActivity extends AppCompatActivity {

    @BindView(R.id.img_back)
    ImageView img_back;
    @BindView(R.id.et_old)
    EditText et_old;
    @BindView(R.id.et_new)
    EditText et_new;
    @BindView(R.id.et_con)
    EditText et_con;
    @BindView(R.id.tv_old)
    TextView tv_old;
    @BindView(R.id.tv_new)
    TextView tv_new;
    @BindView(R.id.tv_con)
    TextView tv_con;
    @BindView(R.id.txt_login)
    TextView txt_login;
    KeyBoardUtils keyBoardUtils;
    final String PASSWORD_PATTERN = "^(?=.*[0-9])(?=.*[A-Z])(?=.*[@#$%^&+=!])(?=\\S+$).{4,}$";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_password);
        ButterKnife.bind(this);
        keyBoardUtils = KeyBoardUtils.getInstance(this);


        img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        tv_old.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                keyBoardUtils.hideKeyboardOutsideTouch(ChangePasswordActivity.this,et_old);
                if(et_old.getText().toString().isEmpty())
                {
                    Toast.makeText(ChangePasswordActivity.this, R.string.pass, Toast.LENGTH_SHORT).show();
                    et_old.setSelection(et_old.getText().length());
                }
                else {
                    String text = tv_old.getText().toString();
                    if (text.equals("Show")) {
                        et_old.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
                        tv_old.setText("hide");
                        et_old.setSelection(et_old.getText().length());

                    } else {
                        et_old.setTransformationMethod(PasswordTransformationMethod.getInstance());
                        tv_old.setText("Show");
                        et_old.setSelection(et_old.getText().length());


                    }
                }
            }
        });
        tv_new.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                keyBoardUtils.hideKeyboardOutsideTouch(ChangePasswordActivity.this,et_new);
                if(et_new.getText().toString().isEmpty())
                {
                    Toast.makeText(ChangePasswordActivity.this, R.string.pass, Toast.LENGTH_SHORT).show();
                    et_new.setSelection(et_new.getText().length());
                }
                else {
                    String text = tv_new.getText().toString();
                    if (text.equals("Show")) {
                        et_new.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
                        tv_new.setText("hide");
                        et_new.setSelection(et_new.getText().length());

                    } else {
                        et_new.setTransformationMethod(PasswordTransformationMethod.getInstance());
                        tv_new.setText("Show");
                        et_new.setSelection(et_new.getText().length());


                    }
                }
            }
        });

        tv_con.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                keyBoardUtils.hideKeyboardOutsideTouch(ChangePasswordActivity.this,et_con);
                if(et_con.getText().toString().isEmpty())
                {
                    Toast.makeText(ChangePasswordActivity.this, R.string.pass, Toast.LENGTH_SHORT).show();
                    et_con.setSelection(et_con.getText().length());
                }
                else {
                    String text = tv_con.getText().toString();
                    if (text.equals("Show")) {
                        et_con.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
                        tv_con.setText("hide");
                        et_con.setSelection(et_con.getText().length());

                    } else {
                        et_con.setTransformationMethod(PasswordTransformationMethod.getInstance());
                        tv_con.setText("Show");
                        et_con.setSelection(et_con.getText().length());


                    }
                }
            }
        });
        txt_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String old_pass=et_old.getText().toString();
                String new_pass=et_new.getText().toString();
                String con_pass=et_con.getText().toString();



                if (TextUtils.isEmpty(old_pass)) {
                    et_old.setFocusable(true);
                    et_old.requestFocus();
                    Toast.makeText(ChangePasswordActivity.this, R.string.pass, Toast.LENGTH_SHORT).show();

                } else if (et_old.getText().toString().length() <= 6 || et_old.getText().toString().length() >= 16) {
                    Toast.makeText(ChangePasswordActivity.this, R.string.betweens, Toast.LENGTH_SHORT).show();
                } else if (old_pass.length() >= 6 && old_pass.length() <= 16 && !old_pass.matches(PASSWORD_PATTERN)) {
                    Toast.makeText(ChangePasswordActivity.this, R.string.combss, Toast.LENGTH_SHORT).show();
                } else if (TextUtils.isEmpty(new_pass)) {
                    et_new.setFocusable(true);
                    et_new.requestFocus();
                    Toast.makeText(ChangePasswordActivity.this, R.string.con_pass, Toast.LENGTH_SHORT).show();

                }
                else if (et_new.getText().toString().length() <= 6 || et_new.getText().toString().length() >= 16) {
                    Toast.makeText(ChangePasswordActivity.this, R.string.between_ch, Toast.LENGTH_SHORT).show();
                } else if (new_pass.length() >= 6 && new_pass.length() <= 16 && !new_pass.matches(PASSWORD_PATTERN)) {
                    Toast.makeText(ChangePasswordActivity.this, R.string.comb_l, Toast.LENGTH_SHORT).show();
                }


                else if (!new_pass.equals(con_pass)) {

                    Toast.makeText(ChangePasswordActivity.this, R.string.matchess, Toast.LENGTH_SHORT).show();
                }
                else
                {
                    serviceChangePassword();
                }
            }
        });

    }
    private void serviceChangePassword() {
        {
            String old_pass=et_old.getText().toString();
            String new_pass=et_new.getText().toString();
            String con_pass=et_con.getText().toString();


            if (new InternetCheck(this).isConnect()) {
                String key = SharedPreferenceWriter.getInstance(ChangePasswordActivity.this).getString(SPreferenceKey.jwtToken);
                String id = SharedPreferenceWriter.getInstance(ChangePasswordActivity.this).getString(SPreferenceKey._id);
                String token="Token"+" "+key;
                //aray1@gmail.com,arya8055
                DialogPopup dialog = new DialogPopup(this);
                dialog.showLoadingDialog(this, "");

                ApiInterface api_service = RetrofitInit.getConnect().createConnection();
                Call<ChangePasswordResponse> call = api_service.changePasswordResponse(token,old_pass,new_pass,con_pass);
                call.enqueue(new Callback<ChangePasswordResponse>() {
                    @Override
                    public void onResponse(Call<ChangePasswordResponse> call, Response<ChangePasswordResponse> response) {
                        if (response.isSuccessful()) {
                            ChangePasswordResponse server_response = response.body();
                            if (server_response.getStatus() == 200) {
                                dialog.dismissLoadingDialog();
                                Toast.makeText(ChangePasswordActivity.this, ""+server_response.getSuccess(), Toast.LENGTH_SHORT).show();
                                Intent i = new Intent(ChangePasswordActivity.this, SettingsActivity.class);

                                ChangePasswordActivity.this.startActivity(i);
                            }  else {
                                dialog.dismissLoadingDialog();

                                Toast.makeText(ChangePasswordActivity.this, server_response.getSuccess(), Toast.LENGTH_SHORT).show();
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<ChangePasswordResponse> call, Throwable t) {
                        dialog.dismissLoadingDialog();

                    }
                });
            } else {
                Toast toast = Toast.makeText(this, R.string.in, Toast.LENGTH_LONG);
                toast.setGravity(Gravity.CENTER, 0, 0);
                toast.show();
            }
        }


    }

}