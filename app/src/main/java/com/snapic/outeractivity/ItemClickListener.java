package com.snapic.outeractivity;

/**
 * Created by Rahul on 12/5/2017.
 */

public interface ItemClickListener {

    public void onItemClickListener(String id);
}
