package com.snapic.outeractivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.Gravity;
import android.widget.TextView;
import android.widget.Toast;

import com.snapic.R;
import com.snapic.activity.HomeActivity;
import com.snapic.response.PaymentDataResponse;
import com.snapic.response.PaymentDoneResponse;
import com.snapic.retrofit.ApiInterface;
import com.snapic.retrofit.RetrofitInit;
import com.snapic.utilities.DialogPopup;
import com.snapic.utilities.InternetCheck;
import com.snapic.utilities.SPreferenceKey;
import com.snapic.utilities.SharedPreferenceWriter;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import androidx.appcompat.app.AppCompatActivity;
import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PaymentStatusActivity extends AppCompatActivity {

    @BindView(R.id.tv_pay)
    TextView tv_pay;
    String id="";
    String orderid="";
    String total="";
    @Override
    protected void onCreate( Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Intent i=getIntent();
       id= i.getStringExtra("id");
       orderid=i.getStringExtra("orderid");
        total=i.getStringExtra("total");
        ButterKnife.bind(this);
        homeImage();
    }
    private void homeImage() {
        {

            String deviceToken = SharedPreferenceWriter.getInstance(this).getString(SPreferenceKey.DEVICETOKEN);

            if (new InternetCheck(this).isConnect()) {
                //aray1@gmail.com,arya8055


                ApiInterface api_service = RetrofitInit.getConnects().createConnections();
                Call<PaymentDataResponse> call = api_service.getPaymentResponse(id);
                call.enqueue(new Callback<PaymentDataResponse>() {
                    @Override
                    public void onResponse(Call<PaymentDataResponse> call, Response<PaymentDataResponse> response) {
                        if (response.isSuccessful()) {
                            PaymentDataResponse server_response = response.body();
                            if (server_response.getStatus() == 200) {
                                Toast.makeText(PaymentStatusActivity.this, server_response.getMessage(), Toast.LENGTH_SHORT).show();
                              tv_pay.setText(server_response.getMessage());
                              servicePayment(total,server_response.getData().getId(),server_response.getData().getOperationid());
                                Toast.makeText(PaymentStatusActivity.this, ""+server_response.getMessage(), Toast.LENGTH_SHORT).show();


                            }  else {
                                tv_pay.setText(R.string.t_failed);
                                onBackPressed();
                                Toast.makeText(PaymentStatusActivity.this, ""+server_response.getMessage(), Toast.LENGTH_SHORT).show();


                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<PaymentDataResponse> call, Throwable t) {

                    }
                });
            } else {
                Toast toast = Toast.makeText(this, R.string.in, Toast.LENGTH_LONG);
                toast.setGravity(Gravity.CENTER, 0, 0);
                toast.show();
            }
        }


    }
    private void servicePayment(String amount,String paymentid,String operatedid) {
        {
            if (new InternetCheck(this).isConnect()) {
                //aray1@gmail.com,arya8055
                String key = SharedPreferenceWriter.getInstance(PaymentStatusActivity.this).getString(SPreferenceKey.jwtToken);
                String token="Token"+" "+key;
                DialogPopup dialog = new DialogPopup(this);
                dialog.showLoadingDialog(this, "");
                String date = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault()).format(new Date());

                ApiInterface api_service = RetrofitInit.getConnect().createConnection();
                Call<PaymentDoneResponse> call = api_service.paymentDone(token,orderid,paymentid,amount,"zain cash",date,operatedid);
                call.enqueue(new Callback<PaymentDoneResponse>() {
                    @Override
                    public void onResponse(Call<PaymentDoneResponse> call, Response<PaymentDoneResponse> response) {
                        if (response.isSuccessful()) {
                            PaymentDoneResponse server_response = response.body();
                            if (server_response.getStatus() == 200) {
                                dialog.dismissLoadingDialog();
                                SharedPreferenceWriter.getInstance(PaymentStatusActivity.this).writeStringValue(SPreferenceKey.id1,"");
                                SharedPreferenceWriter.getInstance(PaymentStatusActivity.this).writeStringValue(SPreferenceKey.id2,"");
                                SharedPreferenceWriter.getInstance(PaymentStatusActivity.this).writeStringValue(SPreferenceKey.id3,"");
                                SharedPreferenceWriter.getInstance(PaymentStatusActivity.this).writeStringValue(SPreferenceKey.id4,"");
                                SharedPreferenceWriter.getInstance(PaymentStatusActivity.this).writeStringValue(SPreferenceKey.id5,"");
                                SharedPreferenceWriter.getInstance(PaymentStatusActivity.this).writeStringValue(SPreferenceKey.id6,"");
                                SharedPreferenceWriter.getInstance(PaymentStatusActivity.this).writeStringValue(SPreferenceKey.id7,"");
                                SharedPreferenceWriter.getInstance(PaymentStatusActivity.this).writeStringValue(SPreferenceKey.id8,"");
                                SharedPreferenceWriter.getInstance(PaymentStatusActivity.this).writeStringValue(SPreferenceKey.id9,"");
                                SharedPreferenceWriter.getInstance(PaymentStatusActivity.this).writeStringValue(SPreferenceKey.id10,"");


                                startActivity(new Intent(PaymentStatusActivity.this, HomeActivity.class));
                            }  else {
                                dialog.dismissLoadingDialog();
                                Toast.makeText(PaymentStatusActivity.this, ""+server_response.getMessage(), Toast.LENGTH_SHORT).show();
                                startActivity(new Intent(PaymentStatusActivity.this, HomeActivity.class));

                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<PaymentDoneResponse> call, Throwable t) {
                        dialog.dismissLoadingDialog();

                    }
                });
            }
            else {
                Toast toast = Toast.makeText(this, "Internet connection lost", Toast.LENGTH_LONG);
                toast.setGravity(Gravity.CENTER, 0, 0);
                toast.show();
            }
        }


    }


}
