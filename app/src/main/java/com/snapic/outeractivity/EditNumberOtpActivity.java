package com.snapic.outeractivity;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseException;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthProvider;
import com.mukesh.OnOtpCompletionListener;
import com.mukesh.OtpView;
import com.snapic.R;
import com.snapic.response.EmailNumberVerifyResponse;
import com.snapic.retrofit.ApiInterface;
import com.snapic.retrofit.RetrofitInit;
import com.snapic.utilities.DialogPopup;
import com.snapic.utilities.InternetCheck;
import com.snapic.utilities.SPreferenceKey;
import com.snapic.utilities.SharedPreferenceWriter;

import java.util.concurrent.TimeUnit;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;
import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class EditNumberOtpActivity extends AppCompatActivity {
    @BindView(R.id.txt_submit)
    TextView txt_submit;
    @BindView(R.id.img_back)
    ImageView img_back;
    String number="";
    String countrycode="";
    String numberonly="";

    @BindView(R.id.tv_num)
    TextView tv_num;
    @BindView(R.id.tv_resend)
    TextView tv_resend;
    private FirebaseAuth mAuth;
    private String verificationCode = "";
    private PhoneAuthProvider.ForceResendingToken mResendToken;
    private OtpView otpView;
    String otpenter;
    private PhoneAuthProvider.OnVerificationStateChangedCallbacks mCallback;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_enter_otp);
        ButterKnife.bind(this);
        Intent i = getIntent();
        number = i.getStringExtra("number");
        countrycode=i.getStringExtra("countrycode");
        numberonly=i.getStringExtra("numberonly");
        txt_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showDialogTPassword();

            }
        });
        img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        mAuth = FirebaseAuth.getInstance();
        txt_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                otpenter=  otpView.getText().toString();
                if(otpenter.length()==6)
                {
                    if (new InternetCheck(EditNumberOtpActivity.this).isConnect()) {
                        verifyVerificationCode(otpenter);
                    }
                    else
                    {
                        Toast.makeText(EditNumberOtpActivity.this, R.string.in, Toast.LENGTH_SHORT).show();
                    }

                }
                else {
                    Toast.makeText(EditNumberOtpActivity.this, R.string.enter_code, Toast.LENGTH_SHORT).show();

                }

            }
        });
        img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        otpView=findViewById(R.id.otp_view);
        tv_num.setText(countrycode+""+numberonly);

        otpView.setOtpCompletionListener(new OnOtpCompletionListener() {
            @Override
            public void onOtpCompleted(String otp) {
                otpenter = otp;

            }
        });
        tv_resend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                fireBaseAction();
            }
        });

        //    showDialogTPassword();
        fireBaseAction();
    }
    private void showDialogTPassword() {

        ConstraintLayout constraintLayout;
        TextView txt_login;
        final Dialog dialogTyreDetail = new Dialog(EditNumberOtpActivity.this, android.R.style.Theme_Black_NoTitleBar);
        dialogTyreDetail.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogTyreDetail.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialogTyreDetail.setContentView(R.layout.dialog_mobile_update_popup);
        dialogTyreDetail.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        dialogTyreDetail.setCanceledOnTouchOutside(true);
        txt_login=dialogTyreDetail.findViewById(R.id.txt_login);
        txt_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(EditNumberOtpActivity.this, EditProfileActivity.class);

                startActivity(i);
            }
        });




        dialogTyreDetail.show();


    }
    private void fireBaseAction() {
        StartFireBaseLogin();
        PhoneAuthProvider.getInstance().verifyPhoneNumber(
                number,// Phone number to verify
                60, // Timeout duration
                TimeUnit.SECONDS, // Unit of timeout
                this, // Activity (for callback binding)
                mCallback);// OnVerificationStateChangedCallback
    }

    private void StartFireBaseLogin() {
        mCallback = new PhoneAuthProvider.OnVerificationStateChangedCallbacks() {
            @Override
            public void onVerificationCompleted(PhoneAuthCredential phoneAuthCredential) {
                String otp = phoneAuthCredential.getSmsCode();

                if (otp != null) {
                    try {
                        otpView.setText(otp);

                        //verifying the code
                        verifyVerificationCode(otp);
                    }
                    catch (Exception e)
                    {

                    }

                }
                Log.d("", "" + otp);
            }

            @Override
            public void onVerificationFailed(FirebaseException e) {
                Toast.makeText(EditNumberOtpActivity.this, R.string.faileds, Toast.LENGTH_SHORT).show();

            }

            @Override
            public void onCodeSent(String s, PhoneAuthProvider.ForceResendingToken forceResendingToken) {
                super.onCodeSent(s, forceResendingToken);
                verificationCode = s;
                mResendToken = forceResendingToken;
                Log.e("OTP Code", s);
                Toast.makeText(EditNumberOtpActivity.this, R.string.sends_codes, Toast.LENGTH_SHORT).show();
            }
        };
    }




    void verifyVerificationCode(String otp) {
        DialogPopup dialog = new DialogPopup(this);
        dialog.showLoadingDialog(this, "");
        PhoneAuthCredential credential = PhoneAuthProvider.getCredential(verificationCode, otp);
        SignInWithPhone(credential);
    }

    private void SignInWithPhone(PhoneAuthCredential credential) {
        DialogPopup dialog = new DialogPopup(this);

        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            Toast.makeText(EditNumberOtpActivity.this, "Verified CODE", Toast.LENGTH_SHORT).show();
                            dialog.dismissLoadingDialog();
                            serviceChangeMobileNumber();
//                            Intent i=new Intent(SignupOtpActivity.this,UserNameActivity.class);
//                            i.putExtra("number",number);
//                            i.putExtra("countrycode",countrycode);
//                            i.putExtra("numberonly",numberonly);
//
//                            SignupOtpActivity.this.startActivity(i);

                            //    serviceSignUp();
                        } else {
                            Toast.makeText(EditNumberOtpActivity.this, "Incorrect CODE", Toast.LENGTH_SHORT).show();
                            dialog.dismissLoadingDialog();
                        }
                    }
                });
    }

    private void serviceChangeMobileNumber() {
        {



            if (new InternetCheck(this).isConnect()) {
                String key = SharedPreferenceWriter.getInstance(EditNumberOtpActivity.this).getString(SPreferenceKey.jwtToken);
                String id = SharedPreferenceWriter.getInstance(EditNumberOtpActivity.this).getString(SPreferenceKey._id);
                String token="Token"+" "+key;
                //aray1@gmail.com,arya8055
                DialogPopup dialog = new DialogPopup(this);
                dialog.showLoadingDialog(this, "");

                ApiInterface api_service = RetrofitInit.getConnect().createConnection();
                Call<EmailNumberVerifyResponse> call = api_service.changeNumberResponse(token,id,countrycode,numberonly);
                call.enqueue(new Callback<EmailNumberVerifyResponse>() {
                    @Override
                    public void onResponse(Call<EmailNumberVerifyResponse> call, Response<EmailNumberVerifyResponse> response) {
                        if (response.isSuccessful()) {
                            EmailNumberVerifyResponse server_response = response.body();
                            if (server_response.getStatus() == 200) {
                                dialog.dismissLoadingDialog();
                                showDialogTPassword();

                            }  else {
                                dialog.dismissLoadingDialog();

                                Toast.makeText(EditNumberOtpActivity.this, server_response.getMsg(), Toast.LENGTH_SHORT).show();
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<EmailNumberVerifyResponse> call, Throwable t) {
                        dialog.dismissLoadingDialog();

                    }
                });
            } else {
                Toast toast = Toast.makeText(this, "Internet connection lost", Toast.LENGTH_LONG);
                toast.setGravity(Gravity.CENTER, 0, 0);
                toast.show();
            }
        }


    }


}
