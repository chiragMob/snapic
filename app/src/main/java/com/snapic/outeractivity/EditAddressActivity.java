package com.snapic.outeractivity;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.hbb20.CountryCodePicker;
import com.snapic.R;
import com.snapic.async.AddressPickerAct;
import com.snapic.response.AddAddressResponse;
import com.snapic.response.AddressDetailResponse;
import com.snapic.retrofit.ApiInterface;
import com.snapic.retrofit.RetrofitInit;
import com.snapic.utilities.DialogPopup;
import com.snapic.utilities.InternetCheck;
import com.snapic.utilities.SPreferenceKey;
import com.snapic.utilities.SharedPreferenceWriter;
import com.snapic.utility.GPSTracker;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.app.ActivityCompat;
import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class EditAddressActivity extends AppCompatActivity implements OnMapReadyCallback {
    @BindView(R.id.img_back)
    ImageView img_back;
    @BindView(R.id.tv_save)
    TextView tv_save;
    @BindView(R.id.et_name)
    EditText et_name;
    @BindView(R.id.et_street)
    EditText et_street;
    @BindView(R.id.et_build)
    EditText et_build;
    @BindView(R.id.et_appoint)
    EditText et_appoint;
    @BindView(R.id.et_city)
    EditText et_city;
    @BindView(R.id.et_num)
    EditText et_num;
    @BindView(R.id.rd_home)
    RadioButton rd_home;
    @BindView(R.id.rd_offi)
    RadioButton rd_offi;
    @BindView(R.id.checkBox)
    CheckBox checkBox;
    @BindView(R.id.countryCodePicker)
    CountryCodePicker countryCodePicker;
    String countrycode = "";
    @BindView(R.id.et_num1)
    ConstraintLayout et_num1;
    @BindView(R.id.tv_add)
    TextView tv_add;

    @BindView(R.id.et_near)
    EditText et_near;
    String id;
    GoogleMap map;
    GPSTracker gpsTracker;
    private double Lat = 0, Long = 0;
    private double dropLat = 0, dropLong = 0;
    String droplat, droplong;
    String lat = "", lang = "";
    private static final int SOURCE_LOCATION = 201;
    String prmo = "";
    ArrayList<String> imagePathList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_add);
        ButterKnife.bind(this);
        Intent i = getIntent();
        id = i.getStringExtra("id");
        prmo = i.getStringExtra("promos");
        if (getIntent().getStringArrayListExtra("test") != null) {

            imagePathList = getIntent().getStringArrayListExtra("test");
        }
        ButterKnife.bind(this);
        img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        et_num1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(EditAddressActivity.this, AddressPickerAct.class);
                startActivityForResult(intent, SOURCE_LOCATION);
            }
        });
        tv_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String number = et_name.getText().toString().trim();
                String street = et_street.getText().toString().trim();
                String buld = et_build.getText().toString().trim();
                String appoint = et_appoint.getText().toString().trim();
                String city = et_city.getText().toString().trim();
                String name = et_num.getText().toString().trim();
                String nearplace = et_near.getText().toString();


                if (TextUtils.isEmpty(number)) {
                    et_name.setFocusable(true);
                    et_name.requestFocus();
                    Toast.makeText(EditAddressActivity.this, R.string.number, Toast.LENGTH_SHORT).show();
                } else if (number.length() < 6 || number.length() > 15) {
                    et_name.setFocusable(true);
                    et_name.requestFocus();
                    Toast.makeText(EditAddressActivity.this, R.string.validsnumber, Toast.LENGTH_SHORT).show();


                } else if (tv_add.getText().toString().isEmpty()) {
                    if (TextUtils.isEmpty(street)) {
                        et_street.setFocusable(true);
                        et_street.requestFocus();
                        Toast.makeText(EditAddressActivity.this, R.string.streets, Toast.LENGTH_SHORT).show();
                    } else if (TextUtils.isEmpty(buld)) {
                        et_build.setFocusable(true);
                        et_build.requestFocus();
                        Toast.makeText(EditAddressActivity.this, R.string.builds, Toast.LENGTH_SHORT).show();
                    } else if (TextUtils.isEmpty(appoint)) {
                        et_appoint.setFocusable(true);
                        et_appoint.requestFocus();
                        Toast.makeText(EditAddressActivity.this, R.string.appoints, Toast.LENGTH_SHORT).show();
                    } else if (TextUtils.isEmpty(city)) {
                        et_city.setFocusable(true);
                        et_city.requestFocus();
                        Toast.makeText(EditAddressActivity.this, R.string.citys, Toast.LENGTH_SHORT).show();
                    } else if (TextUtils.isEmpty(name)) {
                        et_num.setFocusable(true);
                        et_num.requestFocus();
                        Toast.makeText(EditAddressActivity.this, R.string.names, Toast.LENGTH_SHORT).show();
                    }
                    else if (TextUtils.isEmpty(nearplace)) {
                        Toast.makeText(EditAddressActivity.this, "Please enter near place", Toast.LENGTH_SHORT).show();

                    }
                    else if (!rd_home.isChecked() && !rd_offi.isChecked()) {
                        Toast.makeText(EditAddressActivity.this, R.string.checksa, Toast.LENGTH_SHORT).show();

                    } else {
                        serviceAddresss();
                    }
                } else if (TextUtils.isEmpty(name)) {
                    et_num.setFocusable(true);
                    et_num.requestFocus();
                    Toast.makeText(EditAddressActivity.this, R.string.namesss, Toast.LENGTH_SHORT).show();
                }

                else if (TextUtils.isEmpty(nearplace)) {
                    Toast.makeText(EditAddressActivity.this, "Please enter near place", Toast.LENGTH_SHORT).show();

                }
                else if (!rd_home.isChecked() && !rd_offi.isChecked()) {
                    Toast.makeText(EditAddressActivity.this, R.string.checksss, Toast.LENGTH_SHORT).show();

                } else {
                    serviceAddresss();
                }
            }
        });
        addressDetail();
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.addressMap);

        mapFragment.getMapAsync(this::onMapReady);

        this.gpsTracker = new GPSTracker(this);
        Lat = gpsTracker.getLatitude();
        Long = gpsTracker.getLongitude();


    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == SOURCE_LOCATION && data != null) {
            String address = data.getStringExtra("ADDRESS");
            lat = data.getStringExtra("LAT");
            lang = data.getStringExtra("LONG");
            Lat = Double.parseDouble(lat);
            Long = Double.parseDouble(lang);


            getCurrentLocations(Lat, Long);
        }
    }

    private void getCurrentLocations(double Lat, double Long) {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        map.setMyLocationEnabled(false);
        map.getUiSettings().setMyLocationButtonEnabled(false);
        map.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(Lat, Long), 16.0f));
        map.addMarker(new MarkerOptions().position(new LatLng(Lat,Long))
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.map_pin)));
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }




//        map.setOnCameraChangeListener(new GoogleMap.OnCameraChangeListener() {
//            @Override
//            public void onCameraChange(CameraPosition cameraPosition) {
//                mCenterLatLong = cameraPosition.target;
//
//
//
//
//                try {
//
//                    val++;
//                    if(val>=2) {
//                        if(Lat!=mCenterLatLong.latitude&&Long!=mCenterLatLong.longitude)
//                        {
//                            Intent intent = new Intent(UserHomeActivity.this, AddressPickerAct.class);
//                            startActivityForResult(intent, SOURCE_LOCATION);
//                        }
//
//                    }
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
//            }
//        });
        List<Address> addresses = null;
        try {
            Geocoder geocoder = new Geocoder(this, Locale.getDefault());
            addresses = geocoder.getFromLocation(Lat, Long, 1);
        } catch (IOException e) {
            e.printStackTrace();
        }
        if (addresses != null && addresses.size() > 0) {
            String address = addresses.get(0).getAddressLine(0);
            tv_add.setText(address);
// If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
        }
    }

    private void getCurrentLocation() {
        Lat = gpsTracker.getLatitude();
        Long = gpsTracker.getLongitude();
        map.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(Lat, Long), 16.0f));

        map.addMarker(new MarkerOptions().position(new LatLng(gpsTracker.getLatitude(),gpsTracker.getLongitude()))
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.map_pin)).anchor(0.5f, 0.5f));
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            return;
        }
        map.setMyLocationEnabled(false);
        map.getUiSettings().setMyLocationButtonEnabled(false);


        List<Address> addresses = null;
        try {
            Geocoder geocoder = new Geocoder(EditAddressActivity.this, Locale.getDefault());
            addresses = geocoder.getFromLocation(Lat, Long, 1);
        } catch (IOException e) {
            e.printStackTrace();
        }
        if (addresses != null && addresses.size() > 0) {
            String address = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
        }



//        map.setOnCameraChangeListener(new GoogleMap.OnCameraChangeListener() {
//            @Override
//            public void onCameraChange(CameraPosition cameraPosition) {
//                mCenterLatLong = cameraPosition.target;
//
//
//
//
//                try {
//
//
//                    val++;
////if(val>=2) {
////    if(Lat!=mCenterLatLong.latitude&&Long!=mCenterLatLong.longitude)
////    {
////        Intent intent = new Intent(UserHomeActivity.this, AddressPickerAct.class);
////        startActivityForResult(intent, SOURCE_LOCATION);
////    }
////
////}
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
//            }
//        });

    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        map= googleMap;
        getCurrentLocation();
    }
    private void addressDetail() {
        {
            String key = SharedPreferenceWriter.getInstance(EditAddressActivity.this).getString(SPreferenceKey.jwtToken);
            String token="Token"+" "+key;

                if (new InternetCheck(EditAddressActivity.this).isConnect()) {

                    //aray1@gmail.com,arya8055
                    DialogPopup dialog = new DialogPopup(EditAddressActivity.this);
                    dialog.showLoadingDialog(EditAddressActivity.this, "");
                    System.out.print(key);
                    System.out.print(id);

                    ApiInterface api_service = RetrofitInit.getConnect().createConnection();
                    Call<AddressDetailResponse> call = api_service.getProductDetails(token,id);
                    call.enqueue(new Callback<AddressDetailResponse>() {
                        @Override
                        public void onResponse(Call<AddressDetailResponse> call, Response<AddressDetailResponse> response) {
                            if (response.isSuccessful()) {
                                AddressDetailResponse server_response = response.body();
                                if (server_response.getStatus() == 200) {
                                    dialog.dismissLoadingDialog();
                                    String abc;
                                    et_num.setText(server_response.getDetail().getName());
                                    ;
                                    et_build.setText(server_response.getDetail().getBuilding());
                                    et_appoint.setText(server_response.getDetail().getApartment());
                                    et_name.setText(server_response.getDetail().getPhone_number());
                                    et_street.setText(server_response.getDetail().getStreet());
                                    et_city.setText(server_response.getDetail().getCity());
                                    tv_add.setText(server_response.getDetail().getNearest_point());
                                    et_near.setText(server_response.getDetail().getNearest_place());
                                    if(server_response.getDetail().getLat().isEmpty())
                                    {

                                    }
                                    else
                                    {
                                        getCurrentLocations(Double.parseDouble(server_response.getDetail().getLat()),Double.parseDouble(server_response.getDetail().getLongX()));

                                    }

                                    if(server_response.getDetail().getTag().equals("Home")) {
    rd_home.setChecked(true);
}
else {
    rd_offi.setChecked(true);
}

                                    //  setPreferences(server_response);


                                } else if (server_response.getStatus() == 400) {
                                    dialog.dismissLoadingDialog();

                                } else if (server_response.getStatus() == 500) {
                                    dialog.dismissLoadingDialog();

                                    //   startActivity(new Intent(DonationDetailsActivity.this, LoginActivity.class));
                                } else {
                                    dialog.dismissLoadingDialog();

                                    Toast.makeText(EditAddressActivity.this, server_response.getMessage(), Toast.LENGTH_SHORT).show();
                                }
                            }
                        }

                        @Override
                        public void onFailure(Call<AddressDetailResponse> call, Throwable t) {
                            dialog.dismissLoadingDialog();

                        }
                    });
                } else {
                    Toast toast = Toast.makeText(EditAddressActivity.this, R.string.in, Toast.LENGTH_LONG);
                    toast.setGravity(Gravity.CENTER, 0, 0);
                    toast.show();
                }
            }
        }

    private void serviceAddresss() {
        {
            String key = SharedPreferenceWriter.getInstance(EditAddressActivity.this).getString(SPreferenceKey.jwtToken);
            String token="Token"+" "+key;
            String deviceToken = SharedPreferenceWriter.getInstance(this).getString(SPreferenceKey.DEVICETOKEN);
            String ids = SharedPreferenceWriter.getInstance(this).getString(SPreferenceKey._id);
            String nearplace = et_near.getText().toString();

            countrycode=  countryCodePicker.getSelectedCountryCodeWithPlus();
            String tag="";
            if(rd_home.isChecked())
            {
                tag=rd_home.getText().toString();
            }
            else if(rd_offi.isChecked())
            {
                tag=rd_offi.getText().toString();
            }
            String value="";
            if(checkBox.isChecked())
            {
                value="true";
            }
            else
            {
                value="false";

            }

            if (new InternetCheck(this).isConnect()) {
                //aray1@gmail.com,arya8055
                DialogPopup dialog = new DialogPopup(this);
                dialog.showLoadingDialog(this, "");

                ApiInterface api_service = RetrofitInit.getConnect().createConnection();
                Call<AddAddressResponse> call = api_service.updateAddress(token,id,et_num.getText().toString().trim(),et_street.getText().toString().trim(),et_build.getText().toString().trim(),et_appoint.getText().toString().trim(),et_city.getText().toString().trim(),countrycode,et_name.getText().toString().trim(),tag,tv_add.getText().toString(),lat,lang,value,et_near.getText().toString());
                call.enqueue(new Callback<AddAddressResponse>() {
                    @Override
                    public void onResponse(Call<AddAddressResponse> call, Response<AddAddressResponse> response) {
                        if (response.isSuccessful()) {
                            AddAddressResponse server_response = response.body();
                            if (server_response.getStatus() == 200) {
                                dialog.dismissLoadingDialog();
                                Intent i = new Intent(EditAddressActivity.this, MyAddressActivity.class);
                                i.putExtra("promos", prmo);

                                i.putStringArrayListExtra("test", imagePathList);




                                EditAddressActivity.this.startActivity(i);


                                finish();

                            }  else {
                                dialog.dismissLoadingDialog();

                                Toast.makeText(EditAddressActivity.this,String.valueOf(server_response.getStatus()), Toast.LENGTH_SHORT).show();
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<AddAddressResponse> call, Throwable t) {
                        dialog.dismissLoadingDialog();

                    }
                });
            } else {
                Toast toast = Toast.makeText(this, R.string.in, Toast.LENGTH_LONG);
                toast.setGravity(Gravity.CENTER, 0, 0);
                toast.show();
            }
        }


    }



}


