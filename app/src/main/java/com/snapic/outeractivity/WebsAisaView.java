package com.snapic.outeractivity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.Gravity;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;


import com.snapic.R;
import com.snapic.activity.HomeActivity;
import com.snapic.response.PaymentDoneResponse;
import com.snapic.retrofit.ApiInterface;
import com.snapic.retrofit.RetrofitInit;
import com.snapic.utilities.DialogPopup;
import com.snapic.utilities.InternetCheck;
import com.snapic.utilities.SPreferenceKey;
import com.snapic.utilities.SharedPreferenceWriter;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import androidx.appcompat.app.AppCompatActivity;
import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class WebsAisaView extends AppCompatActivity {

    @BindView(R.id.webUrl)
    WebView webUrl;
    String mobilenumber, amoount, purpuse, email, donationid;
    String supid, sellerid;
    String data;
    String appointdate;
    String ticketid, quantity;
    String Appoinid, deliverdate, addressid;
    String total="";
    String url="";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_web_view);
        ButterKnife.bind(this);
        String postData;
        String merchantKey = "3799f50c8adf2acf298995e9335584f355fb6ec83ba9a46d";
        String webURL = "https://pay.asiahawala.net/payments/";
        String amount = "10000";
        String currency = "IQD";
        String language = "en_US";
        String transaction = "1234512";
        String notifyURL= "https://www.google.com";
        getIntentData();



            postData="merchantkey=" + merchantKey  + "&currency_code=" + currency + "&amount=" +  amount + "&transactionid=" +
                    transaction +"&lc=" +  language + "&notify_url=" + notifyURL;

        DialogPopup dialog = new DialogPopup(WebsAisaView.this);
        dialog.showLoadingDialog(WebsAisaView.this, "");
        webUrl.setWebViewClient(new WebViewClient() {
            @Override
            public void onPageFinished(WebView view, String url) {
                DialogPopup dialog = new DialogPopup(WebsAisaView.this);
                dialog.dismissLoadingDialog();
                super.onPageFinished(view, url);

                try {
                    if (url.contains("https://snapicapp.com/?")) {
                        String status = url.split("=")[3].split("&")[0];

                        if(status.matches("TS"))
                        {
                            String transid = url.split("=")[2].split("&")[0];
                            String amount = url.split("=")[4].split("&")[0];
                            String orderid = url.split("=")[1].split("&")[0];

                            servicePayment(orderid,amount,transid,"na");

                        }
                        else
                        {
                            Toast.makeText(WebsAisaView.this, "Transaction Failed!", Toast.LENGTH_SHORT).show();
                            onBackPressed();

                        }

                 //       servicePayment(orderid,paymentid,"na");

                        //  paymentDetail(mobilenumber,amoount,purpuse,paymentId,email);
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                super.onPageStarted(view, url, favicon);

                //    CommonUtilities.showLoadingDialog(WebViewAct.this);
            }

        });




    }
    public String convertToString(InputStream inputStream){
        StringBuffer string = new StringBuffer();
        BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
        String line="";
        try {
            while ((line = reader.readLine()) != null) {
                string.append(line + "\n");
            }
        } catch (Exception e) {}
        return string.toString();
    }





    private void getIntentData() {
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            // set title of toolbar
            Intent i = getIntent();
            url = i.getStringExtra("url");
            webUrl.clearView();
            webUrl.setInitialScale(50);
            webUrl.getSettings().setJavaScriptEnabled(true);
            webUrl.setHorizontalScrollBarEnabled(true);
            webUrl.getSettings().setUseWideViewPort(true);
            webUrl.getSettings().setDomStorageEnabled(true);
            webUrl.loadUrl(url);

            webUrl.getSettings().setJavaScriptEnabled(true);
            webUrl.setHorizontalScrollBarEnabled(true);

            //   webUrl.getSettings().setDomStorageEnabled(true);
            webUrl.getSettings().setUseWideViewPort(true);




        }

    }


    private void servicePayment(String orderid,String amount,String paymentid,String operatedid) {
        {
            if (new InternetCheck(this).isConnect()) {
                //aray1@gmail.com,arya8055
                String key = SharedPreferenceWriter.getInstance(WebsAisaView.this).getString(SPreferenceKey.jwtToken);
                String token="Token"+" "+key;
                DialogPopup dialog = new DialogPopup(this);
                dialog.showLoadingDialog(this, "");
                String date = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault()).format(new Date());

                ApiInterface api_service = RetrofitInit.getConnect().createConnection();
                Call<PaymentDoneResponse> call = api_service.paymentDone(token,orderid,paymentid,amount,"asia hawala",date,operatedid);
                call.enqueue(new Callback<PaymentDoneResponse>() {
                    @Override
                    public void onResponse(Call<PaymentDoneResponse> call, Response<PaymentDoneResponse> response) {
                        if (response.isSuccessful()) {
                            PaymentDoneResponse server_response = response.body();
                            if (server_response.getStatus() == 200) {
                                dialog.dismissLoadingDialog();
                                SharedPreferenceWriter.getInstance(WebsAisaView.this).writeStringValue(SPreferenceKey.id1,"");
                                SharedPreferenceWriter.getInstance(WebsAisaView.this).writeStringValue(SPreferenceKey.id2,"");
                                SharedPreferenceWriter.getInstance(WebsAisaView.this).writeStringValue(SPreferenceKey.id3,"");
                                SharedPreferenceWriter.getInstance(WebsAisaView.this).writeStringValue(SPreferenceKey.id4,"");
                                SharedPreferenceWriter.getInstance(WebsAisaView.this).writeStringValue(SPreferenceKey.id5,"");
                                SharedPreferenceWriter.getInstance(WebsAisaView.this).writeStringValue(SPreferenceKey.id6,"");
                                SharedPreferenceWriter.getInstance(WebsAisaView.this).writeStringValue(SPreferenceKey.id7,"");
                                SharedPreferenceWriter.getInstance(WebsAisaView.this).writeStringValue(SPreferenceKey.id8,"");
                                SharedPreferenceWriter.getInstance(WebsAisaView.this).writeStringValue(SPreferenceKey.id9,"");
                                SharedPreferenceWriter.getInstance(WebsAisaView.this).writeStringValue(SPreferenceKey.id10,"");


                                startActivity(new Intent(WebsAisaView.this, HomeActivity.class));
                            }  else {
                                dialog.dismissLoadingDialog();
                                Toast.makeText(WebsAisaView.this, ""+server_response.getMessage(), Toast.LENGTH_SHORT).show();
                                startActivity(new Intent(WebsAisaView.this, HomeActivity.class));

                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<PaymentDoneResponse> call, Throwable t) {
                        dialog.dismissLoadingDialog();

                    }
                });
            } else {
                Toast toast = Toast.makeText(this, "Internet connection lost", Toast.LENGTH_LONG);
                toast.setGravity(Gravity.CENTER, 0, 0);
                toast.show();
            }
        }


    }

//    private void serviceTicketBooking(String paymentid) {
//        {
//
//            if (new InternetCheck(WebAisaView.this).isConnect()) {
//                String key = SharedPreferenceWriter.getInstance(WebAisaView.this).getString(SPreferenceKey.jwtToken);
//
//                //aray1@gmail.com,arya8055
//                DialogPopup dialog = new DialogPopup(WebAisaView.this);
//                dialog.showLoadingDialog(WebAisaView.this, "");
//
//                ApiInterface api_service = RetrofitInit.getConnect().createConnection();
//                Call<TicketBookingResponse> call = api_service.
//                        ticketBooking(key, "application/x-www-form-urlencoded", ticketid, amoount, "Card", paymentid, "Success", "0", amoount, quantity);
//                call.enqueue(new Callback<TicketBookingResponse>() {
//                    @Override
//                    public void onResponse(Call<TicketBookingResponse> call, Response<TicketBookingResponse> response) {
//                        if (response.isSuccessful()) {
//                            TicketBookingResponse server_response = response.body();
//                            if (server_response.getStatus() == 200) {
//                                dialog.dismissLoadingDialog();
//                                //  setPreferences(server_response);
//
//                                Toast.makeText(WebAisaView.this, "Ticket is successfully purchased. Your booking is confirmed.", Toast.LENGTH_SHORT).show();
//
//                                Intent i = new Intent(WebAisaView.this, HomeActivity.class);
//                                i.putExtra("id", "");
//
//                                WebAisaView.this.startActivity(i);
//
//
//                            } else if (server_response.getStatus() == 400) {
//                                dialog.dismissLoadingDialog();
//
//                            } else if (server_response.getStatus() == 500) {
//                                dialog.dismissLoadingDialog();
//
//                                startActivity(new Intent(WebAisaView.this, LoginActivity.class));
//                            } else {
//                                dialog.dismissLoadingDialog();
//
//                                Toast.makeText(WebAisaView.this, server_response.getMessage(), Toast.LENGTH_SHORT).show();
//                            }
//                        }
//                    }
//
//                    @Override
//                    public void onFailure(Call<TicketBookingResponse> call, Throwable t) {
//                        dialog.dismissLoadingDialog();
//
//                    }
//                });
//            } else {
//                Toast toast = Toast.makeText(WebAisaView.this, "Internet connection lost", Toast.LENGTH_LONG);
//                toast.setGravity(Gravity.CENTER, 0, 0);
//                toast.show();
//            }
//        }
//    }




}



