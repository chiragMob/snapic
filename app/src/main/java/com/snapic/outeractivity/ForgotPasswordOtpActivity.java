package com.snapic.outeractivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseException;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthProvider;
import com.mukesh.OnOtpCompletionListener;
import com.mukesh.OtpView;
import com.snapic.R;
import com.snapic.utilities.DialogPopup;
import com.snapic.utilities.InternetCheck;

import java.util.concurrent.TimeUnit;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import butterknife.BindView;
import butterknife.ButterKnife;


public class ForgotPasswordOtpActivity extends AppCompatActivity {
    @BindView(R.id.txt_submit)
    TextView txt_submit;
    @BindView(R.id.img_back)
    ImageView img_back;
    @BindView(R.id.tv_num)
    TextView tv_num;
    @BindView(R.id.tv_resend)
    TextView tv_resend;
    private FirebaseAuth mAuth;
    private PhoneAuthProvider.OnVerificationStateChangedCallbacks mCallback;
    private String verificationCode = "";
    private PhoneAuthProvider.ForceResendingToken mResendToken;
    private OtpView otpView;
    String otpenter;
    String number="";
    String countrycode="";
    String numberonly="";
    String firstname="";
    String lastname="";
    String promo="";
    String password="";
    String email="";
    int val=0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_enter_otp);
        ButterKnife.bind(this);
        Intent i = getIntent();
        number = i.getStringExtra("number");
        countrycode=i.getStringExtra("countrycode");
        numberonly=i.getStringExtra("numberonly");





        mAuth = FirebaseAuth.getInstance();
        txt_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                otpenter=  otpView.getText().toString();
                if(otpenter.length()==6)
                {
                    if (new InternetCheck(ForgotPasswordOtpActivity.this).isConnect()) {
                        verifyVerificationCode(otpenter);
                    }
                    else
                    {
                        Toast.makeText(ForgotPasswordOtpActivity.this, R.string.in, Toast.LENGTH_SHORT).show();
                    }

                }
                else {
                    Toast.makeText(ForgotPasswordOtpActivity.this, R.string.valid, Toast.LENGTH_SHORT).show();

                }

            }
        });
        img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        otpView=findViewById(R.id.otp_view);
        tv_num.setText(countrycode+""+numberonly);

        otpView.setOtpCompletionListener(new OnOtpCompletionListener() {
            @Override
            public void onOtpCompleted(String otp) {
                otpenter = otp;

            }
        });
        tv_resend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                fireBaseAction();
            }
        });

        //    showDialogTPassword();
        fireBaseAction();
    }

    private void fireBaseAction() {
        StartFireBaseLogin();
        PhoneAuthProvider.getInstance().verifyPhoneNumber(
                number,// Phone number to verify
                60, // Timeout duration
                TimeUnit.SECONDS, // Unit of timeout
                this, // Activity (for callback binding)
                mCallback);// OnVerificationStateChangedCallback
    }

    private void StartFireBaseLogin() {
        mCallback = new PhoneAuthProvider.OnVerificationStateChangedCallbacks() {
            @Override
            public void onVerificationCompleted(PhoneAuthCredential phoneAuthCredential) {
                String otp = phoneAuthCredential.getSmsCode();

                if (otp != null) {
                    try {
                        otpView.setText(otp);

                        //verifying the code
                        verifyVerificationCode(otp);
                    }
                    catch (Exception e)
                    {

                    }

                }
                Log.d("", "" + otp);
            }

            @Override
            public void onVerificationFailed(FirebaseException e) {
                Toast.makeText(ForgotPasswordOtpActivity.this, R.string.verfiys, Toast.LENGTH_SHORT).show();

            }

            @Override
            public void onCodeSent(String s, PhoneAuthProvider.ForceResendingToken forceResendingToken) {
                super.onCodeSent(s, forceResendingToken);
                verificationCode = s;
                mResendToken = forceResendingToken;
                Log.e("OTP Code", s);
                Toast.makeText(ForgotPasswordOtpActivity.this, R.string.Sending, Toast.LENGTH_SHORT).show();
            }
        };
    }




    void verifyVerificationCode(String otp) {
        DialogPopup dialog = new DialogPopup(this);
        dialog.showLoadingDialog(this, "");
        PhoneAuthCredential credential = PhoneAuthProvider.getCredential(verificationCode, otp);
        SignInWithPhone(credential);
    }

    private void SignInWithPhone(PhoneAuthCredential credential) {
        DialogPopup dialog = new DialogPopup(this);

        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            Toast.makeText(ForgotPasswordOtpActivity.this, R.string.verified, Toast.LENGTH_SHORT).show();
                            dialog.dismissLoadingDialog();
                            Intent i=new Intent(ForgotPasswordOtpActivity.this,ResetPasswordActivity.class);

                            i.putExtra("email",numberonly);

                            ForgotPasswordOtpActivity.this.startActivity(i);
                        //    serviceSignUp();
//                            Intent i=new Intent(SignupOtpActivity.this,UserNameActivity.class);
//                            i.putExtra("number",number);
//                            i.putExtra("countrycode",countrycode);
//                            i.putExtra("numberonly",numberonly);
//
//                            SignupOtpActivity.this.startActivity(i);

                            //    serviceSignUp();
                        } else {
                            Toast.makeText(ForgotPasswordOtpActivity.this, R.string.incorrect, Toast.LENGTH_SHORT).show();
                            dialog.dismissLoadingDialog();
                        }
                    }
                });
    }


}

