package com.snapic.outeractivity;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseException;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthProvider;
import com.mukesh.OnOtpCompletionListener;
import com.mukesh.OtpView;
import com.snapic.R;
import com.snapic.response.SignUpResponse;
import com.snapic.retrofit.ApiInterface;
import com.snapic.retrofit.RetrofitInit;
import com.snapic.utilities.DialogPopup;
import com.snapic.utilities.InternetCheck;
import com.snapic.utilities.SPreferenceKey;
import com.snapic.utilities.SharedPreferenceWriter;
import com.snapic.welcome.SignFirstActivity;

import java.util.concurrent.TimeUnit;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;
import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class SignUpOtpActivity extends AppCompatActivity {
    @BindView(R.id.txt_submit)
    TextView txt_submit;
    @BindView(R.id.img_back)
    ImageView img_back;
    @BindView(R.id.tv_num)
    TextView tv_num;
    @BindView(R.id.tv_resend)
    TextView tv_resend;
    private FirebaseAuth mAuth;
    private PhoneAuthProvider.OnVerificationStateChangedCallbacks mCallback;
    private String verificationCode = "";
    private PhoneAuthProvider.ForceResendingToken mResendToken;
    private OtpView otpView;
    String otpenter;
    String number="";
    String countrycode="";
    String numberonly="";
    String firstname="";
    String lastname="";
    String promo="";
    String password="";
    String email="";
    int val=0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_enter_otp);
        ButterKnife.bind(this);
        Intent i = getIntent();
        number = i.getStringExtra("number");
        countrycode=i.getStringExtra("countrycode");
        numberonly=i.getStringExtra("numberonly");

        firstname=i.getStringExtra("firstname");
        lastname=i.getStringExtra("lastname");
        promo=i.getStringExtra("promo");
        password=i.getStringExtra("password");
        email=i.getStringExtra("email");



        mAuth = FirebaseAuth.getInstance();
        txt_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                otpenter=  otpView.getText().toString();
                if(otpenter.length()==6)
                {
                    if (new InternetCheck(SignUpOtpActivity.this).isConnect()) {
                        verifyVerificationCode(otpenter);
                    }
                    else
                    {
                        Toast.makeText(SignUpOtpActivity.this, R.string.in, Toast.LENGTH_SHORT).show();
                    }

                }
                else {
                    Toast.makeText(SignUpOtpActivity.this, R.string.valid, Toast.LENGTH_SHORT).show();

                }

            }
        });
        img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        otpView=findViewById(R.id.otp_view);
        tv_num.setText(countrycode+""+numberonly);

        otpView.setOtpCompletionListener(new OnOtpCompletionListener() {
            @Override
            public void onOtpCompleted(String otp) {
                otpenter = otp;

            }
        });
        tv_resend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               fireBaseAction();
            }
        });

    //    showDialogTPassword();
       fireBaseAction();
    }
    private void showDialogTPassword() {
val=1;
        ConstraintLayout constraintLayout;
        TextView txt_login;
        final Dialog dialogTyreDetail = new Dialog(SignUpOtpActivity.this, android.R.style.Theme_Black_NoTitleBar);
        dialogTyreDetail.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogTyreDetail.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialogTyreDetail.setContentView(R.layout.dialog_profile);
        dialogTyreDetail.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        dialogTyreDetail.setCanceledOnTouchOutside(false);
        txt_login=dialogTyreDetail.findViewById(R.id.txt_login);
        txt_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(SignUpOtpActivity.this, SignFirstActivity.class);

                SignUpOtpActivity.this.startActivity(i);
            }
        });


        dialogTyreDetail.setOnKeyListener(new Dialog.OnKeyListener() {

            @Override
            public boolean onKey(DialogInterface arg0, int keyCode,
                                 KeyEvent event) {
                // TODO Auto-generated method stub
                if (keyCode == KeyEvent.KEYCODE_BACK) {
                    finishAffinity();
                }
                return true;
            }
        });





        dialogTyreDetail.show();


    }

    private void fireBaseAction() {
        StartFireBaseLogin();
        PhoneAuthProvider.getInstance().verifyPhoneNumber(
                number,// Phone number to verify
                60, // Timeout duration
                TimeUnit.SECONDS, // Unit of timeout
                this, // Activity (for callback binding)
                mCallback);// OnVerificationStateChangedCallback
    }

    private void StartFireBaseLogin() {
        mCallback = new PhoneAuthProvider.OnVerificationStateChangedCallbacks() {
            @Override
            public void onVerificationCompleted(PhoneAuthCredential phoneAuthCredential) {
                String otp = phoneAuthCredential.getSmsCode();

                if (otp != null) {
                    try {
                        otpView.setText(otp);

                        //verifying the code
                        verifyVerificationCode(otp);
                    }
                    catch (Exception e)
                    {

                    }

                }
                Log.d("", "" + otp);
            }

            @Override
            public void onVerificationFailed(FirebaseException e) {
                Toast.makeText(SignUpOtpActivity.this, R.string.verfy, Toast.LENGTH_SHORT).show();

            }

            @Override
            public void onCodeSent(String s, PhoneAuthProvider.ForceResendingToken forceResendingToken) {
                super.onCodeSent(s, forceResendingToken);
                verificationCode = s;
                mResendToken = forceResendingToken;
                Log.e("OTP Code", s);
                Toast.makeText(SignUpOtpActivity.this, R.string.sending, Toast.LENGTH_SHORT).show();
            }
        };
    }




    void verifyVerificationCode(String otp) {
        DialogPopup dialog = new DialogPopup(this);
        dialog.showLoadingDialog(this, "");
        PhoneAuthCredential credential = PhoneAuthProvider.getCredential(verificationCode, otp);
        SignInWithPhone(credential);
    }

    private void SignInWithPhone(PhoneAuthCredential credential) {
        DialogPopup dialog = new DialogPopup(this);

        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            Toast.makeText(SignUpOtpActivity.this, R.string.verfy, Toast.LENGTH_SHORT).show();
                            dialog.dismissLoadingDialog();
                            serviceSignUp();
//                            Intent i=new Intent(SignupOtpActivity.this,UserNameActivity.class);
//                            i.putExtra("number",number);
//                            i.putExtra("countrycode",countrycode);
//                            i.putExtra("numberonly",numberonly);
//
//                            SignupOtpActivity.this.startActivity(i);

                            //    serviceSignUp();
                        } else {
                            Toast.makeText(SignUpOtpActivity.this, R.string.cod, Toast.LENGTH_SHORT).show();
                            dialog.dismissLoadingDialog();
                        }
                    }
                });
    }
    private void serviceSignUp() {
        {
            if (new InternetCheck(this).isConnect()) {

                String deviceToken = SharedPreferenceWriter.getInstance(this).getString(SPreferenceKey.DEVICETOKEN);

                DialogPopup dialog = new DialogPopup(this);
                dialog.showLoadingDialog(this, "");

                ApiInterface api_service = RetrofitInit.getConnect().createConnection();
                Call<SignUpResponse> call = api_service.signupDetails(email, password,password,firstname,lastname, countrycode, numberonly, promo,deviceToken);
                call.enqueue(new Callback<SignUpResponse>() {
                    @Override
                    public void onResponse(Call<SignUpResponse> call, Response<SignUpResponse> response) {
                        if (response.isSuccessful()) {
                            SignUpResponse server_response = response.body();
                            if (server_response.getStatus() == 200) {
                                dialog.dismissLoadingDialog();
                                //  showDialogchangepass();
                                SharedPreferenceWriter.getInstance(SignUpOtpActivity.this).writeStringValue(SPreferenceKey._id, String.valueOf(server_response.getData().getId()));
                                SharedPreferenceWriter.getInstance(SignUpOtpActivity.this).writeStringValue(SPreferenceKey.user_id_forgot,"");

                                SharedPreferenceWriter.getInstance(SignUpOtpActivity.this).writeStringValue(SPreferenceKey.jwtToken,String.valueOf(server_response.getData().getToken()));

                                Toast.makeText(SignUpOtpActivity.this, R.string.regs, Toast.LENGTH_SHORT).show();
                               showDialogTPassword();
                            } else if (server_response.getStatus() == 400) {
                                dialog.dismissLoadingDialog();


                            } else {
                                dialog.dismissLoadingDialog();

                                Toast.makeText(SignUpOtpActivity.this, server_response.getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<SignUpResponse> call, Throwable t) {
                        dialog.dismissLoadingDialog();

                    }
                });
            } else {
                Toast toast = Toast.makeText(this, R.string.in, Toast.LENGTH_LONG);
                toast.setGravity(Gravity.CENTER, 0, 0);
                toast.show();
            }
        }


    }


}
