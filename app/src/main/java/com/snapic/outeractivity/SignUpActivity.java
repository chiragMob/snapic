package com.snapic.outeractivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.hbb20.CountryCodePicker;
import com.snapic.R;
import com.snapic.response.EmailNumberVerifyResponse;
import com.snapic.retrofit.ApiInterface;
import com.snapic.retrofit.RetrofitInit;
import com.snapic.utilities.InternetCheck;

import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;
import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class SignUpActivity extends AppCompatActivity implements CountryCodePicker.OnCountryChangeListener{
    @BindView(R.id.cons_login)
    ConstraintLayout cons_login;
    @BindView(R.id.tv_sign_up)
    TextView tv_sign_up;
    @BindView(R.id.con_back)
    ConstraintLayout con_back;
    @BindView(R.id.et_fname)
    EditText et_fname;
    @BindView(R.id.et_lname)
    EditText et_lname;

    @BindView(R.id.et_promo)
    EditText et_promo;
    @BindView(R.id.et_p)
    EditText et_p;
    @BindView(R.id.et_con_pass)
    EditText et_con_pass;
    @BindView(R.id.checkBox)
    CheckBox checkBox;
    @BindView(R.id.et_num)
    EditText et_num;
    @BindView(R.id.et_email_id)
    EditText et_email_id;
    @BindView(R.id.countryCodePicker)
    CountryCodePicker countryCodePicker;
    String countrycode="";

    String firstname,lastname,email,password,con_password,promo,mobilenumber;
    String emailPattern = "^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
    final String PASSWORD_PATTERN = "^(?=.*[0-9])(?=.*[A-Z])(?=.*[@#$%^&+=!])(?=\\S+$).{4,}$";



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sing_up);
        ButterKnife.bind(this);
        cons_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(SignUpActivity.this,LoginActivity.class));

            }
        });
        con_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        tv_sign_up.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                validation();


            }
        });
        countryCodePicker.setOnCountryChangeListener(this);

    }

    public boolean validation() {
        firstname = et_fname.getText().toString();
        lastname = et_lname.getText().toString();
        promo = et_promo.getText().toString();
        password = et_p.getText().toString();
        con_password = et_con_pass.getText().toString();
        mobilenumber = et_num.getText().toString();
        email=et_email_id.getText().toString();


        if (TextUtils.isEmpty(firstname)) {
            et_fname.setFocusable(true);
            et_fname.requestFocus();
            Toast.makeText(this, R.string.firstname, Toast.LENGTH_SHORT).show();
            return false;

        }
        else if (et_fname.getText().toString().trim().isEmpty()) {
            et_fname.setFocusable(true);
            et_fname.requestFocus();
            Toast.makeText(this, R.string.first, Toast.LENGTH_SHORT).show();
            return false;

        }

        else if (TextUtils.isEmpty(lastname)) {
            et_lname.setFocusable(true);
            et_lname.requestFocus();
            Toast.makeText(this, R.string.ll, Toast.LENGTH_SHORT).show();
            return false;

        }
        else if (et_lname.getText().toString().trim().isEmpty()) {
            et_lname.setFocusable(true);
            et_lname.requestFocus();
            Toast.makeText(this, R.string.ll, Toast.LENGTH_SHORT).show();
            return false;

        }
        else if (TextUtils.isEmpty(email)) {
            et_email_id.setFocusable(true);
            et_email_id.requestFocus();
            Toast.makeText(this, R.string.emails, Toast.LENGTH_SHORT).show();
            return false;

        } else if (!email.matches(emailPattern)) {
            et_email_id.setFocusable(true);
            et_email_id.requestFocus();
            Toast.makeText(this, R.string.emails, Toast.LENGTH_SHORT).show();

            return false;
        }

        else if (TextUtils.isEmpty(mobilenumber)) {
            et_num.setFocusable(true);
            et_num.requestFocus();
            Toast.makeText(this, R.string.number, Toast.LENGTH_SHORT).show();
            return false;

        } else if (mobilenumber.length() < 6 || mobilenumber.length() > 15) {
            et_num.setFocusable(true);
            et_num.requestFocus();
            Toast.makeText(this, R.string.number, Toast.LENGTH_SHORT).show();
            return false;

        }   else if (TextUtils.isEmpty(password)) {
            et_p.setFocusable(true);
            et_p.requestFocus();
            Toast.makeText(this, R.string.pass, Toast.LENGTH_SHORT).show();
            return false;

        } else if (et_p.getText().toString().length() <= 6 || et_p.getText().toString().length() >= 16) {
            Toast.makeText(this, R.string.betw, Toast.LENGTH_SHORT).show();
            return false;
        } else if (password.length() >= 6 && password.length() <= 16 && !password.matches(PASSWORD_PATTERN)) {
            Toast.makeText(this, R.string.comb, Toast.LENGTH_SHORT).show();
            return false;
        } else if (TextUtils.isEmpty(con_password)) {
            et_con_pass.setFocusable(true);
            et_con_pass.requestFocus();
            Toast.makeText(this, R.string.conf, Toast.LENGTH_SHORT).show();
            return false;

        } else if (!password.equals(con_password)) {

            Toast.makeText(this, R.string.matches, Toast.LENGTH_SHORT).show();
            return false;
        }
        else if(!checkBox.isChecked())
        {
            Toast.makeText(this, R.string.terms, Toast.LENGTH_SHORT).show();
            return false;
        }
        else
        {
            checkemail();

        }

        return true;
    }
    public void checkemail() {
        if (new InternetCheck(this).isConnect()) {





            ApiInterface api_service = RetrofitInit.getConnect().createConnection();
            Call<EmailNumberVerifyResponse> call = api_service.checkEmail(et_email_id.getText().toString());
            call.enqueue(new Callback<EmailNumberVerifyResponse>() {
                @Override
                public void onResponse(Call<EmailNumberVerifyResponse> call, Response<EmailNumberVerifyResponse> response) {
                    if (response.isSuccessful()) {
                        EmailNumberVerifyResponse server_response = response.body();
                        if (server_response.getStatus() == 200) {

                            checknumber();



                        } else if (server_response.getStatus() == 400) {
                            Toast.makeText(SignUpActivity.this, server_response.getMsg(), Toast.LENGTH_SHORT).show();


                        } else {

                            //   Toast.makeText(LoginActivity.this, server_response.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    }
                }

                @Override
                public void onFailure(Call<EmailNumberVerifyResponse> call, Throwable t) {

                }
            });
        } else {
            Toast toast = Toast.makeText(this, R.string.in, Toast.LENGTH_LONG);
            toast.setGravity(Gravity.CENTER, 0, 0);
            toast.show();
        }

    }
    public void checknumber() {
        if (new InternetCheck(this).isConnect()) {





            ApiInterface api_service = RetrofitInit.getConnect().createConnection();
            Call<EmailNumberVerifyResponse> call = api_service.checkNumber(et_num.getText().toString());
            call.enqueue(new Callback<EmailNumberVerifyResponse>() {
                @Override
                public void onResponse(Call<EmailNumberVerifyResponse> call, Response<EmailNumberVerifyResponse> response) {
                    if (response.isSuccessful()) {
                        EmailNumberVerifyResponse server_response = response.body();
                        if (server_response.getStatus() == 200) {
                            countrycode=  countryCodePicker.getSelectedCountryCodeWithPlus();
                            String numberccode=countrycode+et_num.getText().toString().trim();


                            Intent i=new Intent(SignUpActivity.this,SignUpOtpActivity.class);
                            i.putExtra("number",numberccode);
                            i.putExtra("numberonly",et_num.getText().toString().trim());

                            i.putExtra("countrycode",countrycode);
                            i.putExtra("firstname",et_fname.getText().toString());
                            i.putExtra("lastname",et_lname.getText().toString().trim());

                            i.putExtra("promo",et_promo.getText().toString().trim());
                            i.putExtra("password",et_p.getText().toString().trim());
                            i.putExtra("email",et_email_id.getText().toString().trim());

                            SignUpActivity.this.startActivity(i);



                        } else if (server_response.getStatus() == 400) {
                            Toast.makeText(SignUpActivity.this, server_response.getMsg(), Toast.LENGTH_SHORT).show();


                        } else {

                            //   Toast.makeText(LoginActivity.this, server_response.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    }
                }

                @Override
                public void onFailure(Call<EmailNumberVerifyResponse> call, Throwable t) {

                }
            });
        } else {
            Toast toast = Toast.makeText(this, R.string.in, Toast.LENGTH_LONG);
            toast.setGravity(Gravity.CENTER, 0, 0);
            toast.show();
        }

    }


    @Override
    public void onCountrySelected() {
        countrycode=  countryCodePicker.getSelectedCountryCodeWithPlus();

    }
}
