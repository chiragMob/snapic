package com.snapic.outeractivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.snapic.R;
import com.snapic.response.TerrmsConditionResponse;
import com.snapic.retrofit.ApiInterface;
import com.snapic.retrofit.RetrofitInit;
import com.snapic.utilities.DialogPopup;
import com.snapic.utilities.InternetCheck;
import com.snapic.utilities.SPreferenceKey;
import com.snapic.utilities.SharedPreferenceWriter;

import androidx.appcompat.app.AppCompatActivity;
import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class TermConditionActivity extends AppCompatActivity {
    @BindView(R.id.img_back)
    ImageView img_back;
    @BindView(R.id.tv_term)
    TextView tv_term;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_term_con);
        ButterKnife.bind(this);
        img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        serviceTerms();
    }
    private void serviceTerms() {
        {
            if (new InternetCheck(TermConditionActivity.this).isConnect()) {
                String key = SharedPreferenceWriter.getInstance(TermConditionActivity.this).getString(SPreferenceKey.jwtToken);
                String id = SharedPreferenceWriter.getInstance(TermConditionActivity.this).getString(SPreferenceKey._id);
                String token="Token"+" "+key;

                //aray1@gmail.com,arya8055
                DialogPopup dialog = new DialogPopup(TermConditionActivity.this);
                dialog.showLoadingDialog(TermConditionActivity.this, "");

                ApiInterface api_service = RetrofitInit.getConnect().createConnection();
                Call<TerrmsConditionResponse> call = api_service.term(token);
                call.enqueue(new Callback<TerrmsConditionResponse>() {
                    @Override
                    public void onResponse(Call<TerrmsConditionResponse> call, Response<TerrmsConditionResponse> response) {

                        if (response.isSuccessful()) {
                            TerrmsConditionResponse server_response = response.body();

                            dialog.dismissLoadingDialog();

                            if (server_response.getStatus() == 200) {
                                dialog.dismissLoadingDialog();
                                tv_term.setText(server_response.getData().getTerms_condition());
                                //  setPreferences(server_response);


                            } else if (server_response.getStatus() == 400) {


                                dialog.dismissLoadingDialog();

                            }

                            else {
                                dialog.dismissLoadingDialog();

                                startActivity(new Intent(TermConditionActivity.this,LoginActivity.class));
                            }


                        }
                    }

                    @Override
                    public void onFailure(Call<TerrmsConditionResponse> call, Throwable t) {
                        dialog.dismissLoadingDialog();

                    }
                });
            } else {
                Toast toast = Toast.makeText(TermConditionActivity.this, R.string.in, Toast.LENGTH_LONG);
                toast.setGravity(Gravity.CENTER, 0, 0);
                toast.show();
            }
        }


    }

}

