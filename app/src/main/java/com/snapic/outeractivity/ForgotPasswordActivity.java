package com.snapic.outeractivity;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.hbb20.CountryCodePicker;
import com.snapic.R;
import com.snapic.response.EmailNumberVerifyResponse;
import com.snapic.retrofit.ApiInterface;
import com.snapic.retrofit.RetrofitInit;
import com.snapic.utilities.InternetCheck;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;
import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ForgotPasswordActivity extends AppCompatActivity implements CountryCodePicker.OnCountryChangeListener {
@BindView(R.id.txt_otp)
    TextView txt_otp;
@BindView(R.id.con_back)
ConstraintLayout con_back;
@BindView(R.id.tv_emai)
TextView tv_emai;
@BindView(R.id.tv_mobile)
TextView tv_mobile;
@BindView(R.id.view_email)
View view_email;
@BindView(R.id.view_mobile)
View view_mobile;
@BindView(R.id.txt_otp_mobi)
TextView txt_otp_mobi;
@BindView(R.id.con_num)
ConstraintLayout con_num;
@BindView(R.id.et_mail)
EditText et_mail;
@BindView(R.id.et_phone_number)
EditText et_phone_number;
    @BindView(R.id.countryCodePicker)
    CountryCodePicker countryCodePicker;
    String countrycode="";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_pass);
        ButterKnife.bind(this);
        et_mail.setVisibility(View.VISIBLE);
        txt_otp.setVisibility(View.VISIBLE);
        con_num.setVisibility(View.GONE);
        txt_otp_mobi.setVisibility(View.GONE);
        tv_emai.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onClick(View view) {
                et_mail.setVisibility(View.VISIBLE);
                txt_otp.setVisibility(View.VISIBLE);
                con_num.setVisibility(View.GONE);
                txt_otp_mobi.setVisibility(View.GONE);
                view_email.setVisibility(View.VISIBLE);
                tv_emai.setTextColor(getResources().getColor(R.color.colorBlue, null));
                view_email.setBackgroundColor(getResources().getColor(R.color.colorBlue, null));
                tv_mobile.setTextColor(getResources().getColor(R.color.Grey, null));
                view_mobile.setVisibility(View.GONE);
            }
        });
        tv_mobile.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onClick(View view) {
                et_mail.setVisibility(View.GONE);
                txt_otp.setVisibility(View.GONE);
                con_num.setVisibility(View.VISIBLE);
                view_mobile.setVisibility(View.VISIBLE);

                txt_otp_mobi.setVisibility(View.VISIBLE);
                tv_emai.setTextColor(getResources().getColor(R.color.Grey, null));
                view_email.setVisibility(View.GONE);
                tv_mobile.setTextColor(getResources().getColor(R.color.colorBlue, null));
                view_mobile.setBackgroundColor(getResources().getColor(R.color.colorBlue, null));
            }
        });
        txt_otp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                checkemail();
//                Intent i = new Intent(ForgotPasswordActivity.this, OtpResetActivity.class);
//
//                startActivity(i);

            }
        });
        con_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(ForgotPasswordActivity.this, LoginActivity.class);

                startActivity(i);            }
        });
        txt_otp_mobi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
              String mobilenumber=  et_phone_number.getText().toString();
                if (TextUtils.isEmpty(mobilenumber)) {
                    et_phone_number.setFocusable(true);
                    et_phone_number.requestFocus();
                    Toast.makeText(ForgotPasswordActivity.this, R.string.numbers, Toast.LENGTH_SHORT).show();

                } else if (mobilenumber.length() < 6 || mobilenumber.length() > 15) {
                    et_phone_number.setFocusable(true);
                    et_phone_number.requestFocus();
                    Toast.makeText(ForgotPasswordActivity.this, R.string.numbers, Toast.LENGTH_SHORT).show();
                }
                else
                {
                   checknumber();

                }
            }
        });

    }

    public void checknumber() {
        if (new InternetCheck(this).isConnect()) {





            ApiInterface api_service = RetrofitInit.getConnect().createConnection();
            Call<EmailNumberVerifyResponse> call = api_service.checkNumber(et_phone_number.getText().toString());
            call.enqueue(new Callback<EmailNumberVerifyResponse>() {
                @Override
                public void onResponse(Call<EmailNumberVerifyResponse> call, Response<EmailNumberVerifyResponse> response) {
                    if (response.isSuccessful()) {
                        EmailNumberVerifyResponse server_response = response.body();
                        if (server_response.getStatus() == 200) {

                            Toast.makeText(ForgotPasswordActivity.this, server_response.getMsg(), Toast.LENGTH_SHORT).show();



                        } else if (server_response.getStatus() == 400) {
                            Toast.makeText(ForgotPasswordActivity.this, server_response.getMsg(), Toast.LENGTH_SHORT).show();
                            countrycode=  countryCodePicker.getSelectedCountryCodeWithPlus();
                            String numberccode=countrycode+et_phone_number.getText().toString().trim();
                            Intent i=new Intent(ForgotPasswordActivity.this,ForgotPasswordOtpActivity.class);
                            i.putExtra("number",numberccode);
                            i.putExtra("numberonly",et_phone_number.getText().toString().trim());

                            i.putExtra("countrycode",countrycode);


                            ForgotPasswordActivity.this.startActivity(i);


                        } else {

                            //   Toast.makeText(LoginActivity.this, server_response.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    }
                }

                @Override
                public void onFailure(Call<EmailNumberVerifyResponse> call, Throwable t) {

                }
            });
        } else {
            Toast toast = Toast.makeText(this, R.string.in, Toast.LENGTH_LONG);
            toast.setGravity(Gravity.CENTER, 0, 0);
            toast.show();
        }

    }

    public void checkemail() {
        if (new InternetCheck(this).isConnect()) {





            ApiInterface api_service = RetrofitInit.getConnect().createConnection();
            Call<EmailNumberVerifyResponse> call = api_service.checkEmail(et_mail.getText().toString());
            call.enqueue(new Callback<EmailNumberVerifyResponse>() {
                @Override
                public void onResponse(Call<EmailNumberVerifyResponse> call, Response<EmailNumberVerifyResponse> response) {
                    if (response.isSuccessful()) {
                        EmailNumberVerifyResponse server_response = response.body();
                        if (server_response.getStatus() == 200) {




                        } else if (server_response.getStatus() == 400) {
                            sendotp();

                        } else {

                            //   Toast.makeText(LoginActivity.this, server_response.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    }
                }

                @Override
                public void onFailure(Call<EmailNumberVerifyResponse> call, Throwable t) {

                }
            });
        } else {
            Toast toast = Toast.makeText(this, R.string.in, Toast.LENGTH_LONG);
            toast.setGravity(Gravity.CENTER, 0, 0);
            toast.show();
        }

    }
    public void sendotp() {
        if (new InternetCheck(this).isConnect()) {





            ApiInterface api_service = RetrofitInit.getConnect().createConnection();
            Call<EmailNumberVerifyResponse> call = api_service.SendEmailOtp(et_mail.getText().toString().trim());
            call.enqueue(new Callback<EmailNumberVerifyResponse>() {
                @Override
                public void onResponse(Call<EmailNumberVerifyResponse> call, Response<EmailNumberVerifyResponse> response) {
                    if (response.isSuccessful()) {
                        EmailNumberVerifyResponse server_response = response.body();
                        if (server_response.getStatus() == 200) {

                            Toast.makeText(ForgotPasswordActivity.this, server_response.getMsg(), Toast.LENGTH_SHORT).show();
                            Intent i=new Intent(ForgotPasswordActivity.this,OtpResetActivity.class);

                            i.putExtra("email",et_mail.getText().toString().trim());

                            ForgotPasswordActivity.this.startActivity(i);


                        } else if (server_response.getStatus() == 400) {
                            Toast.makeText(ForgotPasswordActivity.this, server_response.getMsg(), Toast.LENGTH_SHORT).show();


                        } else {

                            //   Toast.makeText(LoginActivity.this, server_response.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    }
                }

                @Override
                public void onFailure(Call<EmailNumberVerifyResponse> call, Throwable t) {

                }
            });
        } else {
            Toast toast = Toast.makeText(this, R.string.in, Toast.LENGTH_LONG);
            toast.setGravity(Gravity.CENTER, 0, 0);
            toast.show();
        }

    }


    @Override
    public void onCountrySelected() {

    }
}
