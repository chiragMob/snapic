package com.snapic.outeractivity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.database.Cursor;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.provider.MediaStore;
import android.view.View;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.snapic.R;
import com.snapic.activity.CreateAlbumActivity;
import com.snapic.activity.HomeActivity;
import com.snapic.adapter.DemoGalleryAdapter;
import com.snapic.response.ImageItem;
import com.snapic.response.MyListData;
import com.snapic.utilities.GetPath;
import com.snapic.utilities.SPreferenceKey;
import com.snapic.utilities.SharedPreferenceWriter;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

public class SelectionGalleryActivity extends AppCompatActivity implements GalleryClicekListner {
    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;
    DemoGalleryAdapter gallerAdapter;
    ProgressBar simpleProgressBar;
    List<MyListData> myListData = new ArrayList<MyListData>();
    @BindView(R.id.tv_point)
    TextView tv_point;
    @BindView(R.id.tv_done)
    TextView tv_done;
    @BindView(R.id.img_back)
    ImageView img_back;
    @BindView(R.id.cons_click)
    ConstraintLayout cons_click;
    public ArrayList<ImageItem> images = new ArrayList<ImageItem>();
    private long lastId;
    public AndroidCustomGalleryActivity.ImageAdapter imageAdapter;
    private final static int TAKE_IMAGE = 1;
    private final static int UPLOAD_IMAGES = 2;
    private final static int VIEW_IMAGE = 3;
    private Uri imageUri;
    private MediaScannerConnection mScanner;
    public GridView imagegrid;
    private final int SPLASH_DISPLAY_LENGTH = 1500;

    public enum WhatAbout {
        START,STOP
    }
    Handler handler;
    int value=0;
    public WhatAbout[] wa = WhatAbout.values();
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.custom_gallery);
        ButterKnife.bind(this);
updateAdapter();
        tv_done.setClickable(false);
        cons_click.setClickable(false);
        handler=new Handler();

        gallerAdapter = new DemoGalleryAdapter(this,images,this);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

              initialize();

            }

        }, SPLASH_DISPLAY_LENGTH);

        simpleProgressBar=findViewById(R.id.simpleProgressBar);
        simpleProgressBar.setProgress(0);
        tv_point.setText("0");
        RecyclerView.LayoutManager manager = new GridLayoutManager(this, 3);
        recyclerView.setLayoutManager(manager);
        recyclerView.addItemDecoration(new DividerItemDecoration(this, LinearLayoutManager.VERTICAL));
        recyclerView.setAdapter(gallerAdapter);

        SharedPreferenceWriter.getInstance(SelectionGalleryActivity.this).writeIntValue(SPreferenceKey.IS_USER_LOGIN, 0);
        tv_done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int value=0;
                ArrayList<String> add=new ArrayList<String>() ;



                for(int i=0;i<images.size();i++)
                {
                    if(images.get(i).isSelection())
                    {
                        value=value+images.get(i).getValue();
                        for (int j=0;j<images.get(i).getValue();j++)
                        {
                            add.add(images.get(i).getPaths());

                        }
                    }

                }
                if(value==50)
                {
                    Intent ij = new Intent(SelectionGalleryActivity.this, CreateAlbumActivity.class);
                    SharedPreferenceWriter.getInstance(SelectionGalleryActivity.this).writeStringValue(SPreferenceKey.notifcation_status, "true");

                    ij.putStringArrayListExtra("test", add);

                    startActivity(ij);

                }
                else
                {
                    Toast.makeText(SelectionGalleryActivity.this, "Please select 50 photos", Toast.LENGTH_SHORT).show();
                }

            }
        });
        img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               onBackPressed();          }
        });


    }
    void  updateAdapter()
    {
        simpleProgressBar=findViewById(R.id.simpleProgressBar);
        simpleProgressBar.setProgress(0);
        tv_point.setText("1");
        RecyclerView.LayoutManager manager = new GridLayoutManager(this, 3);
        recyclerView.setLayoutManager(manager);
        recyclerView.addItemDecoration(new DividerItemDecoration(this, LinearLayoutManager.VERTICAL));
        recyclerView.setAdapter(gallerAdapter);

        SharedPreferenceWriter.getInstance(SelectionGalleryActivity.this).writeIntValue(SPreferenceKey.IS_USER_LOGIN, 0);
    }

    @Override
    public void onItemClickListener(String id) {
        if(!id.isEmpty())
        {
            int value=0;
            if(images.size()>0)
            {
                int count=0;
                for(int i=0;i<images.size();i++)
                {
                    if(images.get(i).isSelection())
                    {
                        value=value+images.get(i).getValue();

                    }

                }
               SharedPreferenceWriter.getInstance(SelectionGalleryActivity.this).writeIntValue(SPreferenceKey.IS_USER_LOGIN, value);
//
//
               tv_point.setText(String.valueOf(value));
              simpleProgressBar.setProgress(value);
            }



        }
    }
    @SuppressLint("MissingSuperCall")
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case TAKE_IMAGE:
                try {
                    if (resultCode == RESULT_OK) {

                        // we need to update the gallery by starting MediaSanner service.
                        mScanner = new MediaScannerConnection(
                                SelectionGalleryActivity.this,
                                new MediaScannerConnection.MediaScannerConnectionClient() {
                                    public void onMediaScannerConnected() {

                                        mScanner.scanFile(imageUri.getPath(), null /* mimeType */);
                                    }

                                    public void onScanCompleted(String path, Uri uri) {
                                        //we can use the uri, to get the newly added image, but it will return path to full sized image
                                        //e.g. content://media/external/images/media/7
                                        //we can also update this path by replacing media by thumbnail to get the thumbnail
                                        //because thumbnail path would be like content://media/external/images/thumbnail/7
                                        //But the thumbnail is created after some delay by Android OS
                                        //So you may not get the thumbnail. This is why I started new UI thread
                                        //and it'll only run after the current thread completed.
                                        if (path.equals(imageUri.getPath())) {
                                            mScanner.disconnect();
                                            //we need to create new UI thread because, we can't update our mail thread from here
                                            //Both the thread will run one by one, see documentation of android
                                            SelectionGalleryActivity.this
                                                    .runOnUiThread(new Runnable() {
                                                        public void run() {

                                                            checkForNewImages();
                                                        }
                                                    });
                                        }
                                    }
                                });
                        mScanner.connect();

                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
            case UPLOAD_IMAGES:
                if (resultCode == RESULT_OK){
                    //do some code where you integrate this project
                }
                break;
        }
    }
    public void checkForNewImages(){
        //Here we'll only check for newer images
        final String[] columns = { MediaStore.Images.Thumbnails._ID };
        final String orderBy = MediaStore.Images.Media._ID;
        Cursor imagecursor = getContentResolver().query(
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI, columns,
                MediaStore.Images.Media._ID + " > " + lastId , null, orderBy);
        int image_column_index = imagecursor
                .getColumnIndex(MediaStore.Images.Media._ID);
        int count = imagecursor.getCount();
        for (int i = 0; i < count; i++) {
            imagecursor.moveToPosition(i);
            int id = imagecursor.getInt(image_column_index);
            Uri uri = Uri.withAppendedPath( MediaStore.Images.Media.EXTERNAL_CONTENT_URI, Integer.toString(id) );
            String path=    GetPath.getPath(SelectionGalleryActivity.this,uri);
            ImageItem imageItem = new ImageItem();
            imageItem.setId(id);
            imageItem.setPaths(path);
            lastId = id;
            imageItem.setImg( MediaStore.Images.Thumbnails.getThumbnail(
                    getApplicationContext().getContentResolver(), id,
                    MediaStore.Images.Thumbnails.MICRO_KIND, null));
            images.add(imageItem);
        }
        imagecursor.close();
        img_back.setClickable(true);
        tv_done.setClickable(true);
        gallerAdapter.notifyDataSetChanged();
    }

    public void initialize() {
        images.clear();
        final String[] columns = { MediaStore.Images.Thumbnails._ID };
        final String orderBy = MediaStore.Images.Media._ID;
        Cursor imagecursor = getContentResolver().query(
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI, columns,
                null, null, orderBy);
        if(imagecursor != null){
            int image_column_index = imagecursor
                    .getColumnIndex(MediaStore.Images.Media._ID);
            int count = imagecursor.getCount();
            for (int i = 0; i < count; i++) {

                imagecursor.moveToPosition(i);
                int id = imagecursor.getInt(image_column_index);
                Uri uri = Uri.withAppendedPath( MediaStore.Images.Media.EXTERNAL_CONTENT_URI, Integer.toString(id) );
            String path=    GetPath.getPath(SelectionGalleryActivity.this,uri);
               ImageItem imageItem = new ImageItem();
                imageItem.setId(id);
                imageItem.setPaths(path);

                lastId = id;
                imageItem.setImg( MediaStore.Images.Thumbnails.getThumbnail(
                        getApplicationContext().getContentResolver(), id,
                        MediaStore.Images.Thumbnails.MICRO_KIND, null));
                images.add(imageItem);
            }
            value=1;
            cons_click.setClickable(true);
            imagecursor.close();
        }
        updateAdapter();

      //  gallerAdapter.notifyDataSetChanged();
    }

    @Override
    public void onBackPressed() {

        if(value==1)
        {
            startActivity(new Intent(SelectionGalleryActivity.this,HomeActivity.class));
        }

    }
}

