package com.snapic.outeractivity;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.TextUtils;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.snapic.R;
import com.snapic.response.EmailNumberVerifyResponse;
import com.snapic.retrofit.ApiInterface;
import com.snapic.retrofit.RetrofitInit;
import com.snapic.utilities.InternetCheck;
import com.snapic.utilities.KeyBoardUtils;

import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;
import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ResetPasswordActivity extends AppCompatActivity {
    @BindView(R.id.txt_submit)
    TextView txt_submit;
    @BindView(R.id.img_back)
    ImageView img_back;
    String email="";
    @BindView(R.id.et_pa)
    EditText et_pa;
    @BindView(R.id.et_con)
    EditText et_con;
    @BindView(R.id.tv_show)
    TextView tv_show;
    @BindView(R.id.tv_con)
    TextView tv_con;
    final String PASSWORD_PATTERN = "^(?=.*[0-9])(?=.*[A-Z])(?=.*[@#$%^&+=!])(?=\\S+$).{4,}$";
    KeyBoardUtils keyBoardUtils;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Intent i = getIntent();
        email = i.getStringExtra("email");
        setContentView(R.layout.activity_reset_password);
        ButterKnife.bind(this);
        keyBoardUtils = KeyBoardUtils.getInstance(this);

        txt_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               String pa= et_pa.getText().toString();
               String con= et_con.getText().toString();
                if (TextUtils.isEmpty(pa)) {
                    et_pa.setFocusable(true);
                    et_pa.requestFocus();
                    Toast.makeText(ResetPasswordActivity.this, R.string.enter, Toast.LENGTH_SHORT).show();

                } else if (et_pa.getText().toString().length() <= 6 || et_pa.getText().toString().length() >= 16) {
                    Toast.makeText(ResetPasswordActivity.this, R.string.combs, Toast.LENGTH_SHORT).show();
                } else if (pa.length() >= 6 && pa.length() <= 16 && !pa.matches(PASSWORD_PATTERN)) {
                    Toast.makeText(ResetPasswordActivity.this, R.string.letters, Toast.LENGTH_SHORT).show();
                } else if (TextUtils.isEmpty(con)) {
                    et_con.setFocusable(true);
                    et_con.requestFocus();
                    Toast.makeText(ResetPasswordActivity.this, R.string.conspasss, Toast.LENGTH_SHORT).show();

                }
                else if (!pa.equals(con)) {

                    Toast.makeText(ResetPasswordActivity.this, R.string.matchesss, Toast.LENGTH_SHORT).show();
                }
                else
                {
                    changepassword();
                }


             //   showDialogTPassword();
            }
        });
        img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        tv_show.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                keyBoardUtils.hideKeyboardOutsideTouch(ResetPasswordActivity.this,et_pa);
                if(et_pa.getText().toString().isEmpty())
                {
                    Toast.makeText(ResetPasswordActivity.this, R.string.enss_pass, Toast.LENGTH_SHORT).show();
                    et_pa.setSelection(et_pa.getText().length());
                }
                else {
                    String text = tv_show.getText().toString();
                    if (text.equals("Show")) {
                        et_pa.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
                        tv_show.setText("hide");
                        et_pa.setSelection(et_pa.getText().length());

                    } else {
                        et_pa.setTransformationMethod(PasswordTransformationMethod.getInstance());
                        tv_show.setText("Show");
                        et_pa.setSelection(et_pa.getText().length());


                    }
                }
            }
        });
        tv_con.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                keyBoardUtils.hideKeyboardOutsideTouch(ResetPasswordActivity.this,et_con);
                if(et_con.getText().toString().isEmpty())
                {
                    Toast.makeText(ResetPasswordActivity.this, R.string.enpass, Toast.LENGTH_SHORT).show();
                    et_con.setSelection(et_pa.getText().length());
                }
                else {
                    String text = tv_con.getText().toString();
                    if (text.equals("Show")) {
                        et_con.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
                        tv_con.setText("hide");
                        et_con.setSelection(et_con.getText().length());

                    } else {
                        et_con.setTransformationMethod(PasswordTransformationMethod.getInstance());
                        tv_con.setText("Show");
                        et_con.setSelection(et_con.getText().length());


                    }
                }
            }
        });


    }

    public void changepassword() {
        if (new InternetCheck(this).isConnect()) {





            ApiInterface api_service = RetrofitInit.getConnect().createConnection();
            Call<EmailNumberVerifyResponse> call = api_service.forgotResponse(email,et_pa.getText().toString(),et_con.getText().toString());
            call.enqueue(new Callback<EmailNumberVerifyResponse>() {
                @Override
                public void onResponse(Call<EmailNumberVerifyResponse> call, Response<EmailNumberVerifyResponse> response) {
                    if (response.isSuccessful()) {
                        EmailNumberVerifyResponse server_response = response.body();
                        if (server_response.getStatus() == 200) {

                            Toast.makeText(ResetPasswordActivity.this, server_response.getMsg(), Toast.LENGTH_SHORT).show();
                            showDialogTPassword();



                        } else if (server_response.getStatus() == 400) {
                            Toast.makeText(ResetPasswordActivity.this, server_response.getMsg(), Toast.LENGTH_SHORT).show();


                        } else {

                            //   Toast.makeText(LoginActivity.this, server_response.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    }
                }

                @Override
                public void onFailure(Call<EmailNumberVerifyResponse> call, Throwable t) {

                }
            });
        } else {
            Toast toast = Toast.makeText(this, "Internet connection lost", Toast.LENGTH_LONG);
            toast.setGravity(Gravity.CENTER, 0, 0);
            toast.show();
        }

    }



    private void showDialogTPassword() {

        ConstraintLayout constraintLayout;
        TextView txt_login;
        final Dialog dialogTyreDetail = new Dialog(ResetPasswordActivity.this, android.R.style.Theme_Black_NoTitleBar);
        dialogTyreDetail.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogTyreDetail.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialogTyreDetail.setContentView(R.layout.dialog_pass_update);
        dialogTyreDetail.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        dialogTyreDetail.setCanceledOnTouchOutside(true);
        txt_login=dialogTyreDetail.findViewById(R.id.txt_login);
        txt_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(ResetPasswordActivity.this, LoginActivity.class);

                startActivity(i);
            }
        });




        dialogTyreDetail.show();


    }

}
