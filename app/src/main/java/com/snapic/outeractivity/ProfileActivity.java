package com.snapic.outeractivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.snapic.R;
import com.snapic.response.UserDetailResponse;
import com.snapic.retrofit.ApiInterface;
import com.snapic.retrofit.RetrofitInit;
import com.snapic.utilities.DialogPopup;
import com.snapic.utilities.InternetCheck;
import com.snapic.utilities.SPreferenceKey;
import com.snapic.utilities.SharedPreferenceWriter;

import androidx.appcompat.app.AppCompatActivity;
import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class ProfileActivity extends AppCompatActivity {
    @BindView(R.id.tv_edit)
    TextView tv_edit;
    @BindView(R.id.img_back)
    ImageView img_back;
    @BindView(R.id.img_pic)
    CircleImageView img_pic;
    @BindView(R.id.tv_num)
    TextView tv_num;
    @BindView(R.id.tv_name)
    TextView tv_name;
    @BindView(R.id.tv_mail)
    TextView tv_mail;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        ButterKnife.bind(this);
        tv_edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(ProfileActivity.this,EditProfileActivity.class));

            }
        });
        img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        serviceUserDetail();
    }

    private void serviceUserDetail() {
        {
            if (new InternetCheck(ProfileActivity.this).isConnect()) {
                String key = SharedPreferenceWriter.getInstance(ProfileActivity.this).getString(SPreferenceKey.jwtToken);
                String id = SharedPreferenceWriter.getInstance(ProfileActivity.this).getString(SPreferenceKey._id);
                String token="Token"+" "+key;

                //aray1@gmail.com,arya8055
                DialogPopup dialog = new DialogPopup(ProfileActivity.this);
                dialog.showLoadingDialog(ProfileActivity.this, "");

                ApiInterface api_service = RetrofitInit.getConnect().createConnection();
                Call<UserDetailResponse> call = api_service.userDetail(token,id);
                call.enqueue(new Callback<UserDetailResponse>() {
                    @Override
                    public void onResponse(Call<UserDetailResponse> call, Response<UserDetailResponse> response) {
                        if(response.code()==401)
                        {
                            startActivity(new Intent(ProfileActivity.this, LoginActivity.class));
                            SharedPreferenceWriter.getInstance(ProfileActivity.this).writeStringValue(SPreferenceKey._id, "");
                            SharedPreferenceWriter.getInstance(ProfileActivity.this).writeStringValue(SPreferenceKey.jwtToken, "");

                        }
                        else
                        {
                            if (response.isSuccessful()) {
                                UserDetailResponse server_response = response.body();
                                if(server_response.getDetail().isEmpty())
                                {
                                    if (server_response.getStatus() == 200) {
                                        dialog.dismissLoadingDialog();
                                        if(server_response.getData()==null)
                                        {

                                        }
                                        else
                                        {
                                            tv_name.setText(server_response.getData().getFirst_name()+" "+server_response.getData().getLast_name());

                                            tv_num.setText(server_response.getData().getPhone_number());
                                            if(!server_response.getData().getEmail().contains("@"))
                                            {

                                            }
                                            else
                                            {
                                                tv_mail.setText(server_response.getData().getEmail());

                                            }

                                            //  setPreferences(server_response);
                                            if (!server_response.getData().getProfile_pic().isEmpty()) {
                                                String pic="https://pagiupload.s3.amazonaws.com/"+server_response.getData().getProfile_pic();
                                                Glide.with(ProfileActivity.this).load(server_response.getData().getProfile_pic()).into(img_pic);

                                            } else {

                                                Glide.with(ProfileActivity.this).load(R.drawable.default_profile).into(img_pic);



                                                //Glide.with(EditProfileActivity.this).load(R.drawable.pro_pic).into(profile_et_img);


                                            }
                                        }


                                    } else if (server_response.getStatus() == 400) {


                                        dialog.dismissLoadingDialog();

                                    }
                                }
                                else {
                                    dialog.dismissLoadingDialog();

                                    if (server_response.getDetail().equals("Invalid token."))
                                    {
                                        startActivity(new Intent(ProfileActivity.this,LoginActivity.class));
                                    }

                                }
                            }

                        }
                    }

                    @Override
                    public void onFailure(Call<UserDetailResponse> call, Throwable t) {
                        dialog.dismissLoadingDialog();

                    }
                });
            } else {
                Toast toast = Toast.makeText(ProfileActivity.this, R.string.in, Toast.LENGTH_LONG);
                toast.setGravity(Gravity.CENTER, 0, 0);
                toast.show();
            }
        }


    }

}