package com.snapic.outeractivity;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.snapic.R;
import com.snapic.activity.ProfileActivity;
import com.snapic.response.ChangeLanuageResponse;
import com.snapic.response.GetFcmResponse;
import com.snapic.response.LanuageSettingsResponse;
import com.snapic.response.NotificationResponse;
import com.snapic.retrofit.ApiInterface;
import com.snapic.retrofit.RetrofitInit;
import com.snapic.utilities.DialogPopup;
import com.snapic.utilities.InternetCheck;
import com.snapic.utilities.SPreferenceKey;
import com.snapic.utilities.SharedPreferenceWriter;
import com.snapic.utility.LocaleHelper;

import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;
import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class SettingsActivity extends AppCompatActivity {
    @BindView(R.id.con_change)
    ConstraintLayout con_change;
    @BindView(R.id.img_back)
    ImageView img_back;
    @BindView(R.id.imageView221)
    ToggleButton imageView221;
    String fcmid;
    String settingsid="";
    @BindView(R.id.first)
    ConstraintLayout first;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
        ButterKnife.bind(this);
        String social  = SharedPreferenceWriter.getInstance(SettingsActivity.this).getString(SPreferenceKey.user_id_forgot);


        if(social.equals("Social"))
        {

        }
        else
        {
            con_change.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    startActivity(new Intent(SettingsActivity.this,ChangePasswordActivity.class));

                }
            });


        }

        img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        first.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showDialog();
            }
        });

        imageView221.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(imageView221.isChecked())
                {
                    ServiceNotificationStatus("true");
                }
                else
                {
                    ServiceNotificationStatus("false");
                }
            }
        });

    }
    private void showDialog() {
        final Dialog dialogLockDetails = new Dialog(SettingsActivity.this, android.R.style.Theme_Black_NoTitleBar);
        dialogLockDetails.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogLockDetails.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialogLockDetails.setContentView(R.layout.dialog_change_lang);
        dialogLockDetails.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        dialogLockDetails.setCanceledOnTouchOutside(true);

        TextView txt_assistance = dialogLockDetails.findViewById(R.id.txt_login);
        RadioButton rad_eng = dialogLockDetails.findViewById(R.id.rad_eng);
        RadioButton rad_hind = dialogLockDetails.findViewById(R.id.rad_hind);

        txt_assistance.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!rad_eng.isChecked()&&!rad_hind.isChecked())
                {
                    Toast.makeText(SettingsActivity.this, R.string.lanu, Toast.LENGTH_SHORT).show();
                }
                else
                {
if(rad_eng.isChecked())
{
    LocaleHelper.setLocale(SettingsActivity.this, "en");

    dialogLockDetails.dismiss();
    SharedPreferenceWriter.getInstance(SettingsActivity.this).writeStringValue(SPreferenceKey.lan, "en");
     settingsid = SharedPreferenceWriter.getInstance(SettingsActivity.this).getString(SPreferenceKey.lat);

    serviceSettingsLanuage("English");

    startActivity(new Intent(SettingsActivity.this,SettingsActivity.class));

}
else if(rad_hind.isChecked())
{
    LocaleHelper.setLocale(SettingsActivity.this, "brx");
    dialogLockDetails.dismiss();
    settingsid = SharedPreferenceWriter.getInstance(SettingsActivity.this).getString(SPreferenceKey.lat);

    SharedPreferenceWriter.getInstance(SettingsActivity.this).writeStringValue(SPreferenceKey.lan, "ar");
    serviceSettingsLanuage("arabic");

    startActivity(new Intent(SettingsActivity.this,SettingsActivity.class));



}
                }

            }
        });



        dialogLockDetails.show();

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        startActivity(new Intent(SettingsActivity.this,ProfileActivity.class));
    }

    @Override
    protected void onResume() {
        super.onResume();
        serviceSettings();
        ServiceNotification();
    }

    public void ServiceNotificationStatus(String status){
        String key = SharedPreferenceWriter.getInstance(SettingsActivity.this).getString(SPreferenceKey.jwtToken);
        String id = SharedPreferenceWriter.getInstance(SettingsActivity.this).getString(SPreferenceKey._id);
        String token="Token"+" "+key;
        if(new InternetCheck(this).isConnect())
        {
            DialogPopup dialogs = new DialogPopup(this);
            dialogs.showLoadingDialog(this,"");

            ApiInterface api_service= RetrofitInit.getConnect().createConnection();
            Call<NotificationResponse> call=api_service.notify(token,fcmid,status);
            call.enqueue(new Callback<NotificationResponse>() {
                @Override
                public void onResponse(Call<NotificationResponse> call, Response<NotificationResponse> response) {
                    if(response.isSuccessful())
                    {
                        NotificationResponse server_response= response.body();
                        if(server_response.getStatus()==200)
                        {

                            dialogs.dismissLoadingDialog();
                            Toast.makeText(SettingsActivity.this, server_response.getMessage(), Toast.LENGTH_SHORT).show();

                            // dialog.dismiss();
                            // fireBaseAction();
                            //  showDialogotp();


                        }
                        else  if(server_response.getStatus()==500)
                        {
                            dialogs.dismissLoadingDialog();
                            Toast.makeText(SettingsActivity.this, server_response.getMessage(), Toast.LENGTH_SHORT).show();

                            //   startActivity(new Intent(SettingsActivity.this,LoginActivity.class));
                        }

                        else if (server_response.getStatus()==400)
                        {
                            dialogs.dismissLoadingDialog();
                            Toast.makeText(SettingsActivity.this, server_response.getMessage(), Toast.LENGTH_SHORT).show();


                        }
                        else
                        {
                            dialogs.dismissLoadingDialog();

                            Toast.makeText(SettingsActivity.this, server_response.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    }
                }
                @Override
                public void onFailure(Call<NotificationResponse> call, Throwable t) {
                    dialogs.dismissLoadingDialog();

                }
            });
        }
        else
        {
            Toast toast=Toast.makeText(this,R.string.in,Toast.LENGTH_LONG);
            toast.setGravity(Gravity.CENTER,0,0);
            toast.show();
        }

    }

    public void ServiceNotification(){
        String key = SharedPreferenceWriter.getInstance(SettingsActivity.this).getString(SPreferenceKey.jwtToken);
        String id = SharedPreferenceWriter.getInstance(SettingsActivity.this).getString(SPreferenceKey._id);
        String token="Token"+" "+key;
        if(new InternetCheck(this).isConnect())
        {
            DialogPopup dialogs = new DialogPopup(this);
            dialogs.showLoadingDialog(this,"");

            ApiInterface api_service= RetrofitInit.getConnect().createConnection();
            Call<GetFcmResponse> call=api_service.fcmSettings(token);
            call.enqueue(new Callback<GetFcmResponse>() {
                @Override
                public void onResponse(Call<GetFcmResponse> call, Response<GetFcmResponse> response) {
                    if(response.isSuccessful())
                    {
                        GetFcmResponse server_response= response.body();
                        if(server_response.getStatus()==200)
                        {

                            dialogs.dismissLoadingDialog();
                            fcmid=String.valueOf(server_response.getId());
                            if(server_response.isData())
                            {
                                imageView221.setChecked(true);
                            }
                            else
                            {
                                imageView221.setChecked(false);

                            }

                            // dialog.dismiss();
                            // fireBaseAction();
                            //  showDialogotp();


                        }
                        else  if(server_response.getStatus()==500)
                        {
                            dialogs.dismissLoadingDialog();

                            //   startActivity(new Intent(SettingsActivity.this,LoginActivity.class));
                        }

                        else if (server_response.getStatus()==400)
                        {
                            dialogs.dismissLoadingDialog();


                        }
                        else
                        {
                            dialogs.dismissLoadingDialog();

                        }
                    }
                }
                @Override
                public void onFailure(Call<GetFcmResponse> call, Throwable t) {
                    dialogs.dismissLoadingDialog();

                }
            });
        }
        else
        {
            Toast toast=Toast.makeText(this,R.string.in,Toast.LENGTH_LONG);
            toast.setGravity(Gravity.CENTER,0,0);
            toast.show();
        }

    }

    private void serviceSettings() {
        {
            if (new InternetCheck(SettingsActivity.this).isConnect()) {
                String key = SharedPreferenceWriter.getInstance(SettingsActivity.this).getString(SPreferenceKey.jwtToken);
                String id = SharedPreferenceWriter.getInstance(SettingsActivity.this).getString(SPreferenceKey._id);
                String token="Token"+" "+key;

                //aray1@gmail.com,arya8055
                DialogPopup dialog = new DialogPopup(SettingsActivity.this);
                dialog.showLoadingDialog(SettingsActivity.this, "");

                ApiInterface api_service = RetrofitInit.getConnect().createConnection();
                Call<ChangeLanuageResponse> call = api_service.chLanuge(token);
                call.enqueue(new Callback<ChangeLanuageResponse>() {
                    @Override
                    public void onResponse(Call<ChangeLanuageResponse> call, Response<ChangeLanuageResponse> response) {

                        if (response.isSuccessful()) {
                            ChangeLanuageResponse server_response = response.body();

                                dialog.dismissLoadingDialog();

                                if (server_response.getStatus() == 200) {
                                    dialog.dismissLoadingDialog();
                                    SharedPreferenceWriter.getInstance(SettingsActivity.this).writeStringValue(SPreferenceKey.lat, String.valueOf(server_response.getId()));


                                    //  setPreferences(server_response);


                                } else if (server_response.getStatus() == 400) {


                                    dialog.dismissLoadingDialog();

                                }

                            else {
                                dialog.dismissLoadingDialog();



                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<ChangeLanuageResponse> call, Throwable t) {
                        dialog.dismissLoadingDialog();

                    }
                });
            } else {
                Toast toast = Toast.makeText(SettingsActivity.this, R.string.in, Toast.LENGTH_LONG);
                toast.setGravity(Gravity.CENTER, 0, 0);
                toast.show();
            }
        }


    }
    private void serviceSettingsLanuage(String lan) {
        {
            if (new InternetCheck(SettingsActivity.this).isConnect()) {
                String key = SharedPreferenceWriter.getInstance(SettingsActivity.this).getString(SPreferenceKey.jwtToken);
                String id = SharedPreferenceWriter.getInstance(SettingsActivity.this).getString(SPreferenceKey._id);
                String token="Token"+" "+key;

                //aray1@gmail.com,arya8055
                DialogPopup dialog = new DialogPopup(SettingsActivity.this);
                dialog.showLoadingDialog(SettingsActivity.this, "");

                ApiInterface api_service = RetrofitInit.getConnect().createConnection();
                Call<LanuageSettingsResponse> call = api_service.updateLanug(token,settingsid,lan);
                call.enqueue(new Callback<LanuageSettingsResponse>() {
                    @Override
                    public void onResponse(Call<LanuageSettingsResponse> call, Response<LanuageSettingsResponse> response) {

                        if (response.isSuccessful()) {
                            LanuageSettingsResponse server_response = response.body();

                            dialog.dismissLoadingDialog();

                            if (server_response.getStatus() == 200) {
                                dialog.dismissLoadingDialog();


                                //  setPreferences(server_response);


                            } else if (server_response.getStatus() == 400) {


                                dialog.dismissLoadingDialog();

                            }

                            else {
                                dialog.dismissLoadingDialog();



                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<LanuageSettingsResponse> call, Throwable t) {
                        dialog.dismissLoadingDialog();

                    }
                });
            } else {
                Toast toast = Toast.makeText(SettingsActivity.this, R.string.in, Toast.LENGTH_LONG);
                toast.setGravity(Gravity.CENTER, 0, 0);
                toast.show();
            }
        }


    }


}
