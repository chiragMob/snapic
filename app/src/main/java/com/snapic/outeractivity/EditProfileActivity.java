package com.snapic.outeractivity;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Bundle;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.view.Gravity;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.snapic.R;
import com.snapic.response.UserDetailResponse;
import com.snapic.response.UserUpdateDetailResponse;
import com.snapic.retrofit.ApiInterface;
import com.snapic.retrofit.RetrofitInit;
import com.snapic.utilities.DialogPopup;
import com.snapic.utilities.GetPath;
import com.snapic.utilities.InternetCheck;
import com.snapic.utilities.SPreferenceKey;
import com.snapic.utilities.SharedPreferenceWriter;
import com.snapic.utilities.TakeImage;
import com.theartofdev.edmodo.cropper.CropImage;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;
import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class EditProfileActivity extends AppCompatActivity {

    @BindView(R.id.cons_edit)
    ConstraintLayout cons_edit;
    @BindView(R.id.img_back)
    ImageView img_back;
    @BindView(R.id.txt_login)
    TextView txt_login;
    @BindView(R.id.img_pic)
    CircleImageView img_pic;
    @BindView(R.id.et_fname)
    EditText et_fname;
    @BindView(R.id.et_lname)
    EditText et_lname;
    @BindView(R.id.et_email)
    EditText et_email;
    @BindView(R.id.et_contry_code)
    TextView et_contry_code;
    @BindView(R.id.et_num)
    TextView et_num;
    @BindView(R.id.img_cam)
    ImageView img_cam;
    public static final int RESULT_LOAD_IMAGE = 1;
    public static final int CAMERA_REQUEST = 11;

    public static final int GALLERY_PICTURE = 11;
    BottomSheetDialog bottomSheetDialog;

    private static final int PICK_GALLERY = 102;
    private final static int TAKE_PICTURE = 100;
    private String imagePath, image1;
    private Uri mImageCaptureUri;
    private File file = null;
    String userImagePath="";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_profile);
        ButterKnife.bind(this);
        if (android.os.Build.VERSION.SDK_INT > 9)
        {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder()
                    .permitAll()
                    .penaltyLog()
                    .build();
            StrictMode.setThreadPolicy(policy);
        }
        serviceUserDetail();

        cons_edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(EditProfileActivity.this,EditMobileNumberActivity.class));
            }
        });
        img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        txt_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(et_fname.getText().toString().isEmpty())
                {
                    Toast.makeText(EditProfileActivity.this, R.string.first_name, Toast.LENGTH_SHORT).show();
                }
               else if(et_lname.getText().toString().isEmpty())
                {
                    Toast.makeText(EditProfileActivity.this, R.string.last_name, Toast.LENGTH_SHORT).show();
                }
                else if(et_email.getText().toString().isEmpty())
                {
                    Toast.makeText(EditProfileActivity.this, R.string.emailid, Toast.LENGTH_SHORT).show();
                }
                else
                {

                    CreateProfileApi();
                }


            }
        });
        img_cam.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectImage();
            }
        });
      //  serviceUserDetail();

    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    private void serviceUserDetail() {
        {
            if (new InternetCheck(EditProfileActivity.this).isConnect()) {
                String key = SharedPreferenceWriter.getInstance(EditProfileActivity.this).getString(SPreferenceKey.jwtToken);
                String id = SharedPreferenceWriter.getInstance(EditProfileActivity.this).getString(SPreferenceKey._id);
                String token="Token"+" "+key;

                //aray1@gmail.com,arya8055
                DialogPopup dialog = new DialogPopup(EditProfileActivity.this);
                dialog.showLoadingDialog(EditProfileActivity.this, "");

                ApiInterface api_service = RetrofitInit.getConnect().createConnection();
                Call<UserDetailResponse> call = api_service.userDetail(token,id);
                call.enqueue(new Callback<UserDetailResponse>() {
                    @Override
                    public void onResponse(Call<UserDetailResponse> call, Response<UserDetailResponse> response) {
                        if(response.code()==401)
                        {
                            startActivity(new Intent(EditProfileActivity.this, LoginActivity.class));
                            SharedPreferenceWriter.getInstance(EditProfileActivity.this).writeStringValue(SPreferenceKey._id, "");
                            SharedPreferenceWriter.getInstance(EditProfileActivity.this).writeStringValue(SPreferenceKey.jwtToken, "");

                        }
                        else
                        {
                            if (response.isSuccessful()) {
                                UserDetailResponse server_response = response.body();
                                if(server_response.getDetail().isEmpty())
                                {
                                    if (server_response.getStatus() == 200) {
                                        dialog.dismissLoadingDialog();
                                        et_fname.setText(server_response.getData().getFirst_name());
                                        et_lname.setText(server_response.getData().getLast_name());
                                        et_contry_code.setText(server_response.getData().getCountry_code());
                                        et_num.setText(server_response.getData().getPhone_number());
                                        if(!server_response.getData().getEmail().contains("@"))
                                        {

                                        }
                                        else
                                        {
                                            et_email.setText(server_response.getData().getEmail());

                                        }
                                        //  setPreferences(server_response);
                                        if (!server_response.getData().getProfile_pic().isEmpty()) {
                                            String pic="https://pagiupload.s3.amazonaws.com/"+server_response.getData().getProfile_pic();
                                            Glide.with(EditProfileActivity.this).load(server_response.getData().getProfile_pic()).into(img_pic);


                                        } else {

                                            Glide.with(EditProfileActivity.this).load(R.drawable.default_profile).into(img_pic);



                                            //Glide.with(EditProfileActivity.this).load(R.drawable.pro_pic).into(profile_et_img);


                                        }

                                    }

                                    else if (server_response.getStatus() == 404) {


                                        dialog.dismissLoadingDialog();
                                        startActivity(new Intent(EditProfileActivity.this,LoginActivity.class));


                                    }


                                    else if (server_response.getStatus() == 400) {


                                        dialog.dismissLoadingDialog();
                                        Toast.makeText(EditProfileActivity.this, ""+server_response.getStatus(), Toast.LENGTH_SHORT).show();

                                    }
                                }
                                else {
                                    dialog.dismissLoadingDialog();
                                    Toast.makeText(EditProfileActivity.this, ""+server_response.getStatus(), Toast.LENGTH_SHORT).show();

                                    if (server_response.getDetail().equals("Invalid token."))
                                    {
                                        startActivity(new Intent(EditProfileActivity.this,LoginActivity.class));
                                    }

                                }
                            }

                        }
                    }

                    @Override
                    public void onFailure(Call<UserDetailResponse> call, Throwable t) {
                        dialog.dismissLoadingDialog();

                    }
                });
            } else {
                Toast toast = Toast.makeText(EditProfileActivity.this, R.string.in, Toast.LENGTH_LONG);
                toast.setGravity(Gravity.CENTER, 0, 0);
                toast.show();
            }
        }


    }

    private void selectImage() {
        TextView gallery_tv, Camera_tv, Cancel_tv;
        bottomSheetDialog = new BottomSheetDialog(this);
        View sheetView = getLayoutInflater().inflate(R.layout.dialog_camera_gallery, null);
        bottomSheetDialog.setContentView(sheetView);
        bottomSheetDialog.getWindow().findViewById(R.id.design_bottom_sheet).setBackgroundResource(android.R.color.transparent);
        bottomSheetDialog.show();
        gallery_tv = sheetView.findViewById(R.id.gallery_tv);
        Camera_tv = sheetView.findViewById(R.id.camera_tv);
        Cancel_tv = sheetView.findViewById(R.id.cancel_tv);
        Camera_tv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(EditProfileActivity.this, TakeImage.class);
                intent.putExtra("from", "camera");
                startActivityForResult(intent, CAMERA_REQUEST);

                bottomSheetDialog.dismiss();

            }
        });
        gallery_tv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String[] mimeTypes ={"image/*"};

                Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
                intent.addCategory(Intent.CATEGORY_OPENABLE);

                intent.setType("image/*");
                //intent.putExtra(Intent.EXTRA_MIME_TYPES, mimeTypes);
                startActivityForResult(Intent.createChooser(intent,"ChooseFile"), GALLERY_PICTURE);
                bottomSheetDialog.dismiss();
            }
        });
        Cancel_tv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bottomSheetDialog.dismiss();
            }
        });

    }


    private void takePictureFromCamera(Context context, String imagePath) {
        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        try {
            mImageCaptureUri = Uri.fromFile(new File(getExternalFilesDir("temp"), imagePath));
            intent.putExtra(MediaStore.EXTRA_OUTPUT, mImageCaptureUri);
            intent.putExtra("return-data", true);
            startActivityForResult(intent, TAKE_PICTURE);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);




        if (requestCode == CAMERA_REQUEST && resultCode == RESULT_OK) {
            userImagePath = data.getStringExtra("filePath");
            if (userImagePath != null) {
                File imgFile = new File(data.getStringExtra("filePath"));
                if (imgFile.exists()) {
                    //ivMyProfileEdit.setImageURI(Uri.fromFile(imgFile));
                    Glide.with(this)
                            .load(imgFile)
                            .into(img_pic);
                    CropImage.activity(Uri.fromFile(imgFile))
                            .start(this);
                }
            }
        }
        if (requestCode == GALLERY_PICTURE && resultCode == RESULT_OK) {
            try
            {
                Uri selectedImageUri = data.getData();


                String selectedImagePath;


                selectedImagePath= GetPath.getPath(EditProfileActivity.this,selectedImageUri);




                userImagePath=selectedImagePath;
                Uri selectedImageUris =getImageUri(this,rotate(BitmapFactory.decodeFile(selectedImagePath),selectedImagePath));
                CropImage.activity(selectedImageUris)
                        .start(this);

            }catch (Exception e)
            {
                e.printStackTrace();
            }

        }



        else if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                // Uri resultUri = result.getUri();
                Uri resultUri = result.getUri();
                userImagePath=resultUri.getPath();


                try {
                    Glide.with(EditProfileActivity.this).load(rotate(BitmapFactory.decodeFile(userImagePath),userImagePath)).into(img_pic);
                } catch (IOException e) {
                    e.printStackTrace();
                }


            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Exception error = result.getError();
            }
        }}

    public Map<String, RequestBody> getParam() {

        Map<String, RequestBody> map = new HashMap<>();

        map.put("email", RequestBody.create(MediaType.parse("text/plain"), et_email.getText().toString()));
        map.put("first_name", RequestBody.create(MediaType.parse("text/plain"), et_fname.getText().toString()));
        map.put("last_name", RequestBody.create(MediaType.parse("text/plain"), et_lname.getText().toString()));
        return map;
    }

    private Uri getImageUri(Context context, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(context.getContentResolver(), inImage, "Title", null);
        return Uri.parse(path);
    }
    public static Bitmap rotate(Bitmap bitmap, String image_absolute_path) throws IOException {
        ExifInterface ei = new ExifInterface(image_absolute_path);
        int orientation = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);

        switch (orientation) {
            case ExifInterface.ORIENTATION_ROTATE_90:
                return rotate(bitmap, 90);

            case ExifInterface.ORIENTATION_ROTATE_180:
                return rotate(bitmap, 180);

            case ExifInterface.ORIENTATION_ROTATE_270:
                return rotate(bitmap, 270);

            case ExifInterface.ORIENTATION_FLIP_HORIZONTAL:
                return flip(bitmap, true, false);

            case ExifInterface.ORIENTATION_FLIP_VERTICAL:
                return flip(bitmap, false, true);

            default:
                return bitmap;
        }
    }

    public static Bitmap rotate(Bitmap bitmap, float degrees) {
        Matrix matrix = new Matrix();
        matrix.postRotate(degrees);
        return Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);
    }

    public static Bitmap flip(Bitmap bitmap, boolean horizontal, boolean vertical) {
        Matrix matrix = new Matrix();
        matrix.preScale(horizontal ? -1 : 1, vertical ? -1 : 1);
        return Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);
    }

    private void CreateProfileApi() {


        RequestBody requestBody = null;
        MultipartBody.Part part = null;
        if (!userImagePath.equalsIgnoreCase("")) {
            File file = new File(userImagePath);
            requestBody = RequestBody.create(MediaType.parse("image/*"), file);
            part = MultipartBody.Part.createFormData("profile_pic", file.getName(), requestBody);
            System.out.print(part);
        }

        if (new InternetCheck(this).isConnect()) {
            String key = SharedPreferenceWriter.getInstance(EditProfileActivity.this).getString(SPreferenceKey.jwtToken);
            String id = SharedPreferenceWriter.getInstance(EditProfileActivity.this).getString(SPreferenceKey._id);
            String token="Token"+" "+key;
            DialogPopup dialogs = new DialogPopup(this);
            dialogs.showLoadingDialog(this, "");
            System.out.print(token);

            ApiInterface api_service = RetrofitInit.getConnect().createConnection();
            Call<UserUpdateDetailResponse> call = api_service.updateUserDetails(token,id, getParam(), part);
            call.enqueue(new Callback<UserUpdateDetailResponse>() {
                @Override
                public void onResponse(Call<UserUpdateDetailResponse> call, Response<UserUpdateDetailResponse> response) {
                    if (response.isSuccessful()) {
                        UserUpdateDetailResponse server_response = response.body();
                        if (server_response.getStatus() == 200) {

                            dialogs.dismissLoadingDialog();
                            Toast.makeText(EditProfileActivity.this, server_response.getMessage(), Toast.LENGTH_SHORT).show();

                            finish();
//                            dialog.dismiss();
                            // fireBaseAction();
                            //  showDialogotp();


                        } else if (server_response.getStatus() == 500) {
                            dialogs.dismissLoadingDialog();

                            //  startActivity(new Intent(EditProfileActivity.this, LoginActivity.class));
                        } else if (server_response.getStatus() == 400) {
                            dialogs.dismissLoadingDialog();


                        } else {
                            dialogs.dismissLoadingDialog();

                            Toast.makeText(EditProfileActivity.this, server_response.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    }
                }

                @Override
                public void onFailure(Call<UserUpdateDetailResponse> call, Throwable t) {
                    dialogs.dismissLoadingDialog();

                }
            });
        } else {
            Toast toast = Toast.makeText(this, R.string.in, Toast.LENGTH_LONG);
            toast.setGravity(Gravity.CENTER, 0, 0);
            toast.show();
        }


    }


}