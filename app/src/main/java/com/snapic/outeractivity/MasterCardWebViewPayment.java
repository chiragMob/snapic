package com.snapic.outeractivity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.Gravity;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;


import com.snapic.R;
import com.snapic.activity.HomeActivity;
import com.snapic.response.PaymentDoneResponse;
import com.snapic.retrofit.ApiInterface;
import com.snapic.retrofit.RetrofitInit;
import com.snapic.utilities.DialogPopup;
import com.snapic.utilities.InternetCheck;
import com.snapic.utilities.SPreferenceKey;
import com.snapic.utilities.SharedPreferenceWriter;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import androidx.appcompat.app.AppCompatActivity;
import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MasterCardWebViewPayment extends AppCompatActivity {

    @BindView(R.id.webUrl)
    WebView webUrl;
    String orderid, amoount, purpuse, email, donationid;
    String supid, sellerid;
    String data;
    String appointdate,paymentid,operateid;
    String ticketid, quantity;
    String Appoinid, deliverdate, addressid;
    String total="";
String callss="";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_web_views);
        ButterKnife.bind(this);
        getIntentData();
        DialogPopup dialog = new DialogPopup(MasterCardWebViewPayment.this);
        dialog.showLoadingDialog(MasterCardWebViewPayment.this, "");
        webUrl.setWebViewClient(new WebViewClient() {
            @Override
            public void onPageFinished(WebView view, String url) {
                DialogPopup dialog = new DialogPopup(MasterCardWebViewPayment.this);
                dialog.dismissLoadingDialog();
                super.onPageFinished(view, url);

                try {
                    if (url.contains("https://www.google.com")) {
                        if(callss.equals("done"))
                        {
                            startActivity(new Intent(MasterCardWebViewPayment.this,HomeActivity.class));

                        }
                        else
                        {
                            servicePayment(orderid,paymentid,"na");

                        }


                        //  paymentDetail(mobilenumber,amoount,purpuse,paymentId,email);
                    }
                    else if(url.contains("https://www.aps.iq/Ar_Home.aspx"))
                    {
                        startActivity(new Intent(MasterCardWebViewPayment.this,HomeActivity.class));
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                super.onPageStarted(view, url, favicon);

                //    CommonUtilities.showLoadingDialog(WebViewAct.this);
            }

        });

    }
    private void servicePayment(String amount,String paymentid,String operatedid) {
        {
            if (new InternetCheck(this).isConnect()) {
                //aray1@gmail.com,arya8055
                String key = SharedPreferenceWriter.getInstance(MasterCardWebViewPayment.this).getString(SPreferenceKey.jwtToken);
                String token="Token"+" "+key;
                DialogPopup dialog = new DialogPopup(this);
                dialog.showLoadingDialog(this, "");
                String date = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault()).format(new Date());

                ApiInterface api_service = RetrofitInit.getConnect().createConnection();
                Call<PaymentDoneResponse> call = api_service.paymentDone(token,orderid,paymentid,amount,"master card",date,operatedid);
                call.enqueue(new Callback<PaymentDoneResponse>() {
                    @Override
                    public void onResponse(Call<PaymentDoneResponse> call, Response<PaymentDoneResponse> response) {
                        if (response.isSuccessful()) {
                            PaymentDoneResponse server_response = response.body();
                            if (server_response.getStatus() == 200) {
                                dialog.dismissLoadingDialog();
                                callss="done";
                                SharedPreferenceWriter.getInstance(MasterCardWebViewPayment.this).writeStringValue(SPreferenceKey.id1,"");
                                SharedPreferenceWriter.getInstance(MasterCardWebViewPayment.this).writeStringValue(SPreferenceKey.id2,"");
                                SharedPreferenceWriter.getInstance(MasterCardWebViewPayment.this).writeStringValue(SPreferenceKey.id3,"");
                                SharedPreferenceWriter.getInstance(MasterCardWebViewPayment.this).writeStringValue(SPreferenceKey.id4,"");
                                SharedPreferenceWriter.getInstance(MasterCardWebViewPayment.this).writeStringValue(SPreferenceKey.id5,"");
                                SharedPreferenceWriter.getInstance(MasterCardWebViewPayment.this).writeStringValue(SPreferenceKey.id6,"");
                                SharedPreferenceWriter.getInstance(MasterCardWebViewPayment.this).writeStringValue(SPreferenceKey.id7,"");
                                SharedPreferenceWriter.getInstance(MasterCardWebViewPayment.this).writeStringValue(SPreferenceKey.id8,"");
                                SharedPreferenceWriter.getInstance(MasterCardWebViewPayment.this).writeStringValue(SPreferenceKey.id9,"");
                                SharedPreferenceWriter.getInstance(MasterCardWebViewPayment.this).writeStringValue(SPreferenceKey.id10,"");


                                startActivity(new Intent(MasterCardWebViewPayment.this, HomeActivity.class));
                            }  else {
                                dialog.dismissLoadingDialog();
                                Toast.makeText(MasterCardWebViewPayment.this, ""+server_response.getMessage(), Toast.LENGTH_SHORT).show();
                                startActivity(new Intent(MasterCardWebViewPayment.this, HomeActivity.class));

                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<PaymentDoneResponse> call, Throwable t) {
                        dialog.dismissLoadingDialog();

                    }
                });
            } else {
                Toast toast = Toast.makeText(this, "Internet connection lost", Toast.LENGTH_LONG);
                toast.setGravity(Gravity.CENTER, 0, 0);
                toast.show();
            }
        }


    }

//    private void serviceDonationBooking(String id, String paymentid) {
//        {
//            String value;
//
//
//            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
//            String formattedDate = dateFormat.format(new Date()).toString();
//
//
//            if (new InternetCheck(MasterCardWebViewPayment.this).isConnect()) {
//                String key = SharedPreferenceWriter.getInstance(MasterCardWebViewPayment.this).getString(SPreferenceKey.jwtToken);
//
//                //aray1@gmail.com,arya8055
//                DialogPopup dialog = new DialogPopup(MasterCardWebViewPayment.this);
//                dialog.showLoadingDialog(MasterCardWebViewPayment.this, "");
//
//                ApiInterface api_service = RetrofitInit.getConnect().createConnection();
//                Call<DonationAmountResponse> call = api_service.
//                        DonationBookingAmount(key, "application/x-www-form-urlencoded", id, amoount, "Card", paymentid, "Success", formattedDate);
//                call.enqueue(new Callback<DonationAmountResponse>() {
//                    @Override
//                    public void onResponse(Call<DonationAmountResponse> call, Response<DonationAmountResponse> response) {
//                        if (response.isSuccessful()) {
//                            DonationAmountResponse server_response = response.body();
//                            if (server_response.getStatus() == 200) {
//                                dialog.dismissLoadingDialog();
//                                //  setPreferences(server_response);
//
//                                Toast.makeText(MasterCardWebViewPayment.this, "Thank you for your contribution! Your donation is greatly appreciated.", Toast.LENGTH_SHORT).show();
//
//                                Intent i = new Intent(MasterCardWebViewPayment.this, HomeActivity.class);
//                                i.putExtra("id", "");
//
//                                MasterCardWebViewPayment.this.startActivity(i);
//
//
//                            } else if (server_response.getStatus() == 400) {
//                                dialog.dismissLoadingDialog();
//
//                            } else if (server_response.getStatus() == 500) {
//                                dialog.dismissLoadingDialog();
//
//                              //  startActivity(new Intent(MasterCardWebViewPayment.this, LoginActivity.class));
//                            } else {
//                                dialog.dismissLoadingDialog();
//
//                                Toast.makeText(MasterCardWebViewPayment.this, server_response.getMessage(), Toast.LENGTH_SHORT).show();
//                            }
//                        }
//                    }
//
//                    @Override
//                    public void onFailure(Call<DonationAmountResponse> call, Throwable t) {
//                        dialog.dismissLoadingDialog();
//
//                    }
//                });
//            } else {
//                Toast toast = Toast.makeText(MasterCardWebViewPayment.this, "Internet connection lost", Toast.LENGTH_LONG);
//                toast.setGravity(Gravity.CENTER, 0, 0);
//                toast.show();
//            }
//        }
//
//
//    }

//    private void serviceSupBooking(String paymentid) {
//        {
//            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
//            String formattedDate = dateFormat.format(new Date()).toString();
//
//            if (new InternetCheck(MasterCardWebViewPayment.this).isConnect()) {
//                String key = SharedPreferenceWriter.getInstance(MasterCardWebViewPayment.this).getString(SPreferenceKey.jwtToken);
//
//                //aray1@gmail.com,arya8055
//                DialogPopup dialog = new DialogPopup(MasterCardWebViewPayment.this);
//                dialog.showLoadingDialog(MasterCardWebViewPayment.this, "");
//
//                ApiInterface api_service = RetrofitInit.getConnect().createConnection();
//                Call<SubcriptionPaymentResponse> call = api_service.
//                        SubscriptionBooking(key, "application/x-www-form-urlencoded", sellerid, supid, "Card", paymentid, "Success", formattedDate);
//                call.enqueue(new Callback<SubcriptionPaymentResponse>() {
//                    @Override
//                    public void onResponse(Call<SubcriptionPaymentResponse> call, Response<SubcriptionPaymentResponse> response) {
//                        if (response.isSuccessful()) {
//                            SubcriptionPaymentResponse server_response = response.body();
//                            if (server_response.getStatus() == 200) {
//                                dialog.dismissLoadingDialog();
//                                //  setPreferences(server_response);
//
//                                Toast.makeText(MasterCardWebViewPayment.this, "Subscription plan purchased successfully.", Toast.LENGTH_SHORT).show();
//                                Intent i = new Intent(MasterCardWebViewPayment.this, HomeActivity.class);
//                                i.putExtra("id", "");
//                                MasterCardWebViewPayment.this.startActivity(i);
//
//
//                            } else if (server_response.getStatus() == 400) {
//                                dialog.dismissLoadingDialog();
//
//                            } else if (server_response.getStatus() == 500) {
//                                dialog.dismissLoadingDialog();
//
//                            } else {
//                                dialog.dismissLoadingDialog();
//
//                                Toast.makeText(MasterCardWebViewPayment.this, server_response.getMessage(), Toast.LENGTH_SHORT).show();
//                            }
//                        }
//                    }
//
//                    @Override
//                    public void onFailure(Call<SubcriptionPaymentResponse> call, Throwable t) {
//                        dialog.dismissLoadingDialog();
//
//                    }
//                });
//            } else {
//                Toast toast = Toast.makeText(MasterCardWebViewPayment.this, "Internet connection lost", Toast.LENGTH_LONG);
//                toast.setGravity(Gravity.CENTER, 0, 0);
//                toast.show();
//            }
//        }
//
//
//    }

    private void getIntentData() {
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            // set title of toolbar
            Intent i = getIntent();
            orderid = i.getStringExtra("orderid");
            total=i.getStringExtra("total");
            paymentid=i.getStringExtra("paymentid");

            String url = i.getStringExtra("id");
            webUrl.getSettings().setJavaScriptEnabled(true);
            webUrl.loadUrl(url);



        }
    }
    private void servicePayment(String orderid,String amount,String paymentid,String operatedid) {
        {
            if (new InternetCheck(this).isConnect()) {
                //aray1@gmail.com,arya8055
                String key = SharedPreferenceWriter.getInstance(MasterCardWebViewPayment.this).getString(SPreferenceKey.jwtToken);
                String token="Token"+" "+key;
                DialogPopup dialog = new DialogPopup(this);
                dialog.showLoadingDialog(this, "");
                String date = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault()).format(new Date());

                ApiInterface api_service = RetrofitInit.getConnect().createConnection();
                Call<PaymentDoneResponse> call = api_service.paymentDone(token,orderid,paymentid,amount,"master card",date,operatedid);
                call.enqueue(new Callback<PaymentDoneResponse>() {
                    @Override
                    public void onResponse(Call<PaymentDoneResponse> call, Response<PaymentDoneResponse> response) {
                        if (response.isSuccessful()) {
                            PaymentDoneResponse server_response = response.body();
                            if (server_response.getStatus() == 200) {
                                dialog.dismissLoadingDialog();
                                SharedPreferenceWriter.getInstance(MasterCardWebViewPayment.this).writeStringValue(SPreferenceKey.id1,"");
                                SharedPreferenceWriter.getInstance(MasterCardWebViewPayment.this).writeStringValue(SPreferenceKey.id2,"");
                                SharedPreferenceWriter.getInstance(MasterCardWebViewPayment.this).writeStringValue(SPreferenceKey.id3,"");
                                SharedPreferenceWriter.getInstance(MasterCardWebViewPayment.this).writeStringValue(SPreferenceKey.id4,"");
                                SharedPreferenceWriter.getInstance(MasterCardWebViewPayment.this).writeStringValue(SPreferenceKey.id5,"");
                                SharedPreferenceWriter.getInstance(MasterCardWebViewPayment.this).writeStringValue(SPreferenceKey.id6,"");
                                SharedPreferenceWriter.getInstance(MasterCardWebViewPayment.this).writeStringValue(SPreferenceKey.id7,"");
                                SharedPreferenceWriter.getInstance(MasterCardWebViewPayment.this).writeStringValue(SPreferenceKey.id8,"");
                                SharedPreferenceWriter.getInstance(MasterCardWebViewPayment.this).writeStringValue(SPreferenceKey.id9,"");
                                SharedPreferenceWriter.getInstance(MasterCardWebViewPayment.this).writeStringValue(SPreferenceKey.id10,"");


                                startActivity(new Intent(MasterCardWebViewPayment.this, HomeActivity.class));
                            }  else {
                                dialog.dismissLoadingDialog();
                                Toast.makeText(MasterCardWebViewPayment.this, ""+server_response.getMessage(), Toast.LENGTH_SHORT).show();
                                startActivity(new Intent(MasterCardWebViewPayment.this, HomeActivity.class));

                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<PaymentDoneResponse> call, Throwable t) {
                        dialog.dismissLoadingDialog();

                    }
                });
            } else {
                Toast toast = Toast.makeText(this, "Internet connection lost", Toast.LENGTH_LONG);
                toast.setGravity(Gravity.CENTER, 0, 0);
                toast.show();
            }
        }


    }



//    private void serviceTicketBooking(String paymentid) {
//        {
//
//            if (new InternetCheck(MasterCardWebViewPayment.this).isConnect()) {
//                String key = SharedPreferenceWriter.getInstance(MasterCardWebViewPayment.this).getString(SPreferenceKey.jwtToken);
//
//                //aray1@gmail.com,arya8055
//                DialogPopup dialog = new DialogPopup(MasterCardWebViewPayment.this);
//                dialog.showLoadingDialog(MasterCardWebViewPayment.this, "");
//
//                ApiInterface api_service = RetrofitInit.getConnect().createConnection();
//                Call<TicketBookingResponse> call = api_service.
//                        ticketBooking(key, "application/x-www-form-urlencoded", ticketid, amoount, "Card", paymentid, "Success", "0", amoount, quantity);
//                call.enqueue(new Callback<TicketBookingResponse>() {
//                    @Override
//                    public void onResponse(Call<TicketBookingResponse> call, Response<TicketBookingResponse> response) {
//                        if (response.isSuccessful()) {
//                            TicketBookingResponse server_response = response.body();
//                            if (server_response.getStatus() == 200) {
//                                dialog.dismissLoadingDialog();
//                                //  setPreferences(server_response);
//
//                                Toast.makeText(MasterCardWebViewPayment.this, "Ticket is successfully purchased. Your booking is confirmed.", Toast.LENGTH_SHORT).show();
//
//                                Intent i = new Intent(MasterCardWebViewPayment.this, HomeActivity.class);
//                                i.putExtra("id", "");
//
//                                MasterCardWebViewPayment.this.startActivity(i);
//
//
//                            } else if (server_response.getStatus() == 400) {
//                                dialog.dismissLoadingDialog();
//
//                            } else if (server_response.getStatus() == 500) {
//                                dialog.dismissLoadingDialog();
//
//                                startActivity(new Intent(MasterCardWebViewPayment.this, LoginActivity.class));
//                            } else {
//                                dialog.dismissLoadingDialog();
//
//                                Toast.makeText(MasterCardWebViewPayment.this, server_response.getMessage(), Toast.LENGTH_SHORT).show();
//                            }
//                        }
//                    }
//
//                    @Override
//                    public void onFailure(Call<TicketBookingResponse> call, Throwable t) {
//                        dialog.dismissLoadingDialog();
//
//                    }
//                });
//            } else {
//                Toast toast = Toast.makeText(MasterCardWebViewPayment.this, "Internet connection lost", Toast.LENGTH_LONG);
//                toast.setGravity(Gravity.CENTER, 0, 0);
//                toast.show();
//            }
//        }
//    }




}
