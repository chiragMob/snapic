package com.snapic.outeractivity;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.snapic.R;
import com.snapic.response.OrderDetailResponse;
import com.snapic.retrofit.ApiInterface;
import com.snapic.retrofit.RetrofitInit;
import com.snapic.utilities.DialogPopup;
import com.snapic.utilities.InternetCheck;
import com.snapic.utilities.SPreferenceKey;
import com.snapic.utilities.SharedPreferenceWriter;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import androidx.appcompat.app.AppCompatActivity;
import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class OrderDetailActivity extends AppCompatActivity {
    @BindView(R.id.img_back)
    ImageView img_back;
    @BindView(R.id.img_src)
    ImageView img_src;
    @BindView(R.id.tv_mode)
    TextView tv_mode;
    @BindView(R.id.tv_date)
    TextView tv_date;
    @BindView(R.id.tv_time)
    TextView tv_time;
    @BindView(R.id.tv_deliver)
    TextView tv_deliver;
    @BindView(R.id.tv_amount)
    TextView tv_amount;
    @BindView(R.id.tv_ex)
    TextView tv_ex;
    @BindView(R.id.tv_dis)
    TextView tv_dis;
    @BindView(R.id.tv_to_am)
    TextView tv_to_am;
    String id="";
    @BindView(R.id.view11)
    View view11;
    @BindView(R.id.view12)
   View  View12;
    @BindView(R.id.imageView17)
    ImageView imageView17;
    @BindView(R.id.imageView18)
    ImageView imageView18;
@BindView(R.id.tv_discou)
    TextView tv_discou;
@BindView(R.id.tv_pay)
TextView tv_pay;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_detail);
        ButterKnife.bind(this);
        Intent i=getIntent();
        id =i.getStringExtra("id");

        if(id.isEmpty())
        {
            addressDetail(id);

        }
        img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

    }

    @Override
    protected void onResume() {
        super.onResume();
        if(!id.isEmpty())
        {
            addressDetail(id);
        }
    }
    private void addressDetail(String id) {
        {
            String key = SharedPreferenceWriter.getInstance(OrderDetailActivity.this).getString(SPreferenceKey.jwtToken);
            String token="Token"+" "+key;

            if (new InternetCheck(OrderDetailActivity.this).isConnect()) {

                //aray1@gmail.com,arya8055
                DialogPopup dialog = new DialogPopup(OrderDetailActivity.this);
                dialog.showLoadingDialog(OrderDetailActivity.this, "");
                System.out.print(key);
                System.out.print(id);

                ApiInterface api_service = RetrofitInit.getConnect().createConnection();
                Call<OrderDetailResponse> call = api_service.getOrderDetails(token,id);
                call.enqueue(new Callback<OrderDetailResponse>() {
                    @Override
                    public void onResponse(Call<OrderDetailResponse> call, Response<OrderDetailResponse> response) {
                        if (response.isSuccessful()) {
                            OrderDetailResponse server_response = response.body();
                            if (server_response.getStatus() == 200) {
                                dialog.dismissLoadingDialog();
                                String abc;
                                tv_to_am.setText(String.valueOf(server_response.getData().getAmount()+server_response.getData().getExtra_charges()-server_response.getData().getDiscount())+ " IO D");
                                if(server_response.getData().getDiscount()>0)
                                {
                                    tv_discou.setVisibility(View.VISIBLE);
                                    tv_dis.setText(String.valueOf(server_response.getData().getDiscount())+ " IQ D");

                                }
                                else
                                {
                                    tv_discou.setVisibility(View.GONE);

                                }                                tv_amount.setText(String.valueOf(server_response.getData().getAmount())+ " IQ D");
                                tv_mode.setText(server_response.getData().get_$ModeOfPayment8());
                                if(server_response.getData().get_$ModeOfPayment8().equals("cash"))
                                {
                                    tv_pay.setText(R.string.payable_amount);
                                }
                                else
                                {
                                    tv_pay.setText(R.string.amount);

                                }
                                if(server_response.getData().getStatus().equals("Order placed"))
                                {

                                }
                                else if(server_response.getData().getStatus().equals("Out for Delivery"))
                                {
imageView17.setImageResource((R.drawable.radio_ss));
view11.setBackgroundColor(getResources().getColor(R.color.colorBlue));
                                }
                                else if(server_response.getData().getStatus().equals("Delivered"))
                                {
                                    imageView17.setBackground(getResources().getDrawable(R.drawable.radio_ss));
                                    view11.setBackgroundColor(getResources().getColor(R.color.colorBlue));
                                    imageView18.setBackground(getResources().getDrawable(R.drawable.radio_ss));
                                    View12.setBackgroundColor(getResources().getColor(R.color.colorBlue));
                                }





                                tv_ex.setText(String.valueOf(server_response.getData().getExtra_charges())+ " IQ D");
                                Glide.with(OrderDetailActivity.this).load(server_response.getData().getImage()).into(img_src);

                                if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                                    SimpleDateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
                                    SimpleDateFormat outputFormat = new SimpleDateFormat("HH:mm");
                                    ;
                                    SimpleDateFormat yeardata = new SimpleDateFormat("yyyy");
                                    SimpleDateFormat monthdata = new SimpleDateFormat("MM");
                                    SimpleDateFormat datedata = new SimpleDateFormat("dd");
                                    SimpleDateFormat fromat = new SimpleDateFormat("yyyy-MM-dd");






                                    Date dateEndd = null;
                                    try {
                                        dateEndd = inputFormat.parse((server_response.getData().getCreated_at()));
                                        String updateTime = outputFormat.format(dateEndd);
                                        String yeardatastr = yeardata.format(dateEndd);
                                        String monthdatastr = monthdata.format(dateEndd);
                                        String datedatastr = datedata.format(dateEndd);
                                        String date = fromat.format(dateEndd);
                                     tv_date.setText(date);
                                     tv_time.setText(updateTime);
                                        if(date.equals(server_response.getData().getCustom_delivery_date()))
                                        {
                                            tv_deliver.setText(getString(R.string.expect)+ server_response.getData().getDelivery_date());


                                        }
                                        else
                                        {
                                            tv_deliver.setText(getString(R.string.expect)+ server_response.getData().getCustom_delivery_date());


                                        }

                                    } catch (ParseException e) {
                                        e.printStackTrace();
                                    }
                                }


                                //  setPreferences(server_response);


                            } else if (server_response.getStatus() == 400) {
                                dialog.dismissLoadingDialog();

                            } else if (server_response.getStatus() == 500) {
                                dialog.dismissLoadingDialog();

                                //   startActivity(new Intent(DonationDetailsActivity.this, LoginActivity.class));
                            } else {
                                dialog.dismissLoadingDialog();

                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<OrderDetailResponse> call, Throwable t) {
                        dialog.dismissLoadingDialog();

                    }
                });
            } else {
                Toast toast = Toast.makeText(OrderDetailActivity.this, R.string.in, Toast.LENGTH_LONG);
                toast.setGravity(Gravity.CENTER, 0, 0);
                toast.show();
            }
        }
    }
}

