package com.snapic.outeractivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.snapic.R;
import com.snapic.adapter.AddressAdapter;
import com.snapic.response.AddressListResponse;
import com.snapic.response.DeleteAddressResponse;
import com.snapic.retrofit.ApiInterface;
import com.snapic.retrofit.RetrofitInit;
import com.snapic.utilities.DialogPopup;
import com.snapic.utilities.InternetCheck;
import com.snapic.utilities.SPreferenceKey;
import com.snapic.utilities.SharedPreferenceWriter;

import java.util.ArrayList;

import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class MyAddressActivity extends AppCompatActivity implements AddressClickListenr{
    @BindView(R.id.address)
    ConstraintLayout address;
    @BindView(R.id.img_back)
    ImageView img_back;
    @BindView(R.id.recycle_address)
    RecyclerView recycle_address;
    @BindView(R.id.txt_login)
    TextView txt_login;
    AddressAdapter addressAdapter;
    private ArrayList<AddressListResponse.DataBean> car_list = new ArrayList<>();

String ids="";
String prmo="";
String size="";
    ArrayList<String>  imagePathList=new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Intent i = getIntent();


        prmo = i.getStringExtra("promos");
        size=i.getStringExtra("size");
        if(getIntent().getStringArrayListExtra("test")!=null) {

            imagePathList = getIntent().getStringArrayListExtra("test");
        }
        setContentView(R.layout.activity_address);
        ButterKnife.bind(this);
        address.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(MyAddressActivity.this, AddressActivity.class);
                i.putExtra("promos", prmo);
                i.putStringArrayListExtra("test", imagePathList);
                i.putExtra("size",size);




                MyAddressActivity.this.startActivity(i);

            }
        });


        img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        serviceCarList();

        updateAdapter();

    }

    @Override
    protected void onResume() {
        super.onResume();
        serviceCarList();
        txt_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(car_list.size()==0)
                {
                    Toast.makeText(MyAddressActivity.this, R.string.add, Toast.LENGTH_SHORT).show();
                }
                else if(car_list.size()==1)
                {
                    int i=0;
                    String pic="",id="",model="",number="",carmake="";
                    for(int j=0;j<car_list.size();j++)
                    {

                            i=1;
                            id=String.valueOf(car_list.get(j).getId());


                    }
                    if(i==1)
                    {
                        Intent ij = new Intent(MyAddressActivity.this, BookActivity.class);
                        ij.putExtra("id", id);
                        ij.putExtra("promos", prmo);
                        ij.putExtra("size",size);

                        ij.putStringArrayListExtra("test", imagePathList);

                        MyAddressActivity.this.startActivity(ij);

                    }
                }


                else
                {
                    int i=0;
                    String pic="",id="",model="",number="",carmake="";
                    for(int j=0;j<car_list.size();j++)
                    {
                        if(car_list.get(j).getSelected())
                        {
                            i=1;
                            id=String.valueOf(car_list.get(j).getId());

                        }
                    }
                    if(i==1)
                    {
                        Intent ij = new Intent(MyAddressActivity.this, BookActivity.class);
                        ij.putExtra("id", id);
                        ij.putExtra("promos", prmo);
                        ij.putExtra("size",size);

                        ij.putStringArrayListExtra("test", imagePathList);

                        MyAddressActivity.this.startActivity(ij);

                    }
                    else
                    {

                        Toast.makeText(MyAddressActivity.this, R.string.selectsss, Toast.LENGTH_SHORT).show();

                    }
                }



            }
        });

    }

    void updateAdapter() {
        {
            addressAdapter = new AddressAdapter(this, car_list,this);
            recycle_address.setLayoutManager(new LinearLayoutManager(
                    this,
                    LinearLayoutManager.VERTICAL,
                    false));
            recycle_address.setHasFixedSize(true);

            recycle_address.setAdapter(addressAdapter);

        }
    }
    private void serviceRemoveCar(String ids) {
        {
            if (new InternetCheck(this).isConnect()) {
                //aray1@gmail.com,arya8055
                String key = SharedPreferenceWriter.getInstance(MyAddressActivity.this).getString(SPreferenceKey.jwtToken);
                String id = SharedPreferenceWriter.getInstance(MyAddressActivity.this).getString(SPreferenceKey._id);
                String token="Token"+" "+key;
                DialogPopup dialog = new DialogPopup(this);
                dialog.showLoadingDialog(this, "");

                ApiInterface api_service = RetrofitInit.getConnect().createConnection();
                Call<DeleteAddressResponse> call = api_service.deleteAddress(token,ids);
                call.enqueue(new Callback<DeleteAddressResponse>() {
                    @Override
                    public void onResponse(Call<DeleteAddressResponse> call, Response<DeleteAddressResponse> response) {
                        if (response.isSuccessful()) {
                            DeleteAddressResponse server_response = response.body();
                            if (server_response.getStatus() == 200) {
                                dialog.dismissLoadingDialog();
                                serviceCarList();


                            } else {
                                dialog.dismissLoadingDialog();
                                serviceCarList();


                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<DeleteAddressResponse> call, Throwable t) {
                        dialog.dismissLoadingDialog();

                    }
                });
            } else {
                Toast toast = Toast.makeText(this, R.string.connectionsss, Toast.LENGTH_LONG);
                toast.setGravity(Gravity.CENTER, 0, 0);
                toast.show();
            }
        }


    }

    private void serviceCarList() {
        {
            if (new InternetCheck(this).isConnect()) {
                //aray1@gmail.com,arya8055
                String key = SharedPreferenceWriter.getInstance(MyAddressActivity.this).getString(SPreferenceKey.jwtToken);
                String id = SharedPreferenceWriter.getInstance(MyAddressActivity.this).getString(SPreferenceKey._id);
                String token="Token"+" "+key;
                DialogPopup dialog = new DialogPopup(this);
                dialog.showLoadingDialog(this, "");

                ApiInterface api_service = RetrofitInit.getConnect().createConnection();
                Call<AddressListResponse> call = api_service.getAddressList(token);
                call.enqueue(new Callback<AddressListResponse>() {
                    @Override
                    public void onResponse(Call<AddressListResponse> call, Response<AddressListResponse> response) {
                        if (response.isSuccessful()) {
                            AddressListResponse server_response = response.body();
                            if (server_response.getStatus() == 200) {
                                dialog.dismissLoadingDialog();
                                ArrayList<AddressListResponse.DataBean> carLists_response = (ArrayList<AddressListResponse.DataBean>) server_response.getData();
                                if (carLists_response.size() > 0) {
                                    recycle_address.setVisibility(View.VISIBLE);
                                    car_list.clear();
                                    car_list.addAll(carLists_response);
                                    updateAdapter();
                                } else {
                                    updateAdapter();
                                    recycle_address.setVisibility(View.GONE);


                                }
                            }

                            else if(server_response.getMessage().equals("Address fetched successfully"))
                            {
                                dialog.dismissLoadingDialog();
                                car_list.clear();
                                updateAdapter();
                                recycle_address.setVisibility(View.GONE);



                            }
                            else {
                                dialog.dismissLoadingDialog();
                                car_list.clear();

                                updateAdapter();


                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<AddressListResponse> call, Throwable t) {
                        dialog.dismissLoadingDialog();

                    }
                });
            } else {
                Toast toast = Toast.makeText(this, R.string.connectionsss, Toast.LENGTH_LONG);
                toast.setGravity(Gravity.CENTER, 0, 0);
                toast.show();
            }
        }


    }



    @Override
    public void onItemClickListener(String type,String id) {
        if(!id.isEmpty())
        {
            if(type.equals("del"))
            {
                ids=id;
                serviceRemoveCar(ids);
            }
            else
            {
                Intent i = new Intent(MyAddressActivity.this, EditAddressActivity.class);
                i.putExtra("id", String.valueOf(id));
                i.putExtra("promos", prmo);
                i.putStringArrayListExtra("test", imagePathList);

                MyAddressActivity.this.startActivity(i);
            }

        }


    }
}

