package com.snapic.outeractivity;

import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.snapic.R;
import com.snapic.adapter.FaqAdapter;
import com.snapic.response.FaqResponse;
import com.snapic.retrofit.ApiInterface;
import com.snapic.retrofit.RetrofitInit;
import com.snapic.utilities.InternetCheck;
import com.snapic.utilities.SPreferenceKey;
import com.snapic.utilities.SharedPreferenceWriter;

import java.util.ArrayList;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FaqActivity extends AppCompatActivity {
    @BindView(R.id.img_back)
    ImageView img_back;
    @BindView(R.id.recycle)
    RecyclerView recycle;
    FaqAdapter faqAdapter;
    private ArrayList<FaqResponse.DataBean> faq_list = new ArrayList<>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_faq);
        ButterKnife.bind(this);
        img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        serviceFaqList();

    }
    void updateAdapter()
    {

        faqAdapter = new FaqAdapter(FaqActivity.this,faq_list);
        recycle.setLayoutManager(new LinearLayoutManager(FaqActivity.this, LinearLayoutManager.VERTICAL, false));
        recycle.setHasFixedSize(true);

        recycle.setAdapter(faqAdapter);


    }
    private void serviceFaqList() {
        {
            String keys= SharedPreferenceWriter.getInstance(FaqActivity.this).getString(SPreferenceKey.jwtToken);


            {
                if(new InternetCheck(FaqActivity.this).isConnect())
                {
                    //aray1@gmail.com,arya8055
                    String key = SharedPreferenceWriter.getInstance(FaqActivity.this).getString(SPreferenceKey.jwtToken);
                    String token="Token"+" "+key;


                    ApiInterface api_service= RetrofitInit.getConnect().createConnection();
                    Call<FaqResponse> call=api_service.faqList(token);
                    call.enqueue(new Callback<FaqResponse>() {
                        @Override
                        public void onResponse(Call<FaqResponse> call, Response<FaqResponse> response) {
                            if(response.isSuccessful())
                            {
                                FaqResponse server_response= response.body();
                                if(server_response.getStatus()==200)
                                {
                                    ArrayList<FaqResponse.DataBean> usersLists_response = (ArrayList<FaqResponse.DataBean>) server_response.getData();
                                    if (usersLists_response.size() > 0) {

                                        recycle.setVisibility(View.VISIBLE);
                                        faq_list.clear();
                                        faq_list.addAll(usersLists_response);
                                        updateAdapter();
                                    } else {
                                        recycle.setVisibility(View.GONE);
                                    }
                                }
                                else  if(server_response.getStatus()==500)
                                {

                                    //  startActivity(new Intent(NotifcationActivity.this,LoginActivity.class));
                                }
                                else if (server_response.getStatus()==400)
                                {

                                }
                                else
                                {

                                }
                            }
                        }
                        @Override
                        public void onFailure(Call<FaqResponse> call, Throwable t) {

                        }
                    });
                }
                else
                {
                    Toast toast=Toast.makeText(FaqActivity.this,R.string.in,Toast.LENGTH_LONG);
                    toast.setGravity(Gravity.CENTER,0,0);
                    toast.show();
                }
            }}



    }

}

