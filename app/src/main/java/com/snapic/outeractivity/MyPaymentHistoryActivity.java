package com.snapic.outeractivity;

import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.snapic.R;
import com.snapic.adapter.PaymentHistoryAdapter;
import com.snapic.response.PaymentListResponse;
import com.snapic.retrofit.ApiInterface;
import com.snapic.retrofit.RetrofitInit;
import com.snapic.utilities.DialogPopup;
import com.snapic.utilities.InternetCheck;
import com.snapic.utilities.SPreferenceKey;
import com.snapic.utilities.SharedPreferenceWriter;

import java.util.ArrayList;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MyPaymentHistoryActivity extends AppCompatActivity {
    @BindView(R.id.img_back)
    ImageView img_back;
//    @BindView(R.id.txt_login)
//    TextView txt_login;
//    @BindView(R.id.tv_name)
//    TextView tv_name;
//    @BindView(R.id.tv_add)
//    TextView tv_add;
//    @BindView(R.id.tv_price)
//    TextView tv_price;
//    @BindView(R.id.tv_charge)
//    TextView tv_charge;
//    @BindView(R.id.tv_dis)
//    TextView tv_dis;
//    @BindView(R.id.tv_full)
//    TextView tv_full;
//    @BindView(R.id.tv_num)
//    TextView tv_num;
    @BindView(R.id.recycle)
    RecyclerView recycle;
    private ArrayList<PaymentListResponse.DataBean> list = new ArrayList<>();

    String id="";
    String totalvalue="";
    String extracharg="";
    String amount="";
    String promos="";
    String discount="";
    PaymentHistoryAdapter paymentHistoryAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_history_my_payments);
        ButterKnife.bind(this);



//        tv_full.setText(totalvalue+ "IO D");
//        tv_price.setText(amount+ "IO D");
//        tv_charge.setText(extracharg+ "IO D");
//        tv_dis.setText(discount+ "IO D");
        img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
//        txt_login.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                startActivity(new Intent(MyPaymentHistoryActivity.this, HomeActivity.class));
//            }
//        });
        servicePaymentList();

    }
    private void servicePaymentList() {
        {
            if (new InternetCheck(this).isConnect()) {
                //aray1@gmail.com,arya8055
                String key = SharedPreferenceWriter.getInstance(MyPaymentHistoryActivity.this).getString(SPreferenceKey.jwtToken);
                String token="Token"+" "+key;
                DialogPopup dialog = new DialogPopup(this);
                dialog.showLoadingDialog(this, "");

                ApiInterface api_service = RetrofitInit.getConnect().createConnection();
                Call<PaymentListResponse> call = api_service.getPaymentList(token);
                call.enqueue(new Callback<PaymentListResponse>() {
                    @Override
                    public void onResponse(Call<PaymentListResponse> call, Response<PaymentListResponse> response) {
                        if (response.isSuccessful()) {
                            PaymentListResponse server_response = response.body();
                            if (server_response.getStatus() == 200) {
                                dialog.dismissLoadingDialog();
                                ArrayList<PaymentListResponse.DataBean> carLists_response = (ArrayList<PaymentListResponse.DataBean>) server_response.getData();
                                if (carLists_response.size() > 0) {

                                    recycle.setVisibility(View.VISIBLE);
                                    list.clear();
                                    list.addAll(carLists_response);
                                    updateAdapter();
                                } else {
                                    recycle.setVisibility(View.GONE);
                                }
                            } else if (server_response.getStatus() == 500) {
                                dialog.dismissLoadingDialog();

                                //   startActivity(new Intent(CustomerStoryActivity.this, LoginActivity.class));
                            } else {
                                dialog.dismissLoadingDialog();

                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<PaymentListResponse> call, Throwable t) {
                        dialog.dismissLoadingDialog();

                    }
                });
            } else {
                Toast toast = Toast.makeText(this, R.string.connectionsss, Toast.LENGTH_LONG);
                toast.setGravity(Gravity.CENTER, 0, 0);
                toast.show();
            }
        }


    }

    void updateAdapter() {
        {
            if ( paymentHistoryAdapter!= null) {
                paymentHistoryAdapter.notifyDataSetChanged();
            } else {
                paymentHistoryAdapter = new PaymentHistoryAdapter(this, list);
                recycle.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
                recycle.setHasFixedSize(true);

                recycle.setAdapter(paymentHistoryAdapter);

            }

        }
    }


}

