package com.snapic.outeractivity;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.snapic.R;
import com.snapic.response.OrderDetailResponse;
import com.snapic.retrofit.ApiInterface;
import com.snapic.retrofit.RetrofitInit;
import com.snapic.utilities.DialogPopup;
import com.snapic.utilities.InternetCheck;
import com.snapic.utilities.SPreferenceKey;
import com.snapic.utilities.SharedPreferenceWriter;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;
import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class OrderDetailSucessActvity extends AppCompatActivity {

    @BindView(R.id.img_back)
    ImageView img_back;
    @BindView(R.id.con_rate)
    ConstraintLayout con_rate;
    @BindView(R.id.tv_date)
    TextView tv_date;
    @BindView(R.id.tv_time)
    TextView tv_time;
    @BindView(R.id.img_pic)
    ImageView img_pic;
    @BindView(R.id.tv_status)
    TextView tv_status;
    @BindView(R.id.tv_dates)
    TextView tv_dates;
    @BindView(R.id.tv_total)
    TextView tv_total;
    @BindView(R.id.tv_e_charge)
    TextView tv_e_charge;
    @BindView(R.id.tv_dis)
    TextView tv_dis;
    @BindView(R.id.tv_am)
    TextView tv_am;
    @BindView(R.id.tv_mode)
    TextView tv_mode;
    @BindView(R.id.tv_discou)
    TextView tv_discou;
    String id="";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_detail_sucess);
        ButterKnife.bind(this);
        Intent i=getIntent();
        id =i.getStringExtra("id");
        img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        con_rate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i=new Intent(OrderDetailSucessActvity.this, RatingActivity.class);
                i.putExtra("id",String.valueOf(id));
                view.getContext().startActivity(i);             }
        });

    }
    @Override
    protected void onResume() {
        super.onResume();
        if(!id.isEmpty())
        {
            addressDetail(id);
        }
    }
    private void addressDetail(String id) {
        {
            String key = SharedPreferenceWriter.getInstance(OrderDetailSucessActvity.this).getString(SPreferenceKey.jwtToken);
            String token="Token"+" "+key;

            if (new InternetCheck(OrderDetailSucessActvity.this).isConnect()) {

                //aray1@gmail.com,arya8055
                DialogPopup dialog = new DialogPopup(OrderDetailSucessActvity.this);
                dialog.showLoadingDialog(OrderDetailSucessActvity.this, "");
                System.out.print(key);
                System.out.print(id);

                ApiInterface api_service = RetrofitInit.getConnect().createConnection();
                Call<OrderDetailResponse> call = api_service.getPastOrderDetails(token,id);
                call.enqueue(new Callback<OrderDetailResponse>() {
                    @Override
                    public void onResponse(Call<OrderDetailResponse> call, Response<OrderDetailResponse> response) {
                        if (response.isSuccessful()) {
                            OrderDetailResponse server_response = response.body();
                            if (server_response.getStatus() == 200) {
                                dialog.dismissLoadingDialog();
                                String abc;
                                tv_am.setText(String.valueOf(server_response.getData().getAmount()+server_response.getData().getExtra_charges()-server_response.getData().getDiscount())+ " IQ D");

                               if(server_response.getData().getDiscount()>0)
                               {
                                   tv_discou.setVisibility(View.VISIBLE);
                                   tv_dis.setText(String.valueOf(server_response.getData().getDiscount())+ " IQ D");

                               }
                               else
                               {
                                   tv_discou.setVisibility(View.GONE);

                               }
                                tv_mode.setText(server_response.getData().get_$ModeOfPayment8());
                                tv_total.setText(String.valueOf(server_response.getData().getAmount())+ " IQ D");


                                tv_e_charge.setText(String.valueOf(server_response.getData().getExtra_charges())+ " IQ D");
                                Glide.with(OrderDetailSucessActvity.this).load(server_response.getData().getImage()).into(img_pic);

                                if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                                    SimpleDateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
                                    SimpleDateFormat outputFormat = new SimpleDateFormat("HH:mm");
                                    ;
                                    SimpleDateFormat yeardata = new SimpleDateFormat("yyyy");
                                    SimpleDateFormat monthdata = new SimpleDateFormat("MM");
                                    SimpleDateFormat datedata = new SimpleDateFormat("dd");
                                    SimpleDateFormat fromat = new SimpleDateFormat("yyyy-MM-dd");






                                    Date dateEndd = null;
                                    try {
                                        dateEndd = inputFormat.parse((server_response.getData().getCreated_at()));
                                        String updateTime = outputFormat.format(dateEndd);
                                        String yeardatastr = yeardata.format(dateEndd);
                                        String monthdatastr = monthdata.format(dateEndd);
                                        String datedatastr = datedata.format(dateEndd);
                                        String date = fromat.format(dateEndd);
                                        tv_date.setText(date);
                                        tv_time.setText(" " +updateTime);
                                        if(date.equals(server_response.getData().getCustom_delivery_date()))
                                        {
                                            tv_dates.setText("On "+ server_response.getData().getDelivery_date());



                                        }
                                        else
                                        {
                                            tv_dates.setText("On "+ server_response.getData().getCustom_delivery_date());



                                        }

                                    } catch (ParseException e) {
                                        e.printStackTrace();
                                    }
                                }


                                //  setPreferences(server_response);


                            } else if (server_response.getStatus() == 400) {
                                dialog.dismissLoadingDialog();

                            } else if (server_response.getStatus() == 500) {
                                dialog.dismissLoadingDialog();

                                //   startActivity(new Intent(DonationDetailsActivity.this, LoginActivity.class));
                            } else {
                                dialog.dismissLoadingDialog();

                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<OrderDetailResponse> call, Throwable t) {
                        dialog.dismissLoadingDialog();

                    }
                });
            } else {
                Toast toast = Toast.makeText(OrderDetailSucessActvity.this, R.string.in, Toast.LENGTH_LONG);
                toast.setGravity(Gravity.CENTER, 0, 0);
                toast.show();
            }
        }
    }
}

