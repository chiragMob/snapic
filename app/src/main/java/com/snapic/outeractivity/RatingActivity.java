package com.snapic.outeractivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.InputType;
import android.view.Gravity;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.snapic.R;
import com.snapic.activity.HomeActivity;
import com.snapic.response.FeedbackResponse;
import com.snapic.response.RatingResponse;
import com.snapic.retrofit.ApiInterface;
import com.snapic.retrofit.RetrofitInit;
import com.snapic.utilities.DialogPopup;
import com.snapic.utilities.InternetCheck;
import com.snapic.utilities.SPreferenceKey;
import com.snapic.utilities.SharedPreferenceWriter;

import androidx.appcompat.app.AppCompatActivity;
import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class RatingActivity extends AppCompatActivity {

    @BindView(R.id.img_back)
    ImageView img_back;
    @BindView(R.id.txt_login)
    TextView txt_login;
    String ratting="",id="";
    @BindView(R.id.ratingBar)
    RatingBar ratingBar;
    @BindView(R.id.editText)
    EditText editText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rate_us);
        ButterKnife.bind(this);
        Intent i=getIntent();
        id =i.getStringExtra("id");
        img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        serviceFeedbacks();

        txt_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                Intent i = new Intent(RatingActivity.this, HomeActivity.class);
//
//                RatingActivity.this.startActivity(i);
                int noofstars = ratingBar.getNumStars();
                float getrating = ratingBar.getRating();
                ratting= String.valueOf(ratingBar.getRating());
                serviceFeedback();
            }
        });





    }
    private void serviceFeedbacks() {
        {



            {
                String key = SharedPreferenceWriter.getInstance(RatingActivity.this).getString(SPreferenceKey.jwtToken);
                String token="Token"+" "+key;
                String deviceToken = SharedPreferenceWriter.getInstance(this).getString(SPreferenceKey.DEVICETOKEN);
                String ids = SharedPreferenceWriter.getInstance(this).getString(SPreferenceKey._id);



                if (new InternetCheck(this).isConnect()) {
                    //aray1@gmail.com,arya8055
                    DialogPopup dialog = new DialogPopup(this);
                    dialog.showLoadingDialog(this, "");

                    ApiInterface api_service = RetrofitInit.getConnect().createConnection();
                    Call<RatingResponse> call = api_service.rating(token,id);
                    call.enqueue(new Callback<RatingResponse>() {
                        @Override
                        public void onResponse(Call<RatingResponse> call, Response<RatingResponse> response) {
                            if (response.isSuccessful()) {
                                RatingResponse server_response = response.body();
                                if (server_response.getStatus() == 200) {
                                    dialog.dismissLoadingDialog();
                                    txt_login.setVisibility(View.GONE);
                                    editText.setText(server_response.getContent());
                                    editText.setInputType(InputType.TYPE_NULL);
                                    ratingBar.setIsIndicator(true);
                                    ratingBar.setFocusable(false);
                                    ratingBar.setRating((Float.parseFloat(String.valueOf(server_response.getStars()))));

                                }  else {
                                    dialog.dismissLoadingDialog();

                                }
                            }
                        }

                        @Override
                        public void onFailure(Call<RatingResponse> call, Throwable t) {
                            dialog.dismissLoadingDialog();

                        }
                    });
                } else {
                    Toast toast = Toast.makeText(this, R.string.in, Toast.LENGTH_LONG);
                    toast.setGravity(Gravity.CENTER, 0, 0);
                    toast.show();
                }
            }

        }


    }

    private void serviceFeedback() {
        {
            
            if(ratting.isEmpty())
            {
                Toast.makeText(this, R.string.feed, Toast.LENGTH_SHORT).show();
            }
            else  if(editText.getText().toString().trim().isEmpty())
            {
                Toast.makeText(this, R.string.feeds, Toast.LENGTH_SHORT).show();
            }
            else
            {
                String key = SharedPreferenceWriter.getInstance(RatingActivity.this).getString(SPreferenceKey.jwtToken);
                String token="Token"+" "+key;
                String deviceToken = SharedPreferenceWriter.getInstance(this).getString(SPreferenceKey.DEVICETOKEN);
                String ids = SharedPreferenceWriter.getInstance(this).getString(SPreferenceKey._id);



                if (new InternetCheck(this).isConnect()) {
                    //aray1@gmail.com,arya8055
                    DialogPopup dialog = new DialogPopup(this);
                    dialog.showLoadingDialog(this, "");

                    ApiInterface api_service = RetrofitInit.getConnect().createConnection();
                    Call<FeedbackResponse> call = api_service.feedback(token,id,editText.getText().toString().trim(),ratting);
                    call.enqueue(new Callback<FeedbackResponse>() {
                        @Override
                        public void onResponse(Call<FeedbackResponse> call, Response<FeedbackResponse> response) {
                            if (response.isSuccessful()) {
                                FeedbackResponse server_response = response.body();
                                if (server_response.getStatus() == 200) {
                                    dialog.dismissLoadingDialog();
                                    startActivity(new Intent(RatingActivity.this,HomeActivity.class));

                                }  else {
                                    dialog.dismissLoadingDialog();

                                }
                            }
                        }

                        @Override
                        public void onFailure(Call<FeedbackResponse> call, Throwable t) {
                            dialog.dismissLoadingDialog();

                        }
                    });
                } else {
                    Toast toast = Toast.makeText(this, R.string.in, Toast.LENGTH_LONG);
                    toast.setGravity(Gravity.CENTER, 0, 0);
                    toast.show();
                }
            }

        }


    }

}
