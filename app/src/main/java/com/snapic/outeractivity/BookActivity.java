package com.snapic.outeractivity;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.snapic.R;
import com.snapic.activity.CreateAlbumActivity;
import com.snapic.response.AddressDetailResponse;
import com.snapic.response.AddressListResponse;
import com.snapic.response.CostAlbumResponse;
import com.snapic.response.PromoCodeResponse;
import com.snapic.response.PromoResponse;
import com.snapic.response.SizeResonse;
import com.snapic.retrofit.ApiInterface;
import com.snapic.retrofit.RetrofitInit;
import com.snapic.utilities.DialogPopup;
import com.snapic.utilities.InternetCheck;
import com.snapic.utilities.SPreferenceKey;
import com.snapic.utilities.SharedPreferenceWriter;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;
import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class BookActivity extends AppCompatActivity {

 @BindView(R.id.txt_login)
TextView txt_login;
 @BindView(R.id.img_back)
 ImageView img_back;
 @BindView(R.id.change)
 TextView change;
 @BindView(R.id.promo)
 ConstraintLayout promo;
 @BindView(R.id.tv_name)
 TextView tv_name;
 @BindView(R.id.tv_add)
 TextView tv_add;
 @BindView(R.id.tv_type)
 TextView tv_type;
 @BindView(R.id.tv_num)
 TextView tv_num;
 String id="";
 @BindView(R.id.tv_size)
 TextView tv_size;
 @BindView(R.id.tv_total)
 TextView tv_total;
 @BindView(R.id.tv_charge)
 TextView tv_charge;
 @BindView(R.id.pro)
 TextView pro;
 @BindView(R.id.dis)
 TextView dis;
 @BindView(R.id.totall)
 TextView totall;
 int price=0;
 int charge=0;
 String totalvalue="";
 String extracharg="";
 String amount="";
 String promos="";
 String discount="";
 @BindView(R.id.img_src)
 ImageView img_src;
 @BindView(R.id.text)
 TextView text;
 @BindView(R.id.tv_time)
 TextView tv_time;
 @BindView(R.id.tv_date)
 TextView tv_date;
 @BindView(R.id.tv_dis)
 TextView tv_dis;
 @BindView(R.id.tv_app)
 TextView tv_app;
 @BindView(R.id.spin)
 Spinner spin;

 @BindView(R.id.tv_h)
 TextView tv_h;
    ArrayList<String>  imagePathList=new ArrayList<>();
    String promoss="";
    String sizes="";
    String sizeone="";
    String[] qty = {"Select", "5*7", "4*6"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Intent i = getIntent();
        sizes=i.getStringExtra("size");

        if(getIntent().getStringArrayListExtra("test")!=null) {

            imagePathList = getIntent().getStringArrayListExtra("test");
        }
        id = i.getStringExtra("id");
        promoss=i.getStringExtra("promos");

        if(id.isEmpty())
        {
            serviceCarList();

        }
        else if(!id.isEmpty())
        {
            addressDetail(id);
        }


        sizeone = SharedPreferenceWriter.getInstance(BookActivity.this).getString(SPreferenceKey.val);
       String numberofphotos = SharedPreferenceWriter.getInstance(BookActivity.this).getString(SPreferenceKey.size);


        setContentView(R.layout.activty_book);
        ButterKnife.bind(this);
        tv_app.setVisibility(View.GONE);
        tv_dis.setVisibility(View.GONE);


        String date = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault()).format(new Date());
        String time = new SimpleDateFormat("HH:mm", Locale.getDefault()).format(new Date());

        tv_date.setText(date);
        tv_time.setText( " "+time);
        change.setText(R.string.selects);

            tv_size.setText(sizeone);
        tv_h.setText(numberofphotos);

        txt_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(tv_size.getText().toString().equals("Select"))
                {
                    Toast.makeText(BookActivity.this, "Please Select Size Of Photos", Toast.LENGTH_SHORT).show();

                }
                else if(id.isEmpty())
                {
                    Toast.makeText(BookActivity.this, R.string.adds, Toast.LENGTH_SHORT).show();
                }
                else
                {
                    Intent i = new Intent(BookActivity.this, MyPaymentsActivity.class);
                    i.putExtra("id", id);
                    i.putExtra("amount",amount);
                    i.putExtra("charge",extracharg);
                    i.putExtra("promos",promos);
                    i.putExtra("discount",discount);
                    i.putExtra("totalvalue",totalvalue);
                    i.putExtra("size",tv_size.getText().toString().trim());

                    i.putStringArrayListExtra("test", imagePathList);



                    BookActivity.this.startActivity(i);
                }

            }
        });
        img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        change.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(BookActivity.this, MyAddressActivity.class);
                i.putExtra("promos", promos);
                i.putStringArrayListExtra("test", imagePathList);
                i.putExtra("size",tv_size.getText().toString().trim());




                BookActivity.this.startActivity(i);

            }
        });
        promo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showDialogTPassword();
            }
        });
        sizeResponse();
        costResponse();
        servicePromoCode();
        spinnerQty(spin);
    }

    @Override
    protected void onResume() {
        super.onResume();
        Intent i = getIntent();
        id = i.getStringExtra("id");
        promoss=i.getStringExtra("promos");
        String pic = SharedPreferenceWriter.getInstance(BookActivity.this).getString(SPreferenceKey.pic);

        Glide.with(BookActivity.this).load(pic).into(img_src);
        if(!id.isEmpty()&&!promoss.isEmpty())
        {
            addressDetail(id);
            serviceApplyPromoCode(promoss);
        }
        else if(!id.isEmpty())
        {
            addressDetail(id);

        }
        else if(!promoss.isEmpty())
        {
            serviceApplyPromoCode(promoss);

        }


    }

    @Override
    public void onBackPressed() {
        startActivity(new Intent(BookActivity.this,CreateAlbumActivity.class));
    }


    private void spinnerQty(Spinner spinner) {
        ArrayAdapter<String> spinnerAdapter = new ArrayAdapter<String>(BookActivity.this, android.R.layout.simple_spinner_dropdown_item, qty) {
            @Override
            public boolean isEnabled(int position) {
                if (position == 0) {
                    return false;
                } else {
                    return true;
                }
            }

            @Override
            public View getDropDownView(int position, View convertView, ViewGroup parent) {
                View view = super.getDropDownView(position, convertView, parent);
                TextView tv = (TextView) view;

                return view;
            }
        };

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (i == 0)
                    return;
                tv_size.setText(spinner.getSelectedItem().toString());




            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        spinner.setAdapter(spinnerAdapter);
    }


    private void addressDetail(String id) {
        {
            String key = SharedPreferenceWriter.getInstance(BookActivity.this).getString(SPreferenceKey.jwtToken);
            String token="Token"+" "+key;

            if (new InternetCheck(BookActivity.this).isConnect()) {

                //aray1@gmail.com,arya8055
                DialogPopup dialog = new DialogPopup(BookActivity.this);
                dialog.showLoadingDialog(BookActivity.this, "");
                System.out.print(key);
                System.out.print(id);

                ApiInterface api_service = RetrofitInit.getConnect().createConnection();
                Call<AddressDetailResponse> call = api_service.getProductDetails(token,id);
                call.enqueue(new Callback<AddressDetailResponse>() {
                    @Override
                    public void onResponse(Call<AddressDetailResponse> call, Response<AddressDetailResponse> response) {
                        if (response.isSuccessful()) {
                            AddressDetailResponse server_response = response.body();
                            if (server_response.getStatus() == 200) {
                                dialog.dismissLoadingDialog();
                                String abc;
                                change.setText(R.string.change);

                                if(!server_response.getDetail().getStreet().isEmpty()&&!server_response.getDetail().getCity().isEmpty()&&!server_response.getDetail().getApartment().isEmpty()&&!server_response.getDetail().getBuilding().isEmpty())
                                {
                                    tv_add.setText(server_response.getDetail().getStreet()+","+server_response.getDetail().getBuilding()+","+server_response.getDetail().getApartment()+","+server_response.getDetail().getCity());
                                    tv_num.setText(server_response.getDetail().getCountry_code()+" "+server_response.getDetail().getPhone_number());
                                    tv_name.setText(server_response.getDetail().getName());
                                    tv_type.setText(server_response.getDetail().getTag());
                                    text.setText(server_response.getDetail().getNearest_point());

                                }
                                else
                                {
                                    tv_add.setText(server_response.getDetail().getStreet()+" "+server_response.getDetail().getBuilding()+" "+" "+server_response.getDetail().getApartment()+" "+server_response.getDetail().getCity());
                                    tv_num.setText(server_response.getDetail().getCountry_code()+" "+server_response.getDetail().getPhone_number());
                                    tv_name.setText(server_response.getDetail().getName());
                                    text.setText(server_response.getDetail().getNearest_point());




                                }



                                //  setPreferences(server_response);


                            } else if (server_response.getStatus() == 400) {
                                dialog.dismissLoadingDialog();

                            } else if (server_response.getStatus() == 500) {
                                dialog.dismissLoadingDialog();

                                //   startActivity(new Intent(DonationDetailsActivity.this, LoginActivity.class));
                            } else {
                                dialog.dismissLoadingDialog();

                                Toast.makeText(BookActivity.this, server_response.getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<AddressDetailResponse> call, Throwable t) {
                        dialog.dismissLoadingDialog();

                    }
                });
            } else {
                Toast toast = Toast.makeText(BookActivity.this, R.string.in, Toast.LENGTH_LONG);
                toast.setGravity(Gravity.CENTER, 0, 0);
                toast.show();
            }
        }
    }

    private void sizeResponse() {
        {
            String key = SharedPreferenceWriter.getInstance(BookActivity.this).getString(SPreferenceKey.jwtToken);
            String token="Token"+" "+key;

            if (new InternetCheck(BookActivity.this).isConnect()) {

                //aray1@gmail.com,arya8055
                DialogPopup dialog = new DialogPopup(BookActivity.this);
                dialog.showLoadingDialog(BookActivity.this, "");
                System.out.print(key);
                System.out.print(id);

                ApiInterface api_service = RetrofitInit.getConnect().createConnection();
                Call<SizeResonse> call = api_service.getSize(token);
                call.enqueue(new Callback<SizeResonse>() {
                    @Override
                    public void onResponse(Call<SizeResonse> call, Response<SizeResonse> response) {
                        if (response.isSuccessful()) {
                            SizeResonse server_response = response.body();
                            assert server_response != null;
                            if (server_response.getStatus() == 200) {
                                dialog.dismissLoadingDialog();
                                String abc;




                                //  setPreferences(server_response);


                            } else if (server_response.getStatus() == 400) {
                                dialog.dismissLoadingDialog();

                            } else if (server_response.getStatus() == 500) {
                                dialog.dismissLoadingDialog();

                                //   startActivity(new Intent(DonationDetailsActivity.this, LoginActivity.class));
                            } else {
                                dialog.dismissLoadingDialog();

                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<SizeResonse> call, Throwable t) {
                        dialog.dismissLoadingDialog();

                    }
                });
            } else {
                Toast toast = Toast.makeText(BookActivity.this, R.string.in, Toast.LENGTH_LONG);
                toast.setGravity(Gravity.CENTER, 0, 0);
                toast.show();
            }
        }
    }
    private void costResponse() {
        {
            String key = SharedPreferenceWriter.getInstance(BookActivity.this).getString(SPreferenceKey.jwtToken);
            String token="Token"+" "+key;

            if (new InternetCheck(BookActivity.this).isConnect()) {

                //aray1@gmail.com,arya8055
                DialogPopup dialog = new DialogPopup(BookActivity.this);
                dialog.showLoadingDialog(BookActivity.this, "");
                System.out.print(key);
                System.out.print(id);

                ApiInterface api_service = RetrofitInit.getConnect().createConnection();
                Call<CostAlbumResponse> call = api_service.costAlbum(token);
                call.enqueue(new Callback<CostAlbumResponse>() {
                    @Override
                    public void onResponse(Call<CostAlbumResponse> call, Response<CostAlbumResponse> response) {
                        if (response.isSuccessful()) {
                            CostAlbumResponse server_response = response.body();
                            assert server_response != null;
                            if (server_response.getStatus() == 200) {
                                dialog.dismissLoadingDialog();
                                String abc;
                                price=server_response.getPrice();
                                charge=server_response.get_$ExtraCharge24();
                                tv_total.setText(String.valueOf(server_response.getPrice())+" "+"IQ D");
                                tv_charge.setText(String.valueOf(server_response.get_$ExtraCharge24())+" "+"IQ D");
                                totall.setText(price+charge+" "+"IQ D");
                                totalvalue=String.valueOf(price+charge);

                                 extracharg=String.valueOf(server_response.get_$ExtraCharge24());
                                 amount=String.valueOf(server_response.getPrice());




                                //  setPreferences(server_response);


                            } else if (server_response.getStatus() == 400) {
                                dialog.dismissLoadingDialog();

                            } else if (server_response.getStatus() == 500) {
                                dialog.dismissLoadingDialog();

                                //   startActivity(new Intent(DonationDetailsActivity.this, LoginActivity.class));
                            } else {
                                dialog.dismissLoadingDialog();

                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<CostAlbumResponse> call, Throwable t) {
                        dialog.dismissLoadingDialog();

                    }
                });
            } else {
                Toast toast = Toast.makeText(BookActivity.this, R.string.in, Toast.LENGTH_LONG);
                toast.setGravity(Gravity.CENTER, 0, 0);
                toast.show();
            }
        }
    }

    private void serviceCarList() {
        {
            if (new InternetCheck(this).isConnect()) {
                //aray1@gmail.com,arya8055
                String key = SharedPreferenceWriter.getInstance(BookActivity.this).getString(SPreferenceKey.jwtToken);
                String token="Token"+" "+key;
                DialogPopup dialog = new DialogPopup(this);
                dialog.showLoadingDialog(this, "");

                ApiInterface api_service = RetrofitInit.getConnect().createConnection();
                Call<AddressListResponse> call = api_service.getAddressList(token);
                call.enqueue(new Callback<AddressListResponse>() {
                    @Override
                    public void onResponse(Call<AddressListResponse> call, Response<AddressListResponse> response) {
                        if (response.isSuccessful()) {
                            AddressListResponse server_response = response.body();
                            if (server_response.getStatus() == 200) {
                                dialog.dismissLoadingDialog();
                                ArrayList<AddressListResponse.DataBean> carLists_response = (ArrayList<AddressListResponse.DataBean>) server_response.getData();
                                if (carLists_response.size() > 0) {
                                    for(int i=0;i<carLists_response.size();i++)
                                    {
                                        change.setText(R.string.change);

                                        if(carLists_response.get(i).isDefault_address())
                                        {
                                            id=String.valueOf(carLists_response.get(i).getId());
                                            if(!carLists_response.get(i).getStreet().isEmpty()&&!carLists_response.get(i).getCity().isEmpty()&&!carLists_response.get(i).getApartment().isEmpty()&&!carLists_response.get(i).getBuilding().isEmpty())
                                            {
                                               tv_add.setText(carLists_response.get(i).getStreet()+","+carLists_response.get(i).getBuilding()+","+carLists_response.get(i).getApartment()+","+carLists_response.get(i).getCity());
                                           tv_num.setText(carLists_response.get(i).getCountry_code()+" "+carLists_response.get(i).getPhone_number());
                                           tv_name.setText(carLists_response.get(i).getName());
                                                tv_type.setText(carLists_response.get(i).getTag());
                                                text.setText(server_response.getData().get(i).getNearest_point());
                                            }
                                            else
                                            {
                                            tv_add.setText(carLists_response.get(i).getStreet()+" "+carLists_response.get(i).getBuilding()+" "+" "+carLists_response.get(i).getApartment()+" "+carLists_response.get(i).getCity());
                                                tv_num.setText(carLists_response.get(i).getCountry_code()+" "+carLists_response.get(i).getPhone_number());
                                                tv_name.setText(carLists_response.get(i).getName());
                                                text.setText(server_response.getData().get(i).getNearest_point());




                                            }
                                        }
                                    }

                                } else {
                                    change.setText(R.string.selects);
                                }
                            } else if (server_response.getStatus() == 500) {
                                dialog.dismissLoadingDialog();

                                //   startActivity(new Intent(CustomerStoryActivity.this, LoginActivity.class));
                            } else {
                                dialog.dismissLoadingDialog();

                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<AddressListResponse> call, Throwable t) {
                        dialog.dismissLoadingDialog();

                    }
                });
            } else {
                Toast toast = Toast.makeText(this, R.string.in, Toast.LENGTH_LONG);
                toast.setGravity(Gravity.CENTER, 0, 0);
                toast.show();
            }
        }


    }

    private void showDialogTPassword() {

        ConstraintLayout constraintLayout;
        TextView txt_login;
        EditText et_prom;
        final Dialog dialogTyreDetail = new Dialog(BookActivity.this, android.R.style.Theme_Black_NoTitleBar);
        dialogTyreDetail.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogTyreDetail.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialogTyreDetail.setContentView(R.layout.dialog_promo_code);
        dialogTyreDetail.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        dialogTyreDetail.setCanceledOnTouchOutside(true);
        txt_login=dialogTyreDetail.findViewById(R.id.txt_login);
        et_prom=dialogTyreDetail.findViewById(R.id.et_prom);
        String promo=et_prom.getText().toString().trim();
        txt_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                serviceApplyPromoCode(et_prom.getText().toString().trim());

                dialogTyreDetail.dismiss();

            }
        });




        dialogTyreDetail.show();


    }
    private void serviceApplyPromoCode(String promo) {
        {
            String key = SharedPreferenceWriter.getInstance(BookActivity.this).getString(SPreferenceKey.jwtToken);
            String id = SharedPreferenceWriter.getInstance(BookActivity.this).getString(SPreferenceKey._id);
            String token="Token"+" "+key;
            if (new InternetCheck(this).isConnect()) {
                //aray1@gmail.com,arya8055
                DialogPopup dialog = new DialogPopup(this);
                dialog.showLoadingDialog(this, "");

                ApiInterface api_service = RetrofitInit.getConnect().createConnection();
                Call<PromoResponse> call = api_service.applypromoCode(token,promo);
                call.enqueue(new Callback<PromoResponse>() {
                    @Override
                    public void onResponse(Call<PromoResponse> call, Response<PromoResponse> response) {
                        if (response.isSuccessful()) {
                            PromoResponse server_response = response.body();
                            if (server_response.getStatus() == 200) {
                                dialog.dismissLoadingDialog();


                                pro.setText(" " +promo);
                                dis.setText(String.valueOf(server_response.getMessage())+" "+"IQ D");
                                int value = (int)server_response.getMessage();
                                totall.setText(price+charge-value+" "+"IQ D");
                                totalvalue=String.valueOf(price+charge-value);
                                tv_dis.setVisibility(View.VISIBLE);


                                promos=promo;
                                 discount=String.valueOf(value);

                            }  else {
                                dialog.dismissLoadingDialog();
                                tv_dis.setVisibility(View.GONE);

                                Toast.makeText(BookActivity.this, ""+String.valueOf(server_response.getMessage()), Toast.LENGTH_SHORT).show();

                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<PromoResponse> call, Throwable t) {
                        dialog.dismissLoadingDialog();
                        Toast.makeText(BookActivity.this, ""+t.getMessage(), Toast.LENGTH_SHORT).show();


                    }
                });
            } else {
                Toast toast = Toast.makeText(this, "Internet connection lost", Toast.LENGTH_LONG);
                toast.setGravity(Gravity.CENTER, 0, 0);
                toast.show();
            }
        }


    }

    private void servicePromoCode() {
        {
            if (new InternetCheck(BookActivity.this).isConnect()) {
                String key = SharedPreferenceWriter.getInstance(BookActivity.this).getString(SPreferenceKey.jwtToken);
                String id = SharedPreferenceWriter.getInstance(BookActivity.this).getString(SPreferenceKey._id);
                String token="Token"+" "+key;

                //aray1@gmail.com,arya8055
                DialogPopup dialog = new DialogPopup(BookActivity.this);
                dialog.showLoadingDialog(BookActivity.this, "");

                ApiInterface api_service = RetrofitInit.getConnect().createConnection();
                Call<PromoCodeResponse> call = api_service.promoCode(token);
                call.enqueue(new Callback<PromoCodeResponse>() {
                    @Override
                    public void onResponse(Call<PromoCodeResponse> call, Response<PromoCodeResponse> response) {

                        if (response.isSuccessful()) {
                            dialog.dismissLoadingDialog();
                        }           PromoCodeResponse server_response = response.body();

                        if (server_response.getStatus() == 200) {

                            tv_app.setVisibility(View.VISIBLE);


                        } else if (server_response.getStatus() == 400) {
                            tv_app.setVisibility(View.GONE);



                        }
                    }

                    @Override
                    public void onFailure(Call<PromoCodeResponse> call, Throwable t) {
                        dialog.dismissLoadingDialog();

                    }
                });
            } else {
                Toast toast = Toast.makeText(BookActivity.this, "Internet connection lost", Toast.LENGTH_LONG);
                toast.setGravity(Gravity.CENTER, 0, 0);
                toast.show();
            }
        }


    }



}
