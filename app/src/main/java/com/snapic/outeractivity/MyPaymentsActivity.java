package com.snapic.outeractivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.snapic.R;
import com.snapic.activity.HomeActivity;
import com.snapic.response.AddressDetailResponse;
import com.snapic.response.ImageResponse;
import com.snapic.response.MasterCardResponse;
import com.snapic.response.OrderResponse;
import com.snapic.response.PaymentDoneResponse;
import com.snapic.response.PaymentResponse;
import com.snapic.retrofit.ApiInterface;
import com.snapic.retrofit.RetrofitInit;
import com.snapic.utilities.DialogPopup;
import com.snapic.utilities.InternetCheck;
import com.snapic.utilities.SPreferenceKey;
import com.snapic.utilities.SharedPreferenceWriter;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

import androidx.appcompat.app.AppCompatActivity;
import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MyPaymentsActivity extends AppCompatActivity {
    @BindView(R.id.img_back)
    ImageView img_back;
    @BindView(R.id.txt_login)
    TextView txt_login;
    @BindView(R.id.tv_name)
    TextView tv_name;
    @BindView(R.id.tv_add)
    TextView tv_add;
    @BindView(R.id.tv_price)
    TextView tv_price;
    @BindView(R.id.tv_charge)
    TextView tv_charge;
    @BindView(R.id.tv_dis)
    TextView tv_dis;
    @BindView(R.id.tv_full)
    TextView tv_full;
    @BindView(R.id.tv_num)
    TextView tv_num;
    @BindView(R.id.tv_near)
    TextView tv_near;

    String id="";
    String totalvalue="";
    String extracharg="";
    String amount="";
    String promos="";
    String discount="";
    String orderid="";
    String paymentAmount="";
    String size="";
    ArrayList<String>  imagePathList=new ArrayList<>();
    @BindView(R.id.rad1)
    RadioButton rad1;
    @BindView(R.id.rad2)
    RadioButton rad2;
    @BindView(R.id.rad3)
    RadioButton rad3;
    @BindView(R.id.rad6)
    RadioButton rad6;
    @BindView(R.id.tv_avails)
    TextView tv_avails;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payments);
        ButterKnife.bind(this);
        Intent i = getIntent();
        

        id = i.getStringExtra("id");
        size=i.getStringExtra("size");

        totalvalue = i.getStringExtra("totalvalue");
        extracharg = i.getStringExtra("charge");
        amount = i.getStringExtra("amount");
        promos = i.getStringExtra("promos");
        discount = i.getStringExtra("discount");
        if(getIntent().getStringArrayListExtra("test")!=null) {

            imagePathList = getIntent().getStringArrayListExtra("test");
        }

        if(!id.isEmpty())
        {
            addressDetail(id);

        }
        tv_full.setText(totalvalue+ "IQ D");
        tv_price.setText(amount+ "IQ D");
        tv_charge.setText(extracharg+ "IQ D");
        if(discount.isEmpty())
        {
            tv_dis.setVisibility(View.GONE);
            tv_avails.setVisibility(View.GONE);
        }
        else
        {
            tv_dis.setText(discount+ "IQ D");

        }
        img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        txt_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
              //  homeImage();

             //   cardResponse();
                if(!rad2.isChecked()&&!rad3.isChecked()&&!rad6.isChecked())
                {
                    Toast.makeText(MyPaymentsActivity.this, R.string.zain, Toast.LENGTH_SHORT).show();

                }



                else
                {
                    if(!promos.isEmpty())
                    {
                        serviceOrderPrmo();
                    }
                    else
                    {
                        serviceOrder();
                    }
                }



            }
        });


    }

    private void cardResponse() {
        {
            if (new InternetCheck(MyPaymentsActivity.this).isConnect()) {

                //aray1@gmail.com,arya8055
                DialogPopup dialog = new DialogPopup(MyPaymentsActivity.this);
                dialog.showLoadingDialog(MyPaymentsActivity.this, "");


                ApiInterface api_service = RetrofitInit.getConnects1().createConnections1();
                Call<MasterCardResponse> call = api_service.getCardResponse("api_gulfroots","1234@",orderid,totalvalue+"000","368","https://www.google.com","https://www.aps.iq");
                call.enqueue(new Callback<MasterCardResponse>() {
                    @Override
                    public void onResponse(Call<MasterCardResponse> call, Response<MasterCardResponse> response) {
                        if (response.isSuccessful()) {
                            if(response.code()==200)
                            {
                                dialog.dismissLoadingDialog();

                                MasterCardResponse server_response = response.body();
                                if(server_response.getErrorCode()==null)
                                {
                                    Intent i = new Intent(MyPaymentsActivity.this, MasterCardWebViewPayment.class);
                                    i.putExtra("id", server_response.getFormUrl());

                                    i.putExtra("orderid", orderid);
                                    i.putExtra("total", totalvalue+"000");
                                    i.putExtra("paymentid", server_response.getOrderId());


                                    MyPaymentsActivity.this.startActivity(i);
                                }
                                else
                                {
                                    Toast.makeText(MyPaymentsActivity.this, server_response.getErrorMessage(), Toast.LENGTH_SHORT).show();

                                }

                            }


                        }
                    }

                    @Override
                    public void onFailure(Call<MasterCardResponse> call, Throwable t) {
                        dialog.dismissLoadingDialog();

                    }
                });
            } else {
                Toast toast = Toast.makeText(MyPaymentsActivity.this, R.string.in, Toast.LENGTH_LONG);
                toast.setGravity(Gravity.CENTER, 0, 0);
                toast.show();
            }
        }
    }

    private void addressDetail(String id) {
        {
            String key = SharedPreferenceWriter.getInstance(MyPaymentsActivity.this).getString(SPreferenceKey.jwtToken);
            String token="Token"+" "+key;

            if (new InternetCheck(MyPaymentsActivity.this).isConnect()) {

                //aray1@gmail.com,arya8055
                DialogPopup dialog = new DialogPopup(MyPaymentsActivity.this);
                dialog.showLoadingDialog(MyPaymentsActivity.this, "");
                System.out.print(key);
                System.out.print(id);

                ApiInterface api_service = RetrofitInit.getConnect().createConnection();
                Call<AddressDetailResponse> call = api_service.getProductDetails(token,id);
                call.enqueue(new Callback<AddressDetailResponse>() {
                    @Override
                    public void onResponse(Call<AddressDetailResponse> call, Response<AddressDetailResponse> response) {
                        if (response.isSuccessful()) {
                            AddressDetailResponse server_response = response.body();
                            if (server_response.getStatus() == 200) {
                                dialog.dismissLoadingDialog();
                                String abc;

                                if(!server_response.getDetail().getStreet().isEmpty()&&!server_response.getDetail().getCity().isEmpty()&&!server_response.getDetail().getApartment().isEmpty()&&!server_response.getDetail().getBuilding().isEmpty())
                                {
                                    tv_add.setText(server_response.getDetail().getStreet()+","+server_response.getDetail().getBuilding()+","+server_response.getDetail().getApartment()+","+server_response.getDetail().getCity());
                                    tv_num.setText(server_response.getDetail().getCountry_code()+" "+server_response.getDetail().getPhone_number());
                                    tv_name.setText(server_response.getDetail().getName());
                                    tv_near.setText(server_response.getDetail().getNearest_point());

                                }
                                else
                                {
                                    tv_add.setText(server_response.getDetail().getStreet()+" "+server_response.getDetail().getBuilding()+" "+" "+server_response.getDetail().getApartment()+" "+server_response.getDetail().getCity());
                                    tv_num.setText(server_response.getDetail().getCountry_code()+" "+server_response.getDetail().getPhone_number());
                                    tv_name.setText(server_response.getDetail().getName());
                                    tv_near.setText(server_response.getDetail().getNearest_point());




                                }



                                //  setPreferences(server_response);


                            } else if (server_response.getStatus() == 400) {
                                dialog.dismissLoadingDialog();

                            } else if (server_response.getStatus() == 500) {
                                dialog.dismissLoadingDialog();

                                //   startActivity(new Intent(DonationDetailsActivity.this, MyPaymentsActivity.class));
                            } else {
                                dialog.dismissLoadingDialog();

                                Toast.makeText(MyPaymentsActivity.this, server_response.getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<AddressDetailResponse> call, Throwable t) {
                        dialog.dismissLoadingDialog();

                    }
                });
            } else {
                Toast toast = Toast.makeText(MyPaymentsActivity.this, R.string.in, Toast.LENGTH_LONG);
                toast.setGravity(Gravity.CENTER, 0, 0);
                toast.show();
            }
        }
    }


    void call()
    {
        String path1="",path2="",path3="",path4="",path5="",path6="",path7="",path8="",path9="",path10="",path11="",path12="",path13="",path14="",path15="",path16="",path17="",path18="",path19="",path20="",path21="",path22="",path23="",
                path24="",path25="",path26="",path27="",path28="",path29="",path30="",path31="",path32="",path33="",path34="",path35="",path36="",path37="",path38="",path39="",path40="",path41="",path42="",path43="",path44="",path45="",
                path46="",path47="",path48="",path49="",path50="";

        String path51="",path52="",path53="",path54="",path55="",path56="",path57="",path58="",path59="",path60="",path61="",path62="",path63="",path64="",path65="",path66="",path67="",path68="",path69="",path70="",path71="",path72="",path73="",
                path74="",path75="",path76="",path77="",path78="",path79="",path80="",path81="",path82="",path83="",path84="",path85="",path86="",path87="",path88="",path89="",path90="",path91="",path92="",path93="",path94="",path95="",
                path96="",path97="",path98="",path99="",path100="";

        if(imagePathList.size()==100)
        {
            for(int i=0;i<imagePathList.size();i++)
            {
                if(i==0)
                {
                    path1=   imagePathList.get(i);
                }
                else if(i==1)
                {
                    path2=   imagePathList.get(i);

                }
                else if(i==2)
                {
                    path3=   imagePathList.get(i);

                }
                else if(i==3)
                {
                    path4=   imagePathList.get(i);

                }
                else if(i==4)
                {
                    path5=   imagePathList.get(i);

                }
                else if(i==5)
                {
                    path6=   imagePathList.get(i);

                }
                else if(i==6)
                {
                    path7=   imagePathList.get(i);

                }
                else  if(i==7)
                {
                    path8=   imagePathList.get(i);

                }
                else if(i==8)
                {
                    path9=   imagePathList.get(i);

                }
                else if(i==9)
                {
                    path10=   imagePathList.get(i);

                }
                else  if(i==10)
                {
                    path11=   imagePathList.get(i);

                }
                else if(i==11)
                {
                    path12=   imagePathList.get(i);

                }
                else if(i==12)
                {
                    path13=   imagePathList.get(i);

                }
                else  if(i==13)
                {
                    path14=   imagePathList.get(i);

                }
                else if(i==14)
                {
                    path15=   imagePathList.get(i);

                }
                else if(i==15)
                {
                    path16=   imagePathList.get(i);

                }
                else  if(i==16)
                {
                    path17=   imagePathList.get(i);

                }
                else  if(i==17)
                {
                    path18=   imagePathList.get(i);

                }
                else  if(i==18)
                {
                    path19=   imagePathList.get(i);

                }
                else if(i==19)
                {
                    path20=   imagePathList.get(i);

                }
                else if(i==20)
                {
                    path21=   imagePathList.get(i);

                }
                else if(i==21)
                {
                    path22=   imagePathList.get(i);

                }
                else  if(i==22)
                {
                    path23=   imagePathList.get(i);

                }
                else if(i==23)
                {
                    path24=   imagePathList.get(i);

                }
                else if(i==24)
                {
                    path25=   imagePathList.get(i);

                }
                else if(i==25)
                {
                    path26=   imagePathList.get(i);

                }
                else  if(i==26)
                {
                    path27=   imagePathList.get(i);

                }
                else if(i==27)
                {
                    path28=   imagePathList.get(i);

                }
                else if(i==28)
                {
                    path29=   imagePathList.get(i);

                }
                else  if(i==29)
                {
                    path30=   imagePathList.get(i);

                }
                else if(i==30)
                {
                    path31=   imagePathList.get(i);

                }
                else if(i==31)
                {
                    path32=   imagePathList.get(i);

                }
                else  if(i==32)
                {
                    path33=   imagePathList.get(i);

                }
                else if(i==33)
                {
                    path34=   imagePathList.get(i);

                }
                else if(i==34)
                {
                    path35=   imagePathList.get(i);

                }
                else  if(i==35)
                {
                    path36=   imagePathList.get(i);

                }
                else if(i==36)
                {
                    path37=   imagePathList.get(i);

                }

                else if(i==37)
                {
                    path38=   imagePathList.get(i);

                }
                else if(i==38)
                {
                    path39=   imagePathList.get(i);

                }
                else  if(i==39)
                {
                    path40=   imagePathList.get(i);

                }
                else if(i==40)
                {

                    path41=   imagePathList.get(i);

                }
                else if(i==41)
                {
                    path42=   imagePathList.get(i);

                }
                else if(i==42)
                {
                    path43=   imagePathList.get(i);

                }
                else  if(i==43)
                {
                    path44=   imagePathList.get(i);

                }
                else if(i==44)
                {
                    path45=   imagePathList.get(i);

                }
                else  if(i==45)
                {
                    path46=   imagePathList.get(i);

                }
                else if(i==46)
                {
                    path47=   imagePathList.get(i);

                }
                else if(i==47)
                {
                    path48=   imagePathList.get(i);

                }
                else  if(i==48)
                {
                    path49=   imagePathList.get(i);

                }
                else if(i==49)
                {
                    path50=   imagePathList.get(i);
                }


                else if(i==50)
                {
                    path51=   imagePathList.get(i);
                }
                else if(i==51)
                {
                    path52=   imagePathList.get(i);

                }
                else if(i==52)
                {
                    path53=   imagePathList.get(i);

                }
                else if(i==53)
                {
                    path54=   imagePathList.get(i);

                }
                else if(i==54)
                {
                    path55=   imagePathList.get(i);

                }
                else if(i==55)
                {
                    path56=   imagePathList.get(i);

                }
                else if(i==56)
                {
                    path57=   imagePathList.get(i);

                }
                else  if(i==57)
                {
                    path58=   imagePathList.get(i);

                }
                else if(i==58)
                {
                    path59=   imagePathList.get(i);

                }
                else if(i==59)
                {
                    path60=   imagePathList.get(i);

                }
                else  if(i==60)
                {
                    path61=   imagePathList.get(i);

                }
                else if(i==61)
                {
                    path62=   imagePathList.get(i);

                }
                else if(i==62)
                {
                    path63=   imagePathList.get(i);

                }
                else  if(i==63)
                {
                    path64=   imagePathList.get(i);

                }
                else if(i==64)
                {
                    path65=   imagePathList.get(i);

                }
                else if(i==65)
                {
                    path66=   imagePathList.get(i);

                }
                else  if(i==66)
                {
                    path67=   imagePathList.get(i);

                }
                else  if(i==67)
                {
                    path68=   imagePathList.get(i);

                }
                else  if(i==68)
                {
                    path69=   imagePathList.get(i);

                }
                else if(i==69)
                {
                    path70=   imagePathList.get(i);

                }
                else if(i==70)
                {
                    path71=   imagePathList.get(i);

                }
                else if(i==71)
                {
                    path72=   imagePathList.get(i);

                }
                else  if(i==72)
                {
                    path73=   imagePathList.get(i);

                }
                else if(i==73)
                {
                    path74=   imagePathList.get(i);

                }
                else if(i==74)
                {
                    path75=   imagePathList.get(i);

                }
                else if(i==75)
                {
                    path76=   imagePathList.get(i);

                }
                else  if(i==76)
                {
                    path77=   imagePathList.get(i);

                }
                else if(i==77)
                {
                    path78=   imagePathList.get(i);

                }
                else if(i==78)
                {
                    path79=   imagePathList.get(i);

                }
                else  if(i==79)
                {
                    path80=   imagePathList.get(i);

                }
                else if(i==80)
                {
                    path81=   imagePathList.get(i);

                }
                else if(i==81)
                {
                    path82=   imagePathList.get(i);

                }
                else  if(i==82)
                {
                    path83=   imagePathList.get(i);

                }
                else if(i==83)
                {
                    path84=   imagePathList.get(i);

                }
                else if(i==84)
                {
                    path85=   imagePathList.get(i);

                }
                else  if(i==85)
                {
                    path86=   imagePathList.get(i);

                }
                else if(i==86)
                {
                    path87=   imagePathList.get(i);

                }

                else if(i==87)
                {
                    path88=   imagePathList.get(i);

                }
                else if(i==88)
                {
                    path89=   imagePathList.get(i);

                }
                else  if(i==89)
                {
                    path90=   imagePathList.get(i);

                }
                else if(i==90)
                {

                    path91=   imagePathList.get(i);

                }
                else if(i==91)
                {
                    path92=   imagePathList.get(i);

                }
                else if(i==92)
                {
                    path93=   imagePathList.get(i);

                }
                else  if(i==93)
                {
                    path94=   imagePathList.get(i);

                }
                else if(i==94)
                {
                    path95=   imagePathList.get(i);

                }
                else  if(i==95)
                {
                    path96=   imagePathList.get(i);

                }
                else if(i==96)
                {
                    path97=   imagePathList.get(i);

                }
                else if(i==97)
                {
                    path98=   imagePathList.get(i);

                }
                else  if(i==98)
                {
                    path99=   imagePathList.get(i);

                }
                else if(i==99)
                {
                    path100=   imagePathList.get(i);
                }



            }

            if (new InternetCheck(MyPaymentsActivity.this).isConnect()) {
                String key = SharedPreferenceWriter.getInstance(MyPaymentsActivity.this).getString(SPreferenceKey.jwtToken);
                String id = SharedPreferenceWriter.getInstance(MyPaymentsActivity.this).getString(SPreferenceKey._id);
                String token="Token"+" "+key;
                DialogPopup dialogs = new DialogPopup(MyPaymentsActivity.this);
                dialogs.showLoadingDialog(MyPaymentsActivity.this, "");
                System.out.print(token);

                ApiInterface api_service = RetrofitInit.getConnect().createConnection();
                Call<ImageResponse> call = api_service.createPath(token,path1,path2,path3,path4,path5,path6,path7,path8,path9,path10,
                        path11,path12,path13,path14,path15,path16,path17,path18,path19,path20,path21,
                        path22,path23,path24,path25,path26,path27,path28,path29,path30,path31,
                        path32,path33,path34,path35,path36,path37,path38,path39,path40,
                        path41,path42,path43,path44,path45,path46,path47,path48,path49,path50,path51,path52,path53,path54,path55,path56,path57,path58,path59,path60,
                        path61,path62,path63,path64,path65,path66,path67,path68,path69,path70,path71,
                        path72,path73,path74,path75,path76,path77,path78,path79,path80,path81,
                        path82,path83,path84,path85,path86,path87,path88,path89,path90,
                        path91,path92,path93,path94,path95,path96,path97,path98,path99,path100);
                call.enqueue(new Callback<ImageResponse>() {
                    @Override
                    public void onResponse(Call<ImageResponse> call, Response<ImageResponse> response) {
                        if (response.isSuccessful()) {
                            ImageResponse server_response = response.body();
                            if (server_response.getStatus() == 200) {
                                dialogs.dismissLoadingDialog();




//                            dialog.dismiss();
                                // fireBaseAction();
                                //  showDialogotp();


                            } else if (server_response.getStatus() == 500) {
                                dialogs.dismissLoadingDialog();
                                Toast.makeText(MyPaymentsActivity.this, server_response.getMessage(), Toast.LENGTH_SHORT).show();


                                //  startActivity(new Intent(MyPaymentsActivity.this, MyPaymentsActivity.class));
                            } else if (server_response.getStatus() == 400) {
                                dialogs.dismissLoadingDialog();
                                Toast.makeText(MyPaymentsActivity.this, server_response.getMessage(), Toast.LENGTH_SHORT).show();



                            } else {
                                dialogs.dismissLoadingDialog();

                                Toast.makeText(MyPaymentsActivity.this, server_response.getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<ImageResponse> call, Throwable t) {
                        dialogs.dismissLoadingDialog();
                        Toast.makeText(MyPaymentsActivity.this, R.string.your, Toast.LENGTH_SHORT).show();


                    }
                });
            } else {
                Toast toast = Toast.makeText(MyPaymentsActivity.this, R.string.connectionsss, Toast.LENGTH_LONG);
                toast.setGravity(Gravity.CENTER, 0, 0);
                toast.show();
            }
        }
        else
        {
            for(int i=0;i<imagePathList.size();i++)
            {
                if(i==0)
                {
                    path1=   imagePathList.get(i);
                }
                else if(i==1)
                {
                    path2=   imagePathList.get(i);

                }
                else if(i==2)
                {
                    path3=   imagePathList.get(i);

                }
                else if(i==3)
                {
                    path4=   imagePathList.get(i);

                }
                else if(i==4)
                {
                    path5=   imagePathList.get(i);

                }
                else if(i==5)
                {
                    path6=   imagePathList.get(i);

                }
                else if(i==6)
                {
                    path7=   imagePathList.get(i);

                }
                else  if(i==7)
                {
                    path8=   imagePathList.get(i);

                }
                else if(i==8)
                {
                    path9=   imagePathList.get(i);

                }
                else if(i==9)
                {
                    path10=   imagePathList.get(i);

                }
                else  if(i==10)
                {
                    path11=   imagePathList.get(i);

                }
                else if(i==11)
                {
                    path12=   imagePathList.get(i);

                }
                else if(i==12)
                {
                    path13=   imagePathList.get(i);

                }
                else  if(i==13)
                {
                    path14=   imagePathList.get(i);

                }
                else if(i==14)
                {
                    path15=   imagePathList.get(i);

                }
                else if(i==15)
                {
                    path16=   imagePathList.get(i);

                }
                else  if(i==16)
                {
                    path17=   imagePathList.get(i);

                }
                else  if(i==17)
                {
                    path18=   imagePathList.get(i);

                }
                else  if(i==18)
                {
                    path19=   imagePathList.get(i);

                }
                else if(i==19)
                {
                    path20=   imagePathList.get(i);

                }
                else if(i==20)
                {
                    path21=   imagePathList.get(i);

                }
                else if(i==21)
                {
                    path22=   imagePathList.get(i);

                }
                else  if(i==22)
                {
                    path23=   imagePathList.get(i);

                }
                else if(i==23)
                {
                    path24=   imagePathList.get(i);

                }
                else if(i==24)
                {
                    path25=   imagePathList.get(i);

                }
                else if(i==25)
                {
                    path26=   imagePathList.get(i);

                }
                else  if(i==26)
                {
                    path27=   imagePathList.get(i);

                }
                else if(i==27)
                {
                    path28=   imagePathList.get(i);

                }
                else if(i==28)
                {
                    path29=   imagePathList.get(i);

                }
                else  if(i==29)
                {
                    path30=   imagePathList.get(i);

                }
                else if(i==30)
                {
                    path31=   imagePathList.get(i);

                }
                else if(i==31)
                {
                    path32=   imagePathList.get(i);

                }
                else  if(i==32)
                {
                    path33=   imagePathList.get(i);

                }
                else if(i==33)
                {
                    path34=   imagePathList.get(i);

                }
                else if(i==34)
                {
                    path35=   imagePathList.get(i);

                }
                else  if(i==35)
                {
                    path36=   imagePathList.get(i);

                }
                else if(i==36)
                {
                    path37=   imagePathList.get(i);

                }

                else if(i==37)
                {
                    path38=   imagePathList.get(i);

                }
                else if(i==38)
                {
                    path39=   imagePathList.get(i);

                }
                else  if(i==39)
                {
                    path40=   imagePathList.get(i);

                }
                else if(i==40)
                {

                    path41=   imagePathList.get(i);

                }
                else if(i==41)
                {
                    path42=   imagePathList.get(i);

                }
                else if(i==42)
                {
                    path43=   imagePathList.get(i);

                }
                else  if(i==43)
                {
                    path44=   imagePathList.get(i);

                }
                else if(i==44)
                {
                    path45=   imagePathList.get(i);

                }
                else  if(i==45)
                {
                    path46=   imagePathList.get(i);

                }
                else if(i==46)
                {
                    path47=   imagePathList.get(i);

                }
                else if(i==47)
                {
                    path48=   imagePathList.get(i);

                }
                else  if(i==48)
                {
                    path49=   imagePathList.get(i);

                }
                else if(i==49)
                {
                    path50=   imagePathList.get(i);
                }





            }

            if (new InternetCheck(MyPaymentsActivity.this).isConnect()) {
                String key = SharedPreferenceWriter.getInstance(MyPaymentsActivity.this).getString(SPreferenceKey.jwtToken);
                String id = SharedPreferenceWriter.getInstance(MyPaymentsActivity.this).getString(SPreferenceKey._id);
                String token="Token"+" "+key;
                DialogPopup dialogs = new DialogPopup(MyPaymentsActivity.this);
                dialogs.showLoadingDialog(MyPaymentsActivity.this, "");
                System.out.print(token);

                ApiInterface api_service = RetrofitInit.getConnect().createConnection();
                Call<ImageResponse> call = api_service.createPath(token,path1,path2,path3,path4,path5,path6,path7,path8,path9,path10,
                        path11,path12,path13,path14,path15,path16,path17,path18,path19,path20,path21,
                        path22,path23,path24,path25,path26,path27,path28,path29,path30,path31,
                        path32,path33,path34,path35,path36,path37,path38,path39,path40,
                        path41,path42,path43,path44,path45,path46,path47,path48,path49,path50,path51,path52,path53,path54,path55,path56,path57,path58,path59,path60,
                        path61,path62,path63,path64,path65,path66,path67,path68,path69,path70,path71,
                        path72,path73,path74,path75,path76,path77,path78,path79,path80,path81,
                        path82,path83,path84,path85,path86,path87,path88,path89,path90,
                        path91,path92,path93,path94,path95,path96,path97,path98,path99,path100);
                call.enqueue(new Callback<ImageResponse>() {
                    @Override
                    public void onResponse(Call<ImageResponse> call, Response<ImageResponse> response) {
                        if (response.isSuccessful()) {
                            ImageResponse server_response = response.body();
                            if (server_response.getStatus() == 200) {
                                dialogs.dismissLoadingDialog();




//                            dialog.dismiss();
                                // fireBaseAction();
                                //  showDialogotp();


                            } else if (server_response.getStatus() == 500) {
                                dialogs.dismissLoadingDialog();
                                Toast.makeText(MyPaymentsActivity.this, server_response.getMessage(), Toast.LENGTH_SHORT).show();


                                //  startActivity(new Intent(MyPaymentsActivity.this, MyPaymentsActivity.class));
                            } else if (server_response.getStatus() == 400) {
                                dialogs.dismissLoadingDialog();
                                Toast.makeText(MyPaymentsActivity.this, server_response.getMessage(), Toast.LENGTH_SHORT).show();



                            } else {
                                dialogs.dismissLoadingDialog();

                                Toast.makeText(MyPaymentsActivity.this, server_response.getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<ImageResponse> call, Throwable t) {
                        dialogs.dismissLoadingDialog();
                        Toast.makeText(MyPaymentsActivity.this, R.string.your, Toast.LENGTH_SHORT).show();


                    }
                });
            } else {
                Toast toast = Toast.makeText(MyPaymentsActivity.this, R.string.connectionsss, Toast.LENGTH_LONG);
                toast.setGravity(Gravity.CENTER, 0, 0);
                toast.show();
            }
        }

    }
    private void homeImage() {
        {

            String deviceToken = SharedPreferenceWriter.getInstance(this).getString(SPreferenceKey.DEVICETOKEN);

            if (new InternetCheck(this).isConnect()) {
                //aray1@gmail.com,arya8055


                ApiInterface api_service = RetrofitInit.getConnects().createConnections();
                Call<PaymentResponse> call = api_service.payment(orderid,totalvalue);
                call.enqueue(new Callback<PaymentResponse>() {
                    @Override
                    public void onResponse(Call<PaymentResponse> call, Response<PaymentResponse> response) {
                        if (response.isSuccessful()) {
                            PaymentResponse server_response = response.body();
                            if (server_response.getStatus() == 200) {
                                Intent i = new Intent(MyPaymentsActivity.this, WebViewPayment.class);
                                i.putExtra("id", server_response.getUrl());

                                i.putExtra("orderid", orderid);
                                i.putExtra("total", totalvalue);


                                MyPaymentsActivity.this.startActivity(i);

                            }  else {

                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<PaymentResponse> call, Throwable t) {

                    }
                });
            } else {
                Toast toast = Toast.makeText(this, R.string.connectionsss, Toast.LENGTH_LONG);
                toast.setGravity(Gravity.CENTER, 0, 0);
                toast.show();
            }
        }


    }


    @Override
    public void onBackPressed() {
        Intent i = new Intent(MyPaymentsActivity.this, BookActivity.class);
        i.putExtra("id", id);
        i.putExtra("promos",promos);
        i.putExtra("size",size);
        i.putStringArrayListExtra("test", imagePathList);

        MyPaymentsActivity.this.startActivity(i);
    }

    private void serviceOrder() {
        {
            if (new InternetCheck(this).isConnect()) {
                //aray1@gmail.com,arya8055
                String key = SharedPreferenceWriter.getInstance(MyPaymentsActivity.this).getString(SPreferenceKey.jwtToken);
                String token="Token"+" "+key;
                String numberofphotos = SharedPreferenceWriter.getInstance(MyPaymentsActivity.this).getString(SPreferenceKey.size);

                String one = SharedPreferenceWriter.getInstance(MyPaymentsActivity.this).getString(SPreferenceKey.id1);
                String s = SharedPreferenceWriter.getInstance(MyPaymentsActivity.this).getString(SPreferenceKey.id2);
                String t = SharedPreferenceWriter.getInstance(MyPaymentsActivity.this).getString(SPreferenceKey.id3);
                String fo = SharedPreferenceWriter.getInstance(MyPaymentsActivity.this).getString(SPreferenceKey.id4);
                String f = SharedPreferenceWriter.getInstance(MyPaymentsActivity.this).getString(SPreferenceKey.id5);
                String si = SharedPreferenceWriter.getInstance(MyPaymentsActivity.this).getString(SPreferenceKey.id6);
                String se = SharedPreferenceWriter.getInstance(MyPaymentsActivity.this).getString(SPreferenceKey.id7);
                String ei = SharedPreferenceWriter.getInstance(MyPaymentsActivity.this).getString(SPreferenceKey.id8);
                String ni = SharedPreferenceWriter.getInstance(MyPaymentsActivity.this).getString(SPreferenceKey.id9);
                String te = SharedPreferenceWriter.getInstance(MyPaymentsActivity.this).getString(SPreferenceKey.id10);
                String eleven = SharedPreferenceWriter.getInstance(MyPaymentsActivity.this).getString(SPreferenceKey.id11);
                String twelve = SharedPreferenceWriter.getInstance(MyPaymentsActivity.this).getString(SPreferenceKey.id12);
                String thirteen = SharedPreferenceWriter.getInstance(MyPaymentsActivity.this).getString(SPreferenceKey.id13);
                String forteen = SharedPreferenceWriter.getInstance(MyPaymentsActivity.this).getString(SPreferenceKey.id14);
                String fifteen = SharedPreferenceWriter.getInstance(MyPaymentsActivity.this).getString(SPreferenceKey.id15);
                String sixteen = SharedPreferenceWriter.getInstance(MyPaymentsActivity.this).getString(SPreferenceKey.id16);
                String seventeen = SharedPreferenceWriter.getInstance(MyPaymentsActivity.this).getString(SPreferenceKey.id17);
                String eighteen = SharedPreferenceWriter.getInstance(MyPaymentsActivity.this).getString(SPreferenceKey.id18);
                String ninteen = SharedPreferenceWriter.getInstance(MyPaymentsActivity.this).getString(SPreferenceKey.id19);
                String twenty = SharedPreferenceWriter.getInstance(MyPaymentsActivity.this).getString(SPreferenceKey.id20);
                String  sizeone = SharedPreferenceWriter.getInstance(MyPaymentsActivity.this).getString(SPreferenceKey.val);


                DialogPopup dialog = new DialogPopup(this);
                dialog.showLoadingDialog(this, "");

                ApiInterface api_service = RetrofitInit.getConnect().createConnection();


                if(numberofphotos.matches("50"))
                {
                    Call<OrderResponse> call = api_service.order(token,id,one,s,t,fo,f,si,se,ei,ni,te,one,s,t,fo,f,si,se,ei,ni,te,sizeone,numberofphotos);
                    call.enqueue(new Callback<OrderResponse>() {
                        @Override
                        public void onResponse(Call<OrderResponse> call, Response<OrderResponse> response) {
                            if (response.isSuccessful()) {
                                OrderResponse server_response = response.body();
                                if (server_response.getStatus() == 200) {
                                    dialog.dismissLoadingDialog();
                                    orderid=String.valueOf(server_response.getData().get_$OrderId127());
                                    paymentAmount=String.valueOf(server_response.getData().getTotal());
                                    call();
                                    if(rad1.isChecked())
                                    {
                                        homeImage();

                                    }
                                    else if(rad2.isChecked())
                                    {
                                        asiapayment();
                                    }
                                    else if(rad6.isChecked())
                                    {
                                        cardResponse();
                                    }

                                    else
                                    {
                                        servicePayment();

                                    }
                                } else if (server_response.getStatus() == 500) {
                                    dialog.dismissLoadingDialog();

                                    //   startActivity(new Intent(CustomerStoryActivity.this, MyPaymentsActivity.class));
                                } else {
                                    dialog.dismissLoadingDialog();

                                }
                            }
                        }

                        @Override
                        public void onFailure(Call<OrderResponse> call, Throwable t) {
                            dialog.dismissLoadingDialog();

                        }
                    });
                }
                else
                {
                    Call<OrderResponse> call = api_service.order(token,id,one,s,t,fo,f,si,se,ei,ni,te,eleven,twelve,thirteen,forteen,fifteen,sixteen,seventeen,eighteen,ninteen,twenty,sizeone,"100");
                    call.enqueue(new Callback<OrderResponse>() {
                        @Override
                        public void onResponse(Call<OrderResponse> call, Response<OrderResponse> response) {
                            if (response.isSuccessful()) {
                                OrderResponse server_response = response.body();
                                if (server_response.getStatus() == 200) {
                                    dialog.dismissLoadingDialog();
                                    orderid=String.valueOf(server_response.getData().get_$OrderId127());
                                    paymentAmount=String.valueOf(server_response.getData().getTotal());
                                    call();
                                    if(rad1.isChecked())
                                    {
                                        homeImage();

                                    }
                                    else if(rad2.isChecked())
                                    {
                                        asiapayment();
                                    }
                                    else if(rad6.isChecked())
                                    {
                                        cardResponse();
                                    }

                                    else
                                    {
                                        servicePayment();

                                    }
                                } else if (server_response.getStatus() == 500) {
                                    dialog.dismissLoadingDialog();

                                    //   startActivity(new Intent(CustomerStoryActivity.this, MyPaymentsActivity.class));
                                } else {
                                    dialog.dismissLoadingDialog();

                                }
                            }
                        }

                        @Override
                        public void onFailure(Call<OrderResponse> call, Throwable t) {
                            dialog.dismissLoadingDialog();

                        }
                    });
                }

            } else {
                Toast toast = Toast.makeText(this, R.string.connectionsss, Toast.LENGTH_LONG);
                toast.setGravity(Gravity.CENTER, 0, 0);
                toast.show();
            }
        }


    }
    private void serviceOrderPrmo() {
        {
            if (new InternetCheck(this).isConnect()) {
                //aray1@gmail.com,arya8055
                String key = SharedPreferenceWriter.getInstance(MyPaymentsActivity.this).getString(SPreferenceKey.jwtToken);
                String token="Token"+" "+key;
                String numberofphotos = SharedPreferenceWriter.getInstance(MyPaymentsActivity.this).getString(SPreferenceKey.size);

                String one = SharedPreferenceWriter.getInstance(MyPaymentsActivity.this).getString(SPreferenceKey.id1);
                String s = SharedPreferenceWriter.getInstance(MyPaymentsActivity.this).getString(SPreferenceKey.id2);
                String t = SharedPreferenceWriter.getInstance(MyPaymentsActivity.this).getString(SPreferenceKey.id3);
                String fo = SharedPreferenceWriter.getInstance(MyPaymentsActivity.this).getString(SPreferenceKey.id4);
                String f = SharedPreferenceWriter.getInstance(MyPaymentsActivity.this).getString(SPreferenceKey.id5);
                String si = SharedPreferenceWriter.getInstance(MyPaymentsActivity.this).getString(SPreferenceKey.id6);
                String se = SharedPreferenceWriter.getInstance(MyPaymentsActivity.this).getString(SPreferenceKey.id7);
                String ei = SharedPreferenceWriter.getInstance(MyPaymentsActivity.this).getString(SPreferenceKey.id8);
                String ni = SharedPreferenceWriter.getInstance(MyPaymentsActivity.this).getString(SPreferenceKey.id9);
                String te = SharedPreferenceWriter.getInstance(MyPaymentsActivity.this).getString(SPreferenceKey.id10);
                String eleven = SharedPreferenceWriter.getInstance(MyPaymentsActivity.this).getString(SPreferenceKey.id11);
                String twelve = SharedPreferenceWriter.getInstance(MyPaymentsActivity.this).getString(SPreferenceKey.id12);
                String thirteen = SharedPreferenceWriter.getInstance(MyPaymentsActivity.this).getString(SPreferenceKey.id13);
                String forteen = SharedPreferenceWriter.getInstance(MyPaymentsActivity.this).getString(SPreferenceKey.id14);
                String fifteen = SharedPreferenceWriter.getInstance(MyPaymentsActivity.this).getString(SPreferenceKey.id15);
                String sixteen = SharedPreferenceWriter.getInstance(MyPaymentsActivity.this).getString(SPreferenceKey.id16);
                String seventeen = SharedPreferenceWriter.getInstance(MyPaymentsActivity.this).getString(SPreferenceKey.id17);
                String eighteen = SharedPreferenceWriter.getInstance(MyPaymentsActivity.this).getString(SPreferenceKey.id18);
                String ninteen = SharedPreferenceWriter.getInstance(MyPaymentsActivity.this).getString(SPreferenceKey.id19);
                String twenty = SharedPreferenceWriter.getInstance(MyPaymentsActivity.this).getString(SPreferenceKey.id20);
              String  sizeone = SharedPreferenceWriter.getInstance(MyPaymentsActivity.this).getString(SPreferenceKey.val);

                DialogPopup dialog = new DialogPopup(this);
                dialog.showLoadingDialog(this, "");

                ApiInterface api_service = RetrofitInit.getConnect().createConnection();
                if(numberofphotos.matches("50"))
                {
                    Call<OrderResponse> call = api_service.orderp(token,id,promos,one,s,t,fo,f,si,se,ei,ni,te,one,s,t,fo,f,si,se,ei,ni,te,sizeone,numberofphotos);
                    call.enqueue(new Callback<OrderResponse>() {
                        @Override
                        public void onResponse(Call<OrderResponse> call, Response<OrderResponse> response) {
                            if (response.isSuccessful()) {
                                OrderResponse server_response = response.body();
                                if (server_response.getStatus() == 200) {
                                    dialog.dismissLoadingDialog();
                                    orderid=String.valueOf(server_response.getData().get_$OrderId127());
                                    paymentAmount=String.valueOf(server_response.getData().getTotal());
                                    call();
                                    if(rad1.isChecked())
                                    {
                                        homeImage();

                                    }
                                    else if(rad2.isChecked())
                                    {
                                        asiapayment();
                                    }
                                    else if(rad6.isChecked())
                                    {
                                        cardResponse();
                                    }

                                    else
                                    {
                                        servicePayment();

                                    }
                                } else if (server_response.getStatus() == 500) {
                                    dialog.dismissLoadingDialog();

                                    //   startActivity(new Intent(CustomerStoryActivity.this, MyPaymentsActivity.class));
                                } else {
                                    dialog.dismissLoadingDialog();

                                }
                            }
                        }

                        @Override
                        public void onFailure(Call<OrderResponse> call, Throwable t) {
                            dialog.dismissLoadingDialog();

                        }
                    });
                }
                else
                {
                    Call<OrderResponse> call = api_service.orderp(token,id,promos,one,s,t,fo,f,si,se,ei,ni,te,eleven,twelve,thirteen,forteen,fifteen,sixteen,seventeen,eighteen,ninteen,twenty,sizeone,"100");
                    call.enqueue(new Callback<OrderResponse>() {
                        @Override
                        public void onResponse(Call<OrderResponse> call, Response<OrderResponse> response) {
                            if (response.isSuccessful()) {
                                OrderResponse server_response = response.body();
                                if (server_response.getStatus() == 200) {
                                    dialog.dismissLoadingDialog();
                                    orderid=String.valueOf(server_response.getData().get_$OrderId127());
                                    paymentAmount=String.valueOf(server_response.getData().getTotal());
                                    call();
                                    if(rad1.isChecked())
                                    {
                                        homeImage();

                                    }
                                    else if(rad2.isChecked())
                                    {
                                        asiapayment();
                                    }
                                    else if(rad6.isChecked())
                                    {
                                        cardResponse();
                                    }

                                    else
                                    {
                                        servicePayment();

                                    }
                                } else if (server_response.getStatus() == 500) {
                                    dialog.dismissLoadingDialog();

                                    //   startActivity(new Intent(CustomerStoryActivity.this, MyPaymentsActivity.class));
                                } else {
                                    dialog.dismissLoadingDialog();

                                }
                            }
                        }

                        @Override
                        public void onFailure(Call<OrderResponse> call, Throwable t) {
                            dialog.dismissLoadingDialog();

                        }
                    });
                }
            } else {
                Toast toast = Toast.makeText(this, R.string.connectionsss, Toast.LENGTH_LONG);
                toast.setGravity(Gravity.CENTER, 0, 0);
                toast.show();
            }
        }


    }
    private  void asiapayment()
    {
        Intent i = new Intent(MyPaymentsActivity.this, WebsAisaView.class);

        String baseurl="https://snapicapp.com/api/aisa-hawala/?currency_code=IQD&transactionid=";
        String amount="&amount=";
        String pay="&lc=en_US&notify_url=https://snapicapp.com";
        i.putExtra("url", baseurl+orderid+amount+totalvalue+pay);

//        i.putExtra("orderid", orderid);
//        i.putExtra("total", totalvalue);


        MyPaymentsActivity.this.startActivity(i);

    }

    private void servicePayment() {
        {
            if (new InternetCheck(this).isConnect()) {
                //aray1@gmail.com,arya8055
                String key = SharedPreferenceWriter.getInstance(MyPaymentsActivity.this).getString(SPreferenceKey.jwtToken);
                String token="Token"+" "+key;
                DialogPopup dialog = new DialogPopup(this);
                dialog.showLoadingDialog(this, "");
                String date = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault()).format(new Date());

                ApiInterface api_service = RetrofitInit.getConnect().createConnection();
                Call<PaymentDoneResponse> call = api_service.paymentDone(token,orderid,"na",paymentAmount,"cash",date,"na");
                call.enqueue(new Callback<PaymentDoneResponse>() {
                    @Override
                    public void onResponse(Call<PaymentDoneResponse> call, Response<PaymentDoneResponse> response) {
                        if (response.isSuccessful()) {
                            PaymentDoneResponse server_response = response.body();
                            if (server_response.getStatus() == 200) {
                                dialog.dismissLoadingDialog();

                                SharedPreferenceWriter.getInstance(MyPaymentsActivity.this).writeStringValue(SPreferenceKey.id1,"");
                                SharedPreferenceWriter.getInstance(MyPaymentsActivity.this).writeStringValue(SPreferenceKey.id2,"");
                                SharedPreferenceWriter.getInstance(MyPaymentsActivity.this).writeStringValue(SPreferenceKey.id3,"");
                                SharedPreferenceWriter.getInstance(MyPaymentsActivity.this).writeStringValue(SPreferenceKey.id4,"");
                                SharedPreferenceWriter.getInstance(MyPaymentsActivity.this).writeStringValue(SPreferenceKey.id5,"");
                                SharedPreferenceWriter.getInstance(MyPaymentsActivity.this).writeStringValue(SPreferenceKey.id6,"");
                                SharedPreferenceWriter.getInstance(MyPaymentsActivity.this).writeStringValue(SPreferenceKey.id7,"");
                                SharedPreferenceWriter.getInstance(MyPaymentsActivity.this).writeStringValue(SPreferenceKey.id8,"");
                                SharedPreferenceWriter.getInstance(MyPaymentsActivity.this).writeStringValue(SPreferenceKey.id9,"");
                                SharedPreferenceWriter.getInstance(MyPaymentsActivity.this).writeStringValue(SPreferenceKey.id10,"");


                                startActivity(new Intent(MyPaymentsActivity.this, HomeActivity.class));
                            } else if (server_response.getStatus() == 500) {
                                dialog.dismissLoadingDialog();

                                //   startActivity(new Intent(CustomerStoryActivity.this, MyPaymentsActivity.class));
                            } else {
                                dialog.dismissLoadingDialog();

                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<PaymentDoneResponse> call, Throwable t) {
                        dialog.dismissLoadingDialog();

                    }
                });
            } else {
                Toast toast = Toast.makeText(this, R.string.connectionsss, Toast.LENGTH_LONG);
                toast.setGravity(Gravity.CENTER, 0, 0);
                toast.show();
            }
        }


    }

}

