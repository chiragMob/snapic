package com.snapic.outeractivity;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.util.Log;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;
import com.google.firebase.iid.FirebaseInstanceId;
import com.snapic.R;
import com.snapic.activity.HomeActivity;
import com.snapic.activity.WelcomeFirstActivity;
import com.snapic.utilities.SPreferenceKey;
import com.snapic.utilities.SharedPreferenceWriter;
import com.snapic.utility.LocaleHelper;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import butterknife.BindView;
import butterknife.ButterKnife;

public class SplashActivity extends AppCompatActivity implements  GoogleApiClient.OnConnectionFailedListener, GoogleApiClient.ConnectionCallbacks, LocationListener {
    private final int SPLASH_DISPLAY_LENGTH = 2000;
    Dialog dialog;
    private static final int REQUEST_CHECK_SETTINGS_GPS = 27;
    private static final int REQUEST_ID_MULTIPLE_PERMISSIONS = 255;
    GoogleApiClient googleApiClient;

    List<String> listPermissionsNeeded;
    @BindView(R.id.imageView)
    ImageView imageView;
    Handler handler;
    String value="";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        ButterKnife.bind(this);
        handler=new Handler();


        handler.postDelayed(new Runnable() {
            @Override
            public void run() {

                String userId = SharedPreferenceWriter.getInstance(SplashActivity.this).getString(SPreferenceKey._id);
                String welcome = SharedPreferenceWriter.getInstance(SplashActivity.this).getString(SPreferenceKey.IS_USER_LOGIN);

                if (userId.equals("")) {
                    if(welcome.equals("1")) {
                        getDeviceToken();

                        setUpGClient();
                        value="1";


                    }
                    else

                    {
                        getDeviceToken();

                        setUpGClient();
                        value="3";


                    }
                } else {
                    getDeviceToken();

                    value="0";
                    setUpGClient();
                }
            }
        },2000);
        //showDialogchangepass();
        // setUpGClient();

        //  setUpGClient();

    }

    private void getDeviceToken() {

        final Thread thread = new Thread() {
            @Override
            public void run() {
                Log.e(">>>>>>>>>>>>>>", "thred IS running");
                try {
//                    if (SharedPreferenceWriter.getInstance(SplashActivity.this).getString(SPreferenceKey.DEVICETOKEN).isEmpty()) {

                    FirebaseInstanceId.getInstance().getInstanceId().addOnCompleteListener(task -> {
                        if (!task.isSuccessful()) {
                            return;
                        }
                        String token = Objects.requireNonNull(task.getResult()).getToken();
                        Log.e("Generated Device Token", "-->" + token);
                        Log.d("devicetoken:", token);
                        if (token == null) {
                            getDeviceToken();
                        } else {
                            SharedPreferenceWriter.getInstance(SplashActivity.this).writeStringValue(SPreferenceKey.DEVICETOKEN, token);
                        }
                    });

//                    }
                } catch (Exception e1) {
                    e1.printStackTrace();
                }
                super.run();
            }
        };
        thread.start();
    }

    private synchronized void setUpGClient() {
        googleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this, this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
        googleApiClient.connect();
    }

    // second
    @RequiresApi(api = Build.VERSION_CODES.M)

    private void checkPermissions() {
        int permissionLocation = ContextCompat.checkSelfPermission(SplashActivity.this,
                android.Manifest.permission.ACCESS_FINE_LOCATION);
        listPermissionsNeeded = new ArrayList<>();
        if (permissionLocation != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(android.Manifest.permission.ACCESS_FINE_LOCATION);
            listPermissionsNeeded.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);
            listPermissionsNeeded.add(Manifest.permission.CAMERA);

            if (!listPermissionsNeeded.isEmpty()) {
                ActivityCompat.requestPermissions(this,
                        listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]), REQUEST_ID_MULTIPLE_PERMISSIONS);
            }
        } else {
            getMyLocation();
        }
    }

    private void getMyLocation() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                if(value.equals("1"))
                {
                    Intent mainIntent = new Intent(SplashActivity.this, WelcomeFirstActivity.class);
                    SplashActivity.this.startActivity(mainIntent);
                    check();

                }
                else if(value.equals("3"))
                {
                    Intent mainIntent = new Intent(SplashActivity.this, WelcomeFirstActivity.class);
                    SplashActivity.this.startActivity(mainIntent);
                    check();
                }
                else {

                    Intent mainIntent = new Intent(SplashActivity.this, HomeActivity.class);
                    SplashActivity.this.startActivity(mainIntent);
                    check();
                }

            }


        }, SPLASH_DISPLAY_LENGTH);
    }
    void check()
    {
        String lanug = SharedPreferenceWriter.getInstance(SplashActivity.this).getString(SPreferenceKey.lan);
        if(lanug.equals("ar"))
        {
            LocaleHelper.setLocale(SplashActivity.this, "brx");
        }
        else if(lanug.equals("en"))
        {
            LocaleHelper.setLocale(SplashActivity.this, "en");

        }
        else
        {
            LocaleHelper.setLocale(SplashActivity.this, "en");

        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        for (String permission : permissions) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(this, permission)) {
                //denied
                Log.e("denied", permission);
                showSettingLocation(this);

            } else {
                if (ActivityCompat.checkSelfPermission(this, permission) == PackageManager.PERMISSION_GRANTED) {
                    //allowed
                    Log.e("allowed", permission);
                    getMyLocation();
                } else {
                    //set to never ask again
                    Log.e("set to never ask again", permission);
                    //startActivityForResult(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS), 0);
                    Toast.makeText(SplashActivity.this, R.string.grant, Toast.LENGTH_SHORT).show();
                    // User selected the Never Ask Again Option
                    Intent intent = new Intent();
                    intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                    Uri uri = Uri.fromParts("package", getPackageName(), null);
                    intent.setData(uri);
                    startActivityForResult(intent, 0);


                }
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case REQUEST_CHECK_SETTINGS_GPS:
                switch (resultCode) {
                    case Activity.RESULT_OK:
                        getMyLocation();
                        break;
                    case Activity.RESULT_CANCELED:
                        startActivityForResult(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS), 0);
                        break;
                }
                break;
        }
    }




    protected void onStop() {
        super.onStop();
//        if(mediaPlayer.isPlaying()){ //Must check if it's playing, otherwise it may be a NPE
//            mediaPlayer.pause(); //Pauses the sound
//        }
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onConnected(@Nullable Bundle bundle) {
        checkPermissions();
    }

    @Override
    public void onConnectionSuspended(int i) {

    }


    @Override
    public void onLocationChanged(Location location) {

    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }


    public void showSettingLocation(final Activity activity) {
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);

        builder.setMessage("Turn on your GPS and give Snapic access to your location.");

        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {

            @RequiresApi(api = Build.VERSION_CODES.M)
            public void onClick(DialogInterface dialog, int which) {
                // Do nothing but close the dialog
               /* Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                activity.startActivityForResult(intent, PERMISSION_REQUEST_CODE);
                dialog.dismiss();*/


                checkPermissions();
                dialog.dismiss();

            }
        });


        AlertDialog alert = builder.create();
        alert.setCanceledOnTouchOutside(false);
        if (!(activity).isFinishing()) {
            alert.show();
        }
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }
}





