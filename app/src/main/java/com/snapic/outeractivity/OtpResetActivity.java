package com.snapic.outeractivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.mukesh.OtpView;
import com.snapic.R;
import com.snapic.response.EmailNumberVerifyResponse;
import com.snapic.response.VerifyOtpResponse;
import com.snapic.retrofit.ApiInterface;
import com.snapic.retrofit.RetrofitInit;
import com.snapic.utilities.InternetCheck;

import androidx.appcompat.app.AppCompatActivity;
import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class OtpResetActivity extends AppCompatActivity {
    @BindView(R.id.txt_submit)
    TextView txt_submit;
    @BindView(R.id.img_back)
    ImageView img_back;
    String email="";
    String otpenter;

    private OtpView otpView;

    @BindView(R.id.tv_num)
    TextView tv_num;
    @BindView(R.id.tv_resend)
    TextView tv_resend;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_enter_otp);
        ButterKnife.bind(this);
        Intent i = getIntent();
        email = i.getStringExtra("email");
        otpView=findViewById(R.id.otp_view);
        tv_num.setText(email);
        txt_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                otpenter=  otpView.getText().toString();
                if(otpenter.length()==6)
                {
                    if (new InternetCheck(OtpResetActivity.this).isConnect()) {
                        verifyotp(otpenter);
                    }
                    else
                    {
                        Toast.makeText(OtpResetActivity.this, R.string.in, Toast.LENGTH_SHORT).show();
                    }

                }
                else {
                    Toast.makeText(OtpResetActivity.this, R.string.valid, Toast.LENGTH_SHORT).show();

                }

            //    startActivity(new Intent(OtpResetActivity.this,ResetPasswordActivity.class));

            }
        });
        img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        tv_resend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sendotp();
            }
        });
    }
    public void verifyotp(String otpenter) {
        if (new InternetCheck(this).isConnect()) {





            ApiInterface api_service = RetrofitInit.getConnect().createConnection();
            Call<VerifyOtpResponse> call = api_service.verifyResponse(otpenter);
            call.enqueue(new Callback<VerifyOtpResponse>() {
                @Override
                public void onResponse(Call<VerifyOtpResponse> call, Response<VerifyOtpResponse> response) {
                    if (response.isSuccessful()) {
                        VerifyOtpResponse server_response = response.body();
                        if (server_response.getStatus() == 200) {

                            Toast.makeText(OtpResetActivity.this, server_response.getMsg(), Toast.LENGTH_SHORT).show();
                            Intent i=new Intent(OtpResetActivity.this,ResetPasswordActivity.class);

                            i.putExtra("email",email);

                            OtpResetActivity.this.startActivity(i);

                        } else if (server_response.getStatus() == 400) {
                            Toast.makeText(OtpResetActivity.this, server_response.getMsg(), Toast.LENGTH_SHORT).show();


                        } else {
                            startActivity(new Intent(OtpResetActivity.this,ResetPasswordActivity.class));
                            //   Toast.makeText(LoginActivity.this, server_response.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    }
                }

                @Override
                public void onFailure(Call<VerifyOtpResponse> call, Throwable t) {

                }
            });
        } else {
            Toast toast = Toast.makeText(this, R.string.in, Toast.LENGTH_LONG);
            toast.setGravity(Gravity.CENTER, 0, 0);
            toast.show();
        }

    }

    public void sendotp() {
        if (new InternetCheck(this).isConnect()) {





            ApiInterface api_service = RetrofitInit.getConnect().createConnection();
            Call<EmailNumberVerifyResponse> call = api_service.SendEmailOtp(email);
            call.enqueue(new Callback<EmailNumberVerifyResponse>() {
                @Override
                public void onResponse(Call<EmailNumberVerifyResponse> call, Response<EmailNumberVerifyResponse> response) {
                    if (response.isSuccessful()) {
                        EmailNumberVerifyResponse server_response = response.body();
                        if (server_response.getStatus() == 200) {

                            Toast.makeText(OtpResetActivity.this, server_response.getMsg(), Toast.LENGTH_SHORT).show();



                        } else if (server_response.getStatus() == 400) {
                            Toast.makeText(OtpResetActivity.this, server_response.getMsg(), Toast.LENGTH_SHORT).show();


                        } else {

                            //   Toast.makeText(LoginActivity.this, server_response.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    }
                }

                @Override
                public void onFailure(Call<EmailNumberVerifyResponse> call, Throwable t) {

                }
            });
        } else {
            Toast toast = Toast.makeText(this, R.string.in, Toast.LENGTH_LONG);
            toast.setGravity(Gravity.CENTER, 0, 0);
            toast.show();
        }

    }


}