package com.snapic.outeractivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.snapic.R;
import com.snapic.response.ContactUsResponse;
import com.snapic.retrofit.ApiInterface;
import com.snapic.retrofit.RetrofitInit;
import com.snapic.utilities.DialogPopup;
import com.snapic.utilities.InternetCheck;
import com.snapic.utilities.SPreferenceKey;
import com.snapic.utilities.SharedPreferenceWriter;

import androidx.appcompat.app.AppCompatActivity;
import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class ContactUsActivity extends AppCompatActivity {
@BindView(R.id.img_back)
ImageView img_back;
@BindView(R.id.tv_email)
TextView tv_email;
@BindView(R.id.tv_num)
TextView tv_num;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact_us);
        ButterKnife.bind(this);
        img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        serviceContactUs();

    }
    private void serviceContactUs() {
        {
            if (new InternetCheck(ContactUsActivity.this).isConnect()) {
                String key = SharedPreferenceWriter.getInstance(ContactUsActivity.this).getString(SPreferenceKey.jwtToken);
                String id = SharedPreferenceWriter.getInstance(ContactUsActivity.this).getString(SPreferenceKey._id);
                String token="Token"+" "+key;

                //aray1@gmail.com,arya8055
                DialogPopup dialog = new DialogPopup(ContactUsActivity.this);
                dialog.showLoadingDialog(ContactUsActivity.this, "");

                ApiInterface api_service = RetrofitInit.getConnect().createConnection();
                Call<ContactUsResponse> call = api_service.contactUs(token);
                call.enqueue(new Callback<ContactUsResponse>() {
                    @Override
                    public void onResponse(Call<ContactUsResponse> call, Response<ContactUsResponse> response) {

                        if (response.isSuccessful()) {
                            ContactUsResponse server_response = response.body();

                                dialog.dismissLoadingDialog();

                                if (server_response.getStatus() == 200) {
                                    dialog.dismissLoadingDialog();
                                    tv_email.setText(server_response.getEmail());
                                    tv_num.setText(server_response.get_$PhoneNumber57());
                                    //  setPreferences(server_response);


                                } else if (server_response.getStatus() == 400) {


                                    dialog.dismissLoadingDialog();

                                }

                            else {
                                dialog.dismissLoadingDialog();

                              startActivity(new Intent(ContactUsActivity.this,LoginActivity.class));
                                }


                        }
                    }

                    @Override
                    public void onFailure(Call<ContactUsResponse> call, Throwable t) {
                        dialog.dismissLoadingDialog();

                    }
                });
            } else {
                Toast toast = Toast.makeText(ContactUsActivity.this, R.string.in, Toast.LENGTH_LONG);
                toast.setGravity(Gravity.CENTER, 0, 0);
                toast.show();
            }
        }


    }

}

