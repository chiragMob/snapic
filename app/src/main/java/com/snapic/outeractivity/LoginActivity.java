package com.snapic.outeractivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.snapic.R;
import com.snapic.SocialLogin.FirebaseFacebookLogin;
import com.snapic.SocialLogin.GoogleLoginFirebase;
import com.snapic.activity.HomeActivity;
import com.snapic.response.LoginResponse;
import com.snapic.response.SocialLoginResponse;
import com.snapic.retrofit.ApiInterface;
import com.snapic.retrofit.RetrofitInit;
import com.snapic.utilities.DialogPopup;
import com.snapic.utilities.InternetCheck;
import com.snapic.utilities.SPreferenceKey;
import com.snapic.utilities.SharedPreferenceWriter;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends AppCompatActivity {
    @BindView(R.id.txt_forgotPass)
    TextView txt_forgotPass;
    @BindView(R.id.cons_sign)
    ConstraintLayout cons_sign;
    @BindView(R.id.txt_login)
    TextView txt_login;
    @BindView(R.id.et_mail)
    EditText et_mail;
    @BindView(R.id.et_pass)
    EditText et_pass;
    String email, pass;
    @BindView(R.id.constraintLayout2)
    ConstraintLayout constraintLayoutgmail;
    @BindView(R.id.constraintLayout)
            ConstraintLayout constraintLayoutfb;
    private static final int GOOGLE_REQUEST = 101;
    private static final int RC_SIGN_IN = 9001;
    private static final int FACEBOOK_REQ = 103;

    String emailPattern = "^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
    final String PASSWORD_PATTERN = "^(?=.*[0-9])(?=.*[A-Z])(?=.*[@#$%^&+=!])(?=\\S+$).{4,}$";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);
        txt_forgotPass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
startActivity(new Intent(LoginActivity.this,ForgotPasswordActivity.class));
            }
        });
        cons_sign.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(LoginActivity.this,SignUpActivity.class));
            }
        });
        txt_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               isValidate();


            }
        });
    }
    public boolean isValidate() {

        email = et_mail.getText().toString().trim();
        pass = et_pass.getText().toString().trim();

        boolean flag = false;


        if (TextUtils.isEmpty(email)) {
            et_mail.setFocusable(true);
            et_mail.requestFocus();
            Toast.makeText(this, R.string.ors, Toast.LENGTH_SHORT).show();
        } else if
        (email.matches("[0-9]+") && email.length() < 6 || email.matches("[0-9]+") && email.length() > 15) {


            Toast.makeText(this, R.string.valids, Toast.LENGTH_SHORT).show();

        } else if (!email.contains("@") && email.matches("[a-zA-Z.? ]*")) {
            Toast.makeText(this, R.string.emailsss, Toast.LENGTH_SHORT).show();
            et_mail.requestFocus();
        } else if (email.contains("@") && !email.matches(emailPattern)) {
            Toast.makeText(this, R.string.emailsss, Toast.LENGTH_SHORT).show();
            et_mail.requestFocus();
        } else if (TextUtils.isEmpty(pass)) {
            et_pass.setFocusable(true);
            et_pass.requestFocus();
            Toast.makeText(this, R.string.enter, Toast.LENGTH_SHORT).show();
            //edtPass.requestFocus();
        } else if (et_pass.getText().toString().length() <= 6 || et_pass.getText().toString().length() >= 16) {
            Toast.makeText(this, R.string.betweenss, Toast.LENGTH_SHORT).show();
            return false;
        } else if (pass.length() >= 6 && pass.length() <= 16 && !pass.matches(PASSWORD_PATTERN)) {
            Toast.makeText(this, R.string.camb, Toast.LENGTH_SHORT).show();
            return false;
        } else {
            serviceLogin();
        }


        return true;
    }
    private void serviceLogin() {
        {
            email = et_mail.getText().toString().trim();
            pass = et_pass.getText().toString().trim();
            String deviceToken = SharedPreferenceWriter.getInstance(this).getString(SPreferenceKey.DEVICETOKEN);

            if (new InternetCheck(this).isConnect()) {
                //aray1@gmail.com,arya8055
                DialogPopup dialog = new DialogPopup(this);
                dialog.showLoadingDialog(this, "");

                ApiInterface api_service = RetrofitInit.getConnect().createConnection();
                Call<LoginResponse> call = api_service.loginResponse(email,pass,deviceToken);
                call.enqueue(new Callback<LoginResponse>() {
                    @Override
                    public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                        if (response.isSuccessful()) {
                            LoginResponse server_response = response.body();
                            if (server_response.getStatus() == 200) {
                                dialog.dismissLoadingDialog();
                                SharedPreferenceWriter.getInstance(LoginActivity.this).writeStringValue(SPreferenceKey._id, String.valueOf(server_response.getId()));
                                SharedPreferenceWriter.getInstance(LoginActivity.this).writeStringValue(SPreferenceKey.user_id_forgot,"");

                                SharedPreferenceWriter.getInstance(LoginActivity.this).writeStringValue(SPreferenceKey.jwtToken,String.valueOf(server_response.getToken()));
                                Intent i = new Intent(LoginActivity.this, HomeActivity.class);


                                LoginActivity.this.startActivity(i);
                                finish();
                            }  else {
                                dialog.dismissLoadingDialog();

                                Toast.makeText(LoginActivity.this, R.string.in_valid, Toast.LENGTH_SHORT).show();
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<LoginResponse> call, Throwable t) {
                        dialog.dismissLoadingDialog();

                    }
                });
            } else {
                Toast toast = Toast.makeText(this, R.string.in, Toast.LENGTH_LONG);
                toast.setGravity(Gravity.CENTER, 0, 0);
                toast.show();
            }
        }


    }




    @Override
    public void onBackPressed() {
        finishAffinity();

    }

    @OnClick(R.id.constraintLayout)
    void fbimage()
    {
        Intent i = new Intent(LoginActivity.this, FirebaseFacebookLogin.class);
        startActivityForResult(i, FACEBOOK_REQ);
    }
    @OnClick(R.id.constraintLayout2)
    void gmimage()
    {
        Intent i = new Intent(LoginActivity.this, GoogleLoginFirebase.class);
        startActivityForResult(i, RC_SIGN_IN);
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        {
            Intent intent;

            if (requestCode == RC_SIGN_IN) {

                if (data != null) {
                    String name = data.getStringExtra("name");
                    String socialid = data.getStringExtra("socialid");


                    String email = data.getStringExtra("email");
                    data.getStringExtra("image");
                    serviceGoogleLogin(name, socialid, email, "Google");
                } else {
                    Toast.makeText(this, R.string.reques, Toast.LENGTH_SHORT).show();
                }
            }

            if (requestCode == FACEBOOK_REQ) {
                if (data != null) {
                    String email = data.getStringExtra("email");
                    String socialid = data.getStringExtra("socialid");
                    String fname = data.getStringExtra("f_name");
                    String lname = data.getStringExtra("l_name");
                    String number = data.getStringExtra("number");


                    String name = fname + " " + lname;

                    data.getStringExtra("address");
                    data.getStringExtra("image");
                    ;

if(!socialid.isEmpty()&&email==null&&number==null)
{
    serviceFacebbokLogin(fname, lname,socialid, fname, "Facebook");

}
else
{
    serviceFacebbokLogin(fname, lname,socialid, email, "Facebook");

}



                } else {
                    Toast.makeText(this, R.string.requests, Toast.LENGTH_SHORT).show();

                }


            }

        }
    }

    private void serviceGoogleLogin(String name,String socialid,String email,String type) {
        {

            String deviceToken = SharedPreferenceWriter.getInstance(this).getString(SPreferenceKey.DEVICETOKEN);

            if (new InternetCheck(this).isConnect()) {
                //aray1@gmail.com,arya8055
                DialogPopup dialog = new DialogPopup(this);
                dialog.showLoadingDialog(this, "");

                ApiInterface api_service = RetrofitInit.getConnect().createConnection();
                Call<SocialLoginResponse> call = api_service.gmailResponse(name,"",email,socialid,type,deviceToken);
                call.enqueue(new Callback<SocialLoginResponse>() {
                    @Override
                    public void onResponse(Call<SocialLoginResponse> call, Response<SocialLoginResponse> response) {
                        if (response.isSuccessful()) {
                            SocialLoginResponse server_response = response.body();
                            if (server_response.getStatus() == 200) {
                                dialog.dismissLoadingDialog();
                                SharedPreferenceWriter.getInstance(LoginActivity.this).writeStringValue(SPreferenceKey._id, String.valueOf(server_response.get_$UserId165()));
                                SharedPreferenceWriter.getInstance(LoginActivity.this).writeStringValue(SPreferenceKey.user_id_forgot,"Social");

                                SharedPreferenceWriter.getInstance(LoginActivity.this).writeStringValue(SPreferenceKey.jwtToken,String.valueOf(server_response.getToken()));
                                Intent i = new Intent(LoginActivity.this, HomeActivity.class);


                                LoginActivity.this.startActivity(i);
                                finish();
                            }  else {
                                dialog.dismissLoadingDialog();

                                Toast.makeText(LoginActivity.this, R.string.invalid, Toast.LENGTH_SHORT).show();
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<SocialLoginResponse> call, Throwable t) {
                        dialog.dismissLoadingDialog();

                    }
                });
            } else {
                Toast toast = Toast.makeText(this, R.string.connectionsss, Toast.LENGTH_LONG);
                toast.setGravity(Gravity.CENTER, 0, 0);
                toast.show();
            }
        }


    }
    private void serviceFacebbokLogin(String name,String lname,String socialid,String email,String type) {
        {

            String deviceToken = SharedPreferenceWriter.getInstance(this).getString(SPreferenceKey.DEVICETOKEN);

            if (new InternetCheck(this).isConnect()) {
                //aray1@gmail.com,arya8055
                DialogPopup dialog = new DialogPopup(this);
                dialog.showLoadingDialog(this, "");

                ApiInterface api_service = RetrofitInit.getConnect().createConnection();
                Call<SocialLoginResponse> call = api_service.fbResponse(name,lname,email,socialid,type,deviceToken);
                call.enqueue(new Callback<SocialLoginResponse>() {
                    @Override
                    public void onResponse(Call<SocialLoginResponse> call, Response<SocialLoginResponse> response) {
                        if (response.isSuccessful()) {
                            SocialLoginResponse server_response = response.body();
                            if (server_response.getStatus() == 200) {
                                dialog.dismissLoadingDialog();
                                SharedPreferenceWriter.getInstance(LoginActivity.this).writeStringValue(SPreferenceKey._id, String.valueOf(server_response.get_$UserId165()));
                                SharedPreferenceWriter.getInstance(LoginActivity.this).writeStringValue(SPreferenceKey.user_id_forgot,"Social");

                                SharedPreferenceWriter.getInstance(LoginActivity.this).writeStringValue(SPreferenceKey.jwtToken,String.valueOf(server_response.getToken()));
                                Intent i = new Intent(LoginActivity.this, HomeActivity.class);


                                LoginActivity.this.startActivity(i);
                                finish();
                            }  else {
                                dialog.dismissLoadingDialog();

                                Toast.makeText(LoginActivity.this, R.string.invalid, Toast.LENGTH_SHORT).show();
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<SocialLoginResponse> call, Throwable t) {
                        dialog.dismissLoadingDialog();

                    }
                });
            } else {
                Toast toast = Toast.makeText(this, R.string.connectionsss, Toast.LENGTH_LONG);
                toast.setGravity(Gravity.CENTER, 0, 0);
                toast.show();
            }
        }


    }


}
