package com.snapic.outeractivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.hbb20.CountryCodePicker;
import com.snapic.R;
import com.snapic.response.EmailNumberVerifyResponse;
import com.snapic.response.UserDetailResponse;
import com.snapic.retrofit.ApiInterface;
import com.snapic.retrofit.RetrofitInit;
import com.snapic.utilities.DialogPopup;
import com.snapic.utilities.InternetCheck;
import com.snapic.utilities.SPreferenceKey;
import com.snapic.utilities.SharedPreferenceWriter;

import androidx.appcompat.app.AppCompatActivity;
import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class EditMobileNumberActivity extends AppCompatActivity implements CountryCodePicker.OnCountryChangeListener{
    @BindView(R.id.tv_send)
    TextView tv_send;
    @BindView(R.id.img_back)
    ImageView img_back;
    @BindView(R.id.et_num)
    EditText et_num;
    @BindView(R.id.countryCodePicker)
    CountryCodePicker countryCodePicker;
    String countrycode="";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_mobile_number);
        ButterKnife.bind(this);
        countryCodePicker.setOnCountryChangeListener(this);

        tv_send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                checknumber();
            }
        });
        img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        serviceUserDetail();

    }
    private void serviceUserDetail() {
        {
            if (new InternetCheck(EditMobileNumberActivity.this).isConnect()) {
                String key = SharedPreferenceWriter.getInstance(EditMobileNumberActivity.this).getString(SPreferenceKey.jwtToken);
                String id = SharedPreferenceWriter.getInstance(EditMobileNumberActivity.this).getString(SPreferenceKey._id);
                String token="Token"+" "+key;

                //aray1@gmail.com,arya8055
                DialogPopup dialog = new DialogPopup(EditMobileNumberActivity.this);
                dialog.showLoadingDialog(EditMobileNumberActivity.this, "");

                ApiInterface api_service = RetrofitInit.getConnect().createConnection();
                Call<UserDetailResponse> call = api_service.userDetail(token,id);
                call.enqueue(new Callback<UserDetailResponse>() {
                    @Override
                    public void onResponse(Call<UserDetailResponse> call, Response<UserDetailResponse> response) {

                        if (response.isSuccessful()) {
                            UserDetailResponse server_response = response.body();
                            if(server_response.getDetail().isEmpty())
                            {
                                dialog.dismissLoadingDialog();

                                if (server_response.getStatus() == 200) {
                                    dialog.dismissLoadingDialog();
                                    et_num.setText(server_response.getData().getPhone_number());
                                    //  setPreferences(server_response);


                                } else if (server_response.getStatus() == 400) {


                                    dialog.dismissLoadingDialog();

                                }
                            }
                            else {
                                dialog.dismissLoadingDialog();

                                if (server_response.getDetail().equals("Invalid token."))
                                {
                                    startActivity(new Intent(EditMobileNumberActivity.this,LoginActivity.class));
                                }

                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<UserDetailResponse> call, Throwable t) {
                        dialog.dismissLoadingDialog();

                    }
                });
            } else {
                Toast toast = Toast.makeText(EditMobileNumberActivity.this, R.string.in, Toast.LENGTH_LONG);
                toast.setGravity(Gravity.CENTER, 0, 0);
                toast.show();
            }
        }


    }
    public void checknumber() {
        if (new InternetCheck(this).isConnect()) {





            ApiInterface api_service = RetrofitInit.getConnect().createConnection();
            Call<EmailNumberVerifyResponse> call = api_service.checkNumber(et_num.getText().toString());
            call.enqueue(new Callback<EmailNumberVerifyResponse>() {
                @Override
                public void onResponse(Call<EmailNumberVerifyResponse> call, Response<EmailNumberVerifyResponse> response) {
                    if (response.isSuccessful()) {
                        EmailNumberVerifyResponse server_response = response.body();
                        if (server_response.getStatus() == 200) {


                            countrycode=  countryCodePicker.getSelectedCountryCodeWithPlus();
                            String numberccode=countrycode+et_num.getText().toString().trim();

                            Intent i=new Intent(EditMobileNumberActivity.this,EditNumberOtpActivity.class);
                            i.putExtra("numberonly",et_num.getText().toString().trim());
                            i.putExtra("number",numberccode);

                            i.putExtra("countrycode",countrycode);


                            EditMobileNumberActivity.this.startActivity(i);


                        } else if (server_response.getStatus() == 400) {
                            Toast.makeText(EditMobileNumberActivity.this, server_response.getMsg(), Toast.LENGTH_SHORT).show();


                        } else {

                            //   Toast.makeText(LoginActivity.this, server_response.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    }
                }

                @Override
                public void onFailure(Call<EmailNumberVerifyResponse> call, Throwable t) {

                }
            });
        } else {
            Toast toast = Toast.makeText(this, R.string.in, Toast.LENGTH_LONG);
            toast.setGravity(Gravity.CENTER, 0, 0);
            toast.show();
        }

    }


    @Override
    public void onCountrySelected() {
       countrycode=  countryCodePicker.getSelectedCountryCodeWithPlus();

    }
}