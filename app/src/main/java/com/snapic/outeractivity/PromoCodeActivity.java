package com.snapic.outeractivity;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.snapic.R;
import com.snapic.response.PromoCodeResponse;
import com.snapic.response.TerrmsConditionResponse;
import com.snapic.retrofit.ApiInterface;
import com.snapic.retrofit.RetrofitInit;
import com.snapic.utilities.DialogPopup;
import com.snapic.utilities.InternetCheck;
import com.snapic.utilities.SPreferenceKey;
import com.snapic.utilities.SharedPreferenceWriter;

import androidx.appcompat.app.AppCompatActivity;
import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class PromoCodeActivity extends AppCompatActivity {

    @BindView(R.id.img_back)
    ImageView img_back;
    @BindView(R.id.tv_promo)
    TextView tv_promo;
    @BindView(R.id.im_copy)
    ImageView im_copy;
    @BindView(R.id.text)
    TextView text;
    String promo="";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_promocode);
        ButterKnife.bind(this);
        img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        servicePromoCode();
        serviceTerms();
        im_copy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ClipboardManager clipboard = (ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);
                ClipData clip = ClipData.newPlainText("", promo);
                clipboard.setPrimaryClip(clip);
                Toast.makeText(PromoCodeActivity.this, ""+promo, Toast.LENGTH_SHORT).show();
            }
        });

    }

    private void servicePromoCode() {
        {
            if (new InternetCheck(PromoCodeActivity.this).isConnect()) {
                String key = SharedPreferenceWriter.getInstance(PromoCodeActivity.this).getString(SPreferenceKey.jwtToken);
                String id = SharedPreferenceWriter.getInstance(PromoCodeActivity.this).getString(SPreferenceKey._id);
                String token="Token"+" "+key;

                //aray1@gmail.com,arya8055
                DialogPopup dialog = new DialogPopup(PromoCodeActivity.this);
                dialog.showLoadingDialog(PromoCodeActivity.this, "");

                ApiInterface api_service = RetrofitInit.getConnect().createConnection();
                Call<PromoCodeResponse> call = api_service.promoCode(token);
                call.enqueue(new Callback<PromoCodeResponse>() {
                    @Override
                    public void onResponse(Call<PromoCodeResponse> call, Response<PromoCodeResponse> response) {

                        if (response.isSuccessful()) {
                            dialog.dismissLoadingDialog();
                        }           PromoCodeResponse server_response = response.body();

                            if (server_response.getStatus() == 200) {

                                promo=server_response.getData().getPromocode();

                                tv_promo.setText(server_response.getData().getPromocode());


                            } else if (server_response.getStatus() == 400) {




                        }
                    }

                    @Override
                    public void onFailure(Call<PromoCodeResponse> call, Throwable t) {
                        dialog.dismissLoadingDialog();

                    }
                });
            } else {
                Toast toast = Toast.makeText(PromoCodeActivity.this, R.string.in, Toast.LENGTH_LONG);
                toast.setGravity(Gravity.CENTER, 0, 0);
                toast.show();
            }
        }


    }
    private void serviceTerms() {
        {
            if (new InternetCheck(PromoCodeActivity.this).isConnect()) {
                String key = SharedPreferenceWriter.getInstance(PromoCodeActivity.this).getString(SPreferenceKey.jwtToken);
                String id = SharedPreferenceWriter.getInstance(PromoCodeActivity.this).getString(SPreferenceKey._id);
                String token="Token"+" "+key;

                //aray1@gmail.com,arya8055
                DialogPopup dialog = new DialogPopup(PromoCodeActivity.this);
                dialog.showLoadingDialog(PromoCodeActivity.this, "");

                ApiInterface api_service = RetrofitInit.getConnect().createConnection();
                Call<TerrmsConditionResponse> call = api_service.term(token);
                call.enqueue(new Callback<TerrmsConditionResponse>() {
                    @Override
                    public void onResponse(Call<TerrmsConditionResponse> call, Response<TerrmsConditionResponse> response) {

                        if (response.isSuccessful()) {
                            TerrmsConditionResponse server_response = response.body();

                            dialog.dismissLoadingDialog();

                            if (server_response.getStatus() == 200) {
                                dialog.dismissLoadingDialog();
                                text.setText(server_response.getData().getTerms_condition());
                                //  setPreferences(server_response);


                            } else if (server_response.getStatus() == 400) {


                                dialog.dismissLoadingDialog();

                            }

                            else {
                                dialog.dismissLoadingDialog();

                                startActivity(new Intent(PromoCodeActivity.this,LoginActivity.class));
                            }


                        }
                    }

                    @Override
                    public void onFailure(Call<TerrmsConditionResponse> call, Throwable t) {
                        dialog.dismissLoadingDialog();

                    }
                });
            } else {
                Toast toast = Toast.makeText(PromoCodeActivity.this, R.string.in, Toast.LENGTH_LONG);
                toast.setGravity(Gravity.CENTER, 0, 0);
                toast.show();
            }
        }


    }


}
