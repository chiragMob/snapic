package com.snapic.welcome;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.snapic.R;

import androidx.appcompat.app.AppCompatActivity;
import butterknife.BindView;
import butterknife.ButterKnife;

public class SignSecondThirdActivity extends AppCompatActivity {

    @BindView(R.id.get)
    TextView next;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pick_address);
        ButterKnife.bind(this);
        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(SignSecondThirdActivity.this, SignThirdActivity.class));

            }
        });


    }
    @Override
    public void onBackPressed() {
        finishAffinity();
    }
}


