package com.androidcodeman.simpleimagegallery.utils;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.androidcodeman.simpleimagegallery.R;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import static androidx.core.view.ViewCompat.setTransitionName;


public class ImageAdapter extends RecyclerView.Adapter<ImageViewHolder> {

    private ArrayList<PictureAdapter> pictureList;
    private Context pictureContx;
    private final itemClickListener picListerner;
    picture_Adapter picture_adapter;

    public ImageAdapter(ArrayList<PictureAdapter> pictureList, Context pictureContx, itemClickListener picListerner) {
        this.pictureList = pictureList;
        this.pictureContx = pictureContx;
        this.picListerner = picListerner;
    }

    @NonNull
    @Override
    public ImageViewHolder onCreateViewHolder(@NonNull ViewGroup container, int position) {
        LayoutInflater inflater = LayoutInflater.from(container.getContext());
        View cell = inflater.inflate(R.layout.item_design, container, false);

        return new ImageViewHolder(cell);
    }

    @Override
    public void onBindViewHolder(@NonNull final ImageViewHolder holder, final int position) {


holder.textView.setText(pictureList.get(position).getDate());
        picture_Adapter picture_adapter = new picture_Adapter(pictureContx,pictureList.get(position).getText(),picListerner,position);

        RecyclerView.LayoutManager manager = new GridLayoutManager(pictureContx, 4);
       holder.recycler.setLayoutManager(manager);
        holder.recycler.addItemDecoration(new DividerItemDecoration(pictureContx, LinearLayoutManager.VERTICAL));
        holder.recycler.setAdapter(picture_adapter);

        holder.checkBox.setChecked(pictureList.get(position).isSelection());

        holder.checkBox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Toast.makeText(pictureContx, ""+pictureList.size(), Toast.LENGTH_SHORT).show();


                if (pictureList.get(position).isSelection()) {

                    Toast.makeText(pictureContx, "yes", Toast.LENGTH_SHORT).show();



                    holder.checkBox.setChecked(false);
                    pictureList.get(position).setSelection(false);
                    picListerner.onPicClicked("false",position);

                    notifyDataSetChanged();


                }
                else
                {
                    {
                        Toast.makeText(pictureContx, "no", Toast.LENGTH_SHORT).show();

                        holder.checkBox.setChecked(true);

                        pictureList.get(position).setSelection(true);
                        picListerner.onPicClicked("true",position);


                        notifyDataSetChanged();

                    }

                }










            }

        });


    }

    @Override
    public int getItemCount() {
        return pictureList.size();
    }
}

