package com.androidcodeman.simpleimagegallery.utils;

import java.util.ArrayList;

/**
 * Author CodeBoy722
 */
public interface itemClickListener {

    void onPicClicked(String state,int pictureFolderPath);

    void onImageClick(int parentPosition,int childPosition);
    void iconAdd(int parentPosition,int childPosition,int value);
    void iconMinus(int parentPosition,int childPosition,int value);

}
