package com.androidcodeman.simpleimagegallery.utils;

/**
 * author CodeBoy722
 *
 * Custom Class that holds information of a folder containing images
 * on the device external storage, used to populate our RecyclerView of
 * picture folders
 */
public class imageFolder {

    private  String path;
    private  String FolderName;
    private String firstPic;
    private String date;


    public imageFolder() {

    }
    public imageFolder(String path, String folderName) {
        this.path = path;
        FolderName = folderName;
    }
    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }


    public String getFolderName() {
        return FolderName;
    }

    public void setFolderName(String folderName) {
        FolderName = folderName;
    }



    public String getFirstPic() {
        return firstPic;
    }

    public void setFirstPic(String firstPic) {
        this.firstPic = firstPic;
    }
}
