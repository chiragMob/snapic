package com.androidcodeman.simpleimagegallery;

import android.Manifest;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.transition.Fade;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.androidcodeman.simpleimagegallery.fragments.pictureBrowserFragment;
import com.androidcodeman.simpleimagegallery.utils.CustomSpinnerAdapter;
import com.androidcodeman.simpleimagegallery.utils.ImageAdapter;
import com.androidcodeman.simpleimagegallery.utils.ImageItem;
import com.androidcodeman.simpleimagegallery.utils.MarginDecoration;
import com.androidcodeman.simpleimagegallery.utils.PicHolder;
import com.androidcodeman.simpleimagegallery.utils.PictureAdapter;
import com.androidcodeman.simpleimagegallery.utils.imageFolder;
import com.androidcodeman.simpleimagegallery.utils.itemClickListener;
import com.androidcodeman.simpleimagegallery.utils.pictureFacer;
import com.androidcodeman.simpleimagegallery.utils.picture_Adapter;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

/**
 * Author CodeBoy722
 *
 * This Activity get a path to a folder that contains images from the MainActivity Intent and displays
 * all the images in the folder inside a RecyclerView
 */

public class ImageDisplay extends AppCompatActivity implements itemClickListener {

    RecyclerView imageRecycler;
    ArrayList<pictureFacer> allpictures;
    ProgressBar load;
    String foldePath;
    TextView folderName;
    List<imageFolder> folds;

    Button button;
    ArrayList<PictureAdapter> imagestore;
    ArrayList<String> checkedImage;
    ProgressBar simpleProgressBar;


    ImageAdapter adapter;
    private static final int MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE = 1;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image_display);
        imagestore=new ArrayList<>();
        checkedImage=new ArrayList<>();
        if(ContextCompat.checkSelfPermission(ImageDisplay.this,
                Manifest.permission.READ_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED)
            ActivityCompat.requestPermissions(ImageDisplay.this,
                    new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                    MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE);

        folderName = findViewById(R.id.foldername);
        button=findViewById(R.id.button);
        simpleProgressBar=findViewById(R.id.simpleProgressBar);
        simpleProgressBar.setProgress(0);

        allpictures = new ArrayList<>();
        imageRecycler = findViewById(R.id.recycler);

        Spinner spin = (Spinner) findViewById(R.id.spinner);
        folds = getPicturePaths();
        button.setVisibility(View.VISIBLE);



        final CustomSpinnerAdapter adapters = new CustomSpinnerAdapter(ImageDisplay.this,android.R.layout.simple_spinner_dropdown_item,folds);
        spin.setAdapter(adapters);

        spin.setOnItemSelectedListener(itemSelectedListener);
        imageRecycler.addItemDecoration(new MarginDecoration(this));
        imageRecycler.hasFixedSize();
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            SimpleDateFormat formatter = new SimpleDateFormat("EEE MMM dd HH:mm:ss zzzz yyyy");
            SimpleDateFormat datemonth = new SimpleDateFormat("dd-MMM");
            Date dateEndd = null;
            try {
                String date = datemonth.format(dateEndd);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkedImage.clear();
                for (int i=0;i<imagestore.size();i++)
                {
                    ArrayList<ImageItem> images = imagestore.get(i).getText();
                    for(int j=0;j<images.size();j++)
                    {
                        checkedImage.add(images.get(j).getPaths());
                        images.get(j).setSelection(true);


                    }
                }
                adapter.notifyDataSetChanged();


//                if(checkedImage.size()==8)
//                {
//                    Toast.makeText(ImageDisplay.this, "done", Toast.LENGTH_SHORT).show();
//                }
//                else
//                {
//
//                    Toast.makeText(ImageDisplay.this, "no", Toast.LENGTH_SHORT).show();
//
//                }


            }
        });



    }
    void list()
    {
        boolean isAdded = true;
        for (int i = 0; i < allpictures.size(); i++)
        {

            PictureAdapter pictureAdapter = new PictureAdapter();
            String date  = "";
            if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                SimpleDateFormat formatter = new SimpleDateFormat("EEE MMM dd HH:mm:ss zzzz yyyy");
                SimpleDateFormat datemonth = new SimpleDateFormat("dd-MMM");
                Date dateEndd = null;
                try {
                    dateEndd = formatter.parse((allpictures.get(i).getDate()));
                    date = datemonth.format(dateEndd);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            pictureAdapter.setDate(date);
            ArrayList<ImageItem> images = new ArrayList<>();
            for (int j = 0 ; j <  allpictures.size(); j++)
            {
                if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                    SimpleDateFormat formatter = new SimpleDateFormat("EEE MMM dd HH:mm:ss zzzz yyyy");
                    SimpleDateFormat datemonth = new SimpleDateFormat("dd-MMM");
                    Date dateEndd = null;
                    try {
                        dateEndd = formatter.parse((allpictures.get(j).getDate()));
                        String dateOutput = datemonth.format(dateEndd);
                        if(date.equals(dateOutput))
                        {
                            ImageItem imageItem=new ImageItem();
                            imageItem.setPaths(allpictures.get(j).getPicturePath());
                            images.add(imageItem);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

            }
            pictureAdapter.setText(images);
            for(int k = 0;k<imagestore.size();k++){
                if(imagestore.get(k).getDate().equals(date)){
                    isAdded = false;
                }else{
                    isAdded  = true;
                }
            }
            if(isAdded){
                imagestore.add(pictureAdapter);
            }
        }
    }



    AdapterView.OnItemSelectedListener itemSelectedListener = new AdapterView.OnItemSelectedListener() {
        @Override
        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
            //String item = parent.getItemAtPosition(position).toString();
            if(position==0)
                return;
            String item = parent.getItemAtPosition(position).toString();
            allpictures.clear();

            if(allpictures.isEmpty()){
                allpictures = getAllImagesByFolder( folds.get(position).getPath());
                imagestore.clear();
                list();
                adapter = new ImageAdapter(imagestore,ImageDisplay.this,ImageDisplay.this);
                imageRecycler.setLayoutManager(new LinearLayoutManager(ImageDisplay.this, LinearLayoutManager.VERTICAL, false));
                imageRecycler.setHasFixedSize(true);

                imageRecycler.setAdapter(adapter);
            }else{

            }
        }
        @Override
        public void onNothingSelected(AdapterView<?> parent) {
        }
    };

    private ArrayList<imageFolder> getPicturePaths(){
        ArrayList<imageFolder> picFolders = new ArrayList<>();
        ArrayList<String> picPaths = new ArrayList<>();
        Uri allImagesuri = android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
        String[] projection = { MediaStore.Images.ImageColumns.DATA ,MediaStore.Images.Media.DISPLAY_NAME,
                MediaStore.Images.Media.BUCKET_DISPLAY_NAME,MediaStore.Images.Media.BUCKET_ID,MediaStore.Images.Media.DATE_TAKEN};
        Cursor cursor = this.getContentResolver().query(allImagesuri, projection, null, null, null);
        try {
            if (cursor != null) {
                cursor.moveToFirst();
            }
            do{
                imageFolder folds = new imageFolder();
                String name = cursor.getString(cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DISPLAY_NAME));
                String folder = cursor.getString(cursor.getColumnIndexOrThrow(MediaStore.Images.Media.BUCKET_DISPLAY_NAME));
                String datapath = cursor.getString(cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA));

                //  String folderpaths =  datapath.replace(name,"");
                String folderpaths = datapath.substring(0, datapath.lastIndexOf(folder+"/"));

                folderpaths = folderpaths+folder+"/";
                if (!picPaths.contains(folderpaths)) {
                    picPaths.add(folderpaths);


                    folds.setPath(folderpaths);
                    folds.setFolderName(folder);
                    folds.setFirstPic(datapath);//if the folder has only one picture this line helps to set it as first so as to avoid blank image in itemview
                    picFolders.add(folds);
                }else{
                    for(int i = 0;i<picFolders.size();i++){
                        if(picFolders.get(i).getPath().equals(folderpaths)){
                            picFolders.get(i).setFirstPic(datapath);
                        }
                    }
                }
            }while(cursor.moveToNext());
            cursor.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        for(int i = 0;i < picFolders.size();i++){
        }

        //reverse order ArrayList
       /* ArrayList<imageFolder> reverseFolders = new ArrayList<>();

        for(int i = picFolders.size()-1;i > reverseFolders.size()-1;i--){
            reverseFolders.add(picFolders.get(i));
        }*/

        return picFolders;
    }




    @Override
    public void onPicClicked(String state,int postion) {
        if(state.equals("true")){
            ArrayList<ImageItem> list = imagestore.get(postion).getText();
            for(int i=0;i<list.size();i++){
                list.get(i).setSelection(true);
                checkedImage.add(list.get(i).getPaths());
            }
            imagestore.get(postion).setText(list);
            simpleProgressBar.setProgress(checkedImage.size());
            adapter.notifyDataSetChanged();
        }
        else
        {
            ArrayList<ImageItem> list = imagestore.get(postion).getText();
            for(int i=0;i<list.size();i++){
                list.get(i).setSelection(false);
                checkedImage.remove(list.get(i).getPaths());

            }
            simpleProgressBar.setProgress(checkedImage.size());

            imagestore.get(postion).setText(list);
            adapter.notifyDataSetChanged();
        }
        /*if(!postion.isEmpty())
        {
            boolean isAdded = true;
            for (int i = 0; i < allpictures.size(); i++)
            {
                if(i==Integer.parseInt(pictureFolderPath))
                {
                    PictureAdapter pictureAdapter = new PictureAdapter();
                    String date  = "";
                    if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                        SimpleDateFormat formatter = new SimpleDateFormat("EEE MMM dd HH:mm:ss zzzz yyyy");
                        SimpleDateFormat datemonth = new SimpleDateFormat("dd-MMM");
                        Date dateEndd = null;
                        try {
                            dateEndd = formatter.parse((allpictures.get(i).getDate()));
                            date = datemonth.format(dateEndd);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                    pictureAdapter.setDate(date);
                    ArrayList<ImageItem> images = new ArrayList<>();
                    for (int j = 0 ; j <  allpictures.size(); j++)
                    {
                        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                            SimpleDateFormat formatter = new SimpleDateFormat("EEE MMM dd HH:mm:ss zzzz yyyy");
                            SimpleDateFormat datemonth = new SimpleDateFormat("dd-MMM");
                            Date dateEndd = null;
                            try {
                                dateEndd = formatter.parse((allpictures.get(j).getDate()));
                                String dateOutput = datemonth.format(dateEndd);
                                if(date.equals(dateOutput))
                                {
                                    ImageItem imageItem=new ImageItem();
                                    imageItem.setSelection(true);
                                    images.add(imageItem);
                                }
                                adapter.notifyDataSetChanged();

                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }

                    }
                    pictureAdapter.setText(images);
                    for(int k = 0;k<imagestore.size();k++){
                        if(imagestore.get(k).getDate().equals(date)){
                            isAdded = false;
                        }else{
                            isAdded  = true;
                        }
                    }
                    if(isAdded){
                        imagestore.add(pictureAdapter);
                    }
                }



            }
        }*/

    }

    @Override
    public void onImageClick(int parentPosition, int childPosition) {
                        if (imagestore.get(parentPosition).getText().get(childPosition).isSelection()) {




                        //holder.checkBox.setChecked(false);
                            imagestore.get(parentPosition).getText().get(childPosition).setSelection(false);

                    adapter.notifyDataSetChanged();

                    checkedImage.remove(imagestore.get(parentPosition).getText().get(childPosition).getPaths());



                }
                else
                {
                    {
                        Toast.makeText(this, "no", Toast.LENGTH_SHORT).show();

                        //holder.checkBox.setChecked(true);

                        imagestore.get(parentPosition).getText().get(childPosition).setSelection(true);

                        adapter.notifyDataSetChanged();
                        checkedImage.add(imagestore.get(parentPosition).getText().get(childPosition).getPaths());

                    }

                }

    }

    @Override
    public void iconAdd(int parentPosition, int childPosition, int value) {
        int valueInt   =  value;
        valueInt=valueInt+1;
        imagestore.get(parentPosition).getText().get(childPosition).setValue(valueInt);
        //holder.tv_data.setText(String.valueOf(pictureList.get(position).getValue()));
        checkedImage.add(imagestore.get(parentPosition).getText().get(childPosition).getPaths());
        adapter.notifyDataSetChanged();
    }

    @Override
    public void iconMinus(int parentPosition, int childPosition, int value) {

        if(value==1)
        {
            imagestore.get(parentPosition).getText().get(childPosition).setValue(value);

        }
        else {
                value=value-1;
            imagestore.get(parentPosition).getText().get(childPosition).setValue(value);
                //holder.tv_data.setText(String.valueOf(pictureList.get(position).getValue()));
            checkedImage.remove(imagestore.get(parentPosition).getText().get(childPosition).getPaths());

                    //   test1.remove(test.get(position));
                   adapter.notifyDataSetChanged();


                }
    }

    /**
     * This Method gets all the images in the folder paths passed as a String to the method and returns
     * and ArrayList of pictureFacer a custom object that holds data of a given image
     * @param path a String corresponding to a folder path on the device external storage
     */
    public ArrayList<pictureFacer> getAllImagesByFolder(String path){
        ArrayList<pictureFacer> images = new ArrayList<>();
        Uri allVideosuri = android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
        String[] projection = { MediaStore.Images.ImageColumns.DATA ,MediaStore.Images.Media.DISPLAY_NAME,
                MediaStore.Images.Media.SIZE, MediaStore.Images.Media.DATE_TAKEN};
        Cursor cursor = ImageDisplay.this.getContentResolver().query( allVideosuri, projection, MediaStore.Images.Media.DATA + " like ? ", new String[] {"%"+path+"%"}, null);
        try {
            cursor.moveToFirst();
            do{
                pictureFacer pic = new pictureFacer();
                Integer dateTaken = cursor.getInt(cursor.getColumnIndex(MediaStore.Images.Media.DATE_TAKEN));

                Calendar calendar = Calendar.getInstance();
                calendar.setTimeInMillis(dateTaken);
                Date date = calendar.getTime();
                pic.setPicturName(cursor.getString(cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DISPLAY_NAME)));

                pic.setPicturePath(cursor.getString(cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA)));

                pic.setPictureSize(cursor.getString(cursor.getColumnIndexOrThrow(MediaStore.Images.Media.SIZE)));
                pic.setDate(date.toString());

                images.add(pic);
            }while(cursor.moveToNext());
            cursor.close();
            ArrayList<pictureFacer> reSelection = new ArrayList<>();
            for(int i = images.size()-1;i > -1;i--){
                reSelection.add(images.get(i));
            }
            images = reSelection;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return images;
    }


}
