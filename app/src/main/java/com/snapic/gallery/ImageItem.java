package com.androidcodeman.simpleimagegallery.utils;

import android.graphics.Bitmap;

public class ImageItem {

    public boolean isSelection() {
        return selection;
    }

    public void setSelection(boolean selection) {
        this.selection = selection;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Bitmap getImg() {
        return img;
    }

    public void setImg(Bitmap img) {
        this.img = img;
    }
    public String getPaths() {
        return paths;
    }
    public int getValue() {
        return value;
    }
    public void setValue(int value) {
        this.value = value;
    }

    public void setPaths(String paths) {
        this.paths = paths;
    }

    /**
     * success : Your Password has been changed successfully
     * status : 200
     * detail :
     */

    boolean selection;
    int id;
    Bitmap img;
    String paths;
    private int value;


}
