package com.androidcodeman.simpleimagegallery.utils;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.androidcodeman.simpleimagegallery.R;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import java.util.ArrayList;
import static androidx.core.view.ViewCompat.setTransitionName;


public class picture_Adapter extends RecyclerView.Adapter<PicHolder> {

    private ArrayList<ImageItem> pictureList;
    private Context pictureContx;
    private itemClickListener itemClickListener;
    int parentPosition;
    public picture_Adapter(Context pictureContx, ArrayList<ImageItem>
            pictureList,itemClickListener itemClickListener,int parentPosition) {
        this.pictureList = pictureList;
        this.pictureContx = pictureContx;
        this.itemClickListener = itemClickListener;
        this.parentPosition=parentPosition;
    }

    @NonNull
    @Override
    public PicHolder onCreateViewHolder(@NonNull ViewGroup container, int position) {
        LayoutInflater inflater = LayoutInflater.from(container.getContext());
        View cell = inflater.inflate(R.layout.pic_holder_item, container, false);
        return new PicHolder(cell);
    }

    @Override
    public void onBindViewHolder(@NonNull final PicHolder holder, final int position) {

        Glide.with(pictureContx)
                .load(pictureList.get(position).getPaths())
                .apply(new RequestOptions().centerCrop())
                .into(holder.picture);
        holder.checkBox.setChecked(pictureList.get(position).isSelection());



            holder.tv_data.setText(String.valueOf(pictureList.get(position).getValue()));


        if(pictureList.get(position).isSelection())
        {
            holder.plus.setVisibility(View.VISIBLE);
            holder.minus.setVisibility(View.VISIBLE);

        }
        else
        {
            holder.plus.setVisibility(View.GONE);
            holder.minus.setVisibility(View.GONE);
        }
        if(pictureList.get(position).getValue()==0)
        {
            holder.tv_data.setText("1");

        }
        else
        {
            holder.tv_data.setText(String.valueOf(pictureList.get(position).getValue()));

        }
        holder.picture.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Toast.makeText(pictureContx, ""+pictureList.size(), Toast.LENGTH_SHORT).show();


                itemClickListener.onImageClick(parentPosition,position);
//                if (pictureList.get(position).isSelection()) {
//
//                    Toast.makeText(pictureContx, "yes", Toast.LENGTH_SHORT).show();
//
//
//
//                        holder.checkBox.setChecked(false);
//                        pictureList.get(position).setSelection(false);
//
//                    notifyDataSetChanged();
//
//
//
//                }
//                else
//                {
//                    {
//                        Toast.makeText(pictureContx, "no", Toast.LENGTH_SHORT).show();
//
//                        holder.checkBox.setChecked(true);
//
//                        pictureList.get(position).setSelection(true);
//
//                        notifyDataSetChanged();
//
//                    }
//
//                }










            }

        });
        holder.plus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                String data=holder.tv_data.getText().toString();
//                int value   = Integer.parseInt(String.valueOf(data));
//                value=value+1;
//                pictureList.get(position).setValue(value);
//                holder.tv_data.setText(String.valueOf(pictureList.get(position).getValue()));
//                notifyDataSetChanged();
                itemClickListener.iconAdd(parentPosition,position,Integer.parseInt(holder.tv_data.getText().toString()));


            }
        });
        holder.minus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                String data=holder.tv_data.getText().toString();
//
//                int value= Integer.parseInt(String.valueOf(data));
//                if(value==1)
//                {
//                    holder.tv_data.setText(String.valueOf(value));
//
//                }
//                else
//                {
//                    value=value-1;
//                    pictureList.get(position).setValue(value);
//                    holder.tv_data.setText(String.valueOf(pictureList.get(position).getValue()));
//
//                    //   test1.remove(test.get(position));
//                    notifyDataSetChanged();
//
//
//                }
                itemClickListener.iconMinus(parentPosition,position,Integer.parseInt(holder.tv_data.getText().toString()));

            }});


                //setTransitionName(holder.picture, String.valueOf(position) + "_image");


    }

    @Override
    public int getItemCount() {
        return pictureList.size();
    }
}
