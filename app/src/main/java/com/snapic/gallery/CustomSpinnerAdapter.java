package com.androidcodeman.simpleimagegallery.utils;

import android.app.Activity;
import android.content.Context;
import androidx.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.androidcodeman.simpleimagegallery.R;

import java.util.ArrayList;
import java.util.List;

public class CustomSpinnerAdapter extends ArrayAdapter<imageFolder> {
    private Context context;
    private List<imageFolder> values;

    public CustomSpinnerAdapter(Activity context, int textViewResourceId, List<imageFolder> values) {
        super(context, textViewResourceId,values);
        this.context=context;
        this.values=values;
    }

    public View getView(int position, View convertView, ViewGroup parent){
        return super.getView(position,convertView, parent);
    }

    @Override
    public boolean isEnabled(int position) {
        if (position == 0)
            return false;
        else
            return true;
    }

    // And here is when the "chooser" is popped up
    // Normally is the same view, but you can customize it if you want
    @Override
    public View getDropDownView(int position, View convertView, @NonNull ViewGroup parent) {
        LayoutInflater mInflater = LayoutInflater.from(context);
        convertView = mInflater.inflate(R.layout.layout_custom_spinner,parent ,false);

        TextView label = convertView.findViewById(R.id.spinner_text);
        label.setText(values.get(position).getFolderName());

        if (position == 0) {
            label.setTextColor(context.getResources().getColor(R.color.grey));
        } else {
            label.setTextColor(context.getResources().getColor(android.R.color.black));
        }


        return convertView;
    }

}
