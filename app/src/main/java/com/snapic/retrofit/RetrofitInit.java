package com.assistance.roadside.retrofit;


import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


public class RetrofitInit {

    String API_BASE_URL = "http://18.224.114.141:8002";
    private ApiInterface methods;
    private static RetrofitInit connect;

    public static synchronized RetrofitInit getConnect() {
        if (connect == null) {
            connect = new RetrofitInit();
        }
        return connect;
    }

    public ApiInterface createConnection() {

        if (methods == null) {
            HttpLoggingInterceptor logging = new HttpLoggingInterceptor();// logs HTTP request and response data.
            logging.setLevel(HttpLoggingInterceptor.Level.BODY);// set your desired log level
            OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
            httpClient.connectTimeout(60, TimeUnit.SECONDS)
                    .readTimeout(60, TimeUnit.SECONDS);
// add your other interceptors …
            httpClient.addInterceptor(logging); // add logging as last interceptor

            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(API_BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(httpClient.build())
                    .build();

            methods = retrofit.create(ApiInterface.class);
        }
        return methods;
    }

}