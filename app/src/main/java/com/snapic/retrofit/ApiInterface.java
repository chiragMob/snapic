package com.assistance.roadside.retrofit;


import com.assistance.roadside.response.AccountDetailResponse;
import com.assistance.roadside.response.AddCarResponse;
import com.assistance.roadside.response.BankAddResponse;
import com.assistance.roadside.response.BookingProviderRequestListResponse;
import com.assistance.roadside.response.BookingResponse;
import com.assistance.roadside.response.CarListResponse;
import com.assistance.roadside.response.CardAccountDetailResponse;
import com.assistance.roadside.response.ChangePasswordResponse;
import com.assistance.roadside.response.CheckMobileNumberResponse;
import com.assistance.roadside.response.CheckProviderAvilablityResponse;
import com.assistance.roadside.response.EditCarResponse;
import com.assistance.roadside.response.EditVendorCarResponse;
import com.assistance.roadside.response.ForgotPasswordResponse;
import com.assistance.roadside.response.GetBankDetail;
import com.assistance.roadside.response.GetBookingPriceList;
import com.assistance.roadside.response.GetCurrentBookingResponse;
import com.assistance.roadside.response.LoginResponse;
import com.assistance.roadside.response.LogoutReponse;
import com.assistance.roadside.response.NotificationDeleteResponse;
import com.assistance.roadside.response.NotificationResponse;
import com.assistance.roadside.response.NotifyListResponse;
import com.assistance.roadside.response.PaymentListResponse;
import com.assistance.roadside.response.ProBookingAcceptResponse;
import com.assistance.roadside.response.ProivderLatResponse;
import com.assistance.roadside.response.ProviderCarDetailResponse;
import com.assistance.roadside.response.ServiceHistoryResponse;
import com.assistance.roadside.response.ServiceListResponse;
import com.assistance.roadside.response.SignUpResponse;
import com.assistance.roadside.response.SocialResponse;
import com.assistance.roadside.response.StatusUpdateResponse;
import com.assistance.roadside.response.UpdateProfileResponse;
import com.assistance.roadside.response.UserDetailResponse;
import com.assistance.roadside.response.UserLocationUpdateResponse;
import com.assistance.roadside.response.WithDrawResponse;

import java.util.Map;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.PartMap;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface ApiInterface {



    @FormUrlEncoded
    @POST("/user/checkMobile")
    Call<CheckMobileNumberResponse> checkMobileNumber(@Field("mobileNumber") String mobileNumber);


    @FormUrlEncoded
    @POST("/user/checkEmail")
    Call<CheckMobileNumberResponse> checkEmail(@Field("email") String email);


    @FormUrlEncoded
    @POST("/user/withdrawAmount")
    Call<WithDrawResponse> withdrawAmount(@Header("Authorization") String Token,@Field("payAmount") String payAmount,@Field("payType") String payType);

    @FormUrlEncoded
    @POST("/user/socialLogin")
    Call<SocialResponse> Sociallogin(@Field("socialId") String socialId, @Field("socialType") String socialType, @Field("deviceToken") String deviceToken, @Field("deviceType") String deviceType, @Field("name") String name, @Field("profilePic") String profilePic, @Field("email") String email,@Field("mobileNumber") String mobileNumber);
    @FormUrlEncoded
    @POST("/user/login")
    Call<LoginResponse> loginResponse(@Field("email") String email,@Field("password") String password, @Field("deviceType") String deviceType, @Field("deviceToken") String deviceToken);


    @GET("/user/service")
    Call<ServiceListResponse> serviceList(@Header("Authorization") String Token);
    @POST("/user/bookingRequestList")
    Call<BookingProviderRequestListResponse> bookingList(@Header("Authorization") String Token);
    @FormUrlEncoded
    @POST("/user/avaialbleProviderList")
    Call<ProivderLatResponse> providerList(@Header("Authorization") String Token, @Field("pickupLat") String pickupLat, @Field("pickupLong") String pickupLong);

    @GET("/user/getBookingPriceList")
    Call<GetBookingPriceList> getBookingPriceList(@Header("Authorization") String Token);
    @FormUrlEncoded
    @POST("/user/serviecRequestAccept")
    Call<ProBookingAcceptResponse> AcceptResponse(@Header("Authorization") String Token, @Field("bookingId") String bookingId);

    @FormUrlEncoded
    @POST("/user/rejectServiceRequest")
    Call<ProBookingAcceptResponse> RejectResponse(@Header("Authorization") String Token, @Field("bookingId") String bookingId);

    @FormUrlEncoded
    @POST("/user/updateUserLocation")
    Call<UserLocationUpdateResponse> updateUserLocation(@Header("Authorization") String Token,@Field("latitude") String latitude, @Field("longitude") String longitude);

    @FormUrlEncoded
    @POST("/user/bookService")
    Call<BookingResponse> userBookServiceFlatTire(@Header("Authorization") String Token,@Field("token") String token , @Field("workServiceType") String workServiceType, @Field("carId") String carId, @Field("spareTireChange") String spareTireChange, @Field("width") String width, @Field("size") String size, @Field("speed") String speed, @Field("location") String location, @Field("pickupLat") String pickupLat, @Field("pickupLong") String pickupLong, @Field("subTotal") String subTotal, @Field("serviceCharges") String serviceCharges, @Field("totalPrice") String totalPrice, @Field("paymentId") String paymentId, @Field("paymentType") String paymentType, @Field("paymentTDate") String paymentTDate, @Field("paymentStatus") String paymentStatus, @Field("serviceType") String serviceType);

    @FormUrlEncoded
    @POST("/user/bookService")
    Call<BookingResponse> userBookServiceFuel(@Header("Authorization") String Token,@Field("token") String token, @Field("workServiceType") String workServiceType, @Field("carId") String carId, @Field("fuelType") String fuelType, @Field("serviceReason") String serviceReason, @Field("location") String location, @Field("pickupLat") String pickupLat, @Field("pickupLong") String pickupLong, @Field("subTotal") String subTotal, @Field("serviceCharges") String serviceCharges, @Field("totalPrice") String totalPrice, @Field("paymentId") String paymentId, @Field("paymentType") String paymentType, @Field("paymentTDate") String paymentTDate, @Field("paymentStatus") String paymentStatus);

    @FormUrlEncoded
    @POST("/user/bookService")
    Call<BookingResponse> userBookServiceBattery(@Header("Authorization") String Token,@Field("token") String token, @Field("workServiceType") String workServiceType,@Field("carId") String carId, @Field("location") String location, @Field("pickupLat") String pickupLat, @Field("pickupLong") String pickupLong, @Field("subTotal") String subTotal, @Field("serviceCharges") String serviceCharges, @Field("totalPrice") String totalPrice, @Field("paymentId") String paymentId, @Field("paymentType") String paymentType, @Field("paymentTDate") String paymentTDate, @Field("paymentStatus") String paymentStatus, @Field("serviceType") String serviceType);

    @FormUrlEncoded
    @POST("/user/bookService")
    Call<BookingResponse> userBookServiceTow(@Header("Authorization") String Token,@Field("token") String token,@Field("workServiceType") String workServiceType,@Field("carId") String carId, @Field("dropOffLat") String dropOffLat, @Field("dropOffLong") String dropOffLong, @Field("carTowReason") String carTowReason, @Field("carTowMessage") String carTowMessage, @Field("dropLocation") String dropLocation, @Field("location") String location, @Field("pickupLat") String pickupLat, @Field("pickupLong") String pickupLong, @Field("subTotal") String subTotal, @Field("serviceCharges") String serviceCharges, @Field("totalPrice") String totalPrice, @Field("paymentId") String paymentId, @Field("paymentType") String paymentType, @Field("paymentTDate") String paymentTDate, @Field("paymentStatus") String paymentStatus);
    @POST("/user/notificationDelete")
    Call<NotificationDeleteResponse> deleteNotify(@Header("Authorization") String Token);
    @FormUrlEncoded
    @POST("/user/bookService")
    Call<BookingResponse> userBookServiceLockout(@Header("Authorization") String Token,@Field("token") String token, @Field("workServiceType") String workServiceType,@Field("carId") String carId, @Field("location") String location, @Field("pickupLat") String pickupLat, @Field("pickupLong") String pickupLong, @Field("subTotal") String subTotal, @Field("serviceCharges") String serviceCharges, @Field("totalPrice") String totalPrice, @Field("paymentId") String paymentId, @Field("paymentType") String paymentType, @Field("paymentTDate") String paymentTDate, @Field("paymentStatus") String paymentStatus, @Field("serviceType") String serviceType);

    @FormUrlEncoded
    @POST("user/checkProviderAvailability")
    Call<CheckProviderAvilablityResponse> checkProviderAvailResponse(@Header("Authorization") String Token,@Field("latitude") String latitude,@Field("longitude") String longitude);

    @FormUrlEncoded
    @POST("/user/providerStatus")
    Call<StatusUpdateResponse> updateTypeUser(@Header("Authorization") String Token, @Field("userType") String userType);

    @GET("/user/getCurrentBooking")
    Call<GetCurrentBookingResponse> getCurrentBooking(@Header("Authorization") String Token);
    @FormUrlEncoded
    @POST("/user/workDoneByProvider")
    Call<BookingResponse> completeWork(@Header("Authorization") String Token,@Field("bookingId") String bookingId);


    @FormUrlEncoded
    @POST("/user/cancelBooking")
    Call<BookingResponse> CancelBooking(@Header("Authorization") String Token,@Field("bookingId") String bookingId,@Field("cancelReason") String cancelReason,@Field("cancelMessage") String cancelMessage);

    @FormUrlEncoded
    @POST("/user/addBank")
    Call<BankAddResponse> addBankResponse(@Header("Authorization") String Authorization, @Field("bankName") String bankName, @Field("accountHolderName") String accountHolderName, @Field("accountNumber") String accountNumber, @Field("routingNumber") String routingNumber);



    @POST("/vendor/getVendorCar")
    Call<ProviderCarDetailResponse> vendorDetailResponse(@Header("Authorization") String Token);

    @Multipart
    @POST("/vendor/updateVendorCar")
    Call<EditVendorCarResponse> updateVendorCarResponse(@Header("Authorization") String Authorization,
                                                        @PartMap Map<String, RequestBody> part,
                                                        @Part MultipartBody.Part file);
    @Multipart
    @POST("/user/addCar")
    Call<AddCarResponse> addCarResponse(@Header("Authorization") String Authorization,
                                           @PartMap Map<String, RequestBody> part,
                                           @Part MultipartBody.Part file);

    @Multipart
    @POST("/vendor/addVenderCar")
    Call<AddCarResponse> addProviderCar(@Header("Authorization") String Authorization,
                                        @PartMap Map<String, RequestBody> part,
                                        @Part MultipartBody.Part file);
    @Multipart
    @POST("/user/addCar")
    Call<AddCarResponse> addCarDetailResponse(@Header("Authorization") String Authorization,
                                        @PartMap Map<String, RequestBody> part);

    @Multipart
    @POST("/user/updateUserProfile")
    Call<UpdateProfileResponse> updateProfile(@Header("Authorization") String Authorization,
                                              @PartMap Map<String, RequestBody> part,
                                              @Part MultipartBody.Part file);

    @Multipart
    @POST("/user/editCar/{id}")
    Call<EditCarResponse> editResponse(@Path("id") String id, @Header("Authorization") String Authorization,
                                       @PartMap Map<String, RequestBody> part,
                                       @Part MultipartBody.Part file);
    @POST("/user/getCarUserById")
    Call<CarListResponse> carListResonse(@Header("Authorization") String Token);
    @GET("/user/getUserPaymentList")
    Call<PaymentListResponse> paymentListResonse(@Header("Authorization") String Token);
    @GET("/user/getServiceHistory?")
    Call<ServiceHistoryResponse> getServiceList(@Header("Authorization") String Token, @Query("pageNumber") String pageNumber, @Query("limit") String limit);

    @POST("/user/notificationList")
    Call<NotifyListResponse> notifyListResonse(@Header("Authorization") String Token);
    @POST("/user/logout")
    Call<LogoutReponse> logoutResponse(@Header("Authorization") String Token);
    @GET("/user/getUserDetails")
    Call<UserDetailResponse> userDetail(@Header("Authorization") String Token);
    @GET("/user/getBankDetail")
    Call<GetBankDetail> accountDetail(@Header("Authorization") String Token);


    @FormUrlEncoded
    @POST("/user/notificationUpdate")
    Call<NotificationResponse> notifStatus(@Header("Authorization") String Token, @Field("notificationStatus") String notificationStatus);
    @FormUrlEncoded
    @POST("/user/dutyStatus")
    Call<NotificationResponse> dutyStatus(@Header("Authorization") String Token, @Field("dutyStatus") String notificationStatus);
    @FormUrlEncoded
    @POST("/user/forgotPassowrd")
    Call<ForgotPasswordResponse> forgotResponse(@Field("mobileNumber") String mobileNumber, @Field("password") String password);

    @FormUrlEncoded
    @POST("/user/withdrawAmount")
    Call<WithDrawResponse> drawResponse(@Header("Authorization") String Token,@Field("payAmount") String payAmount, @Field("payType") String payType);


    @FormUrlEncoded
    @POST("/user/changePassword")
    Call<ChangePasswordResponse> changePasswordResponse(@Header("Authorization") String Token,@Field("password") String password,@Field("newPassword") String newPassword,@Field("compareNewPassword") String compareNewPassword);

    @FormUrlEncoded
    @POST("/user/payment")
    Call<ChangePasswordResponse> payment(@Field("tokenId") String tokenId,@Field("amount") String amount);


    @FormUrlEncoded
    @POST("user/createUser")
    Call<SignUpResponse> signupDetails(
                                       @Field("mobileNumber") String mobileNumber,
                                       @Field("password") String password,
                                       @Field("first_Name") String first_Name,
                                       @Field("last_Name") String last_Name,
                                       @Field("email") String email,
                                       @Field("terms_Conditions") String terms_Conditions,
                                       @Field("driverLicense") String driverLicense,
                                       @Field("countryCode") String countryCode
            , @Field("deviceType") String deviceType, @Field("deviceToken") String deviceToken);
}